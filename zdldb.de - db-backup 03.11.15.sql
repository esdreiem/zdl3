--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: address; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE address (
    id integer NOT NULL,
    revid integer,
    address text,
    type text
);


ALTER TABLE address OWNER TO zdl;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_id_seq OWNER TO zdl;

--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- Name: books; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE books (
    id integer NOT NULL,
    title text,
    author text,
    publisher text,
    pub_country text,
    price text,
    note text,
    pages text,
    series text,
    nr_in_series text,
    pub_date text,
    isbn text,
    currency text,
    media text,
    acct text
);


ALTER TABLE books OWNER TO zdl;

--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE books_id_seq OWNER TO zdl;

--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE books_id_seq OWNED BY books.id;


--
-- Name: bugreports; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE bugreports (
    id integer NOT NULL,
    date date,
    content text,
    author text,
    in_reply_to integer,
    done boolean,
    hidden boolean
);


ALTER TABLE bugreports OWNER TO zdl;

--
-- Name: bugreports_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE bugreports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bugreports_id_seq OWNER TO zdl;

--
-- Name: bugreports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE bugreports_id_seq OWNED BY bugreports.id;


--
-- Name: email; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE email (
    id integer NOT NULL,
    revid integer,
    address text,
    type text
);


ALTER TABLE email OWNER TO zdl;

--
-- Name: email_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_id_seq OWNER TO zdl;

--
-- Name: email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE email_id_seq OWNED BY email.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE events (
    reviewid integer,
    type text,
    "timestamp" timestamp without time zone NOT NULL,
    date date,
    revid integer,
    payload text,
    id integer NOT NULL,
    acct text
);


ALTER TABLE events OWNER TO zdl;

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE events_id_seq OWNER TO zdl;

--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: fields; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE fields (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE fields OWNER TO zdl;

--
-- Name: fields_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fields_id_seq OWNER TO zdl;

--
-- Name: fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE fields_id_seq OWNED BY fields.id;


--
-- Name: rev_has_fields; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE rev_has_fields (
    revid integer,
    fieldid integer
);


ALTER TABLE rev_has_fields OWNER TO zdl;

--
-- Name: reviewers; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE reviewers (
    id integer NOT NULL,
    firstname text,
    lastname text,
    title text,
    gender text,
    published integer,
    requests integer,
    declined integer,
    warned integer,
    note text,
    published_in text,
    form integer,
    content integer,
    formnote text,
    contentnote text,
    isblocked boolean,
    acct text
);


ALTER TABLE reviewers OWNER TO zdl;

--
-- Name: reviewers_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE reviewers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reviewers_id_seq OWNER TO zdl;

--
-- Name: reviewers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE reviewers_id_seq OWNED BY reviewers.id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE reviews (
    id integer NOT NULL,
    bookid integer,
    revid integer,
    acct text,
    heft text
);


ALTER TABLE reviews OWNER TO zdl;

--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reviews_id_seq OWNER TO zdl;

--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE reviews_id_seq OWNED BY reviews.id;


--
-- Name: user_mgmt; Type: TABLE; Schema: public; Owner: zdl; Tablespace: 
--

CREATE TABLE user_mgmt (
    id integer NOT NULL,
    acct text,
    pw text,
    last_seen timestamp without time zone,
    role text
);


ALTER TABLE user_mgmt OWNER TO zdl;

--
-- Name: user_mgmt_id_seq; Type: SEQUENCE; Schema: public; Owner: zdl
--

CREATE SEQUENCE user_mgmt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_mgmt_id_seq OWNER TO zdl;

--
-- Name: user_mgmt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zdl
--

ALTER SEQUENCE user_mgmt_id_seq OWNED BY user_mgmt.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY books ALTER COLUMN id SET DEFAULT nextval('books_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY bugreports ALTER COLUMN id SET DEFAULT nextval('bugreports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY email ALTER COLUMN id SET DEFAULT nextval('email_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY fields ALTER COLUMN id SET DEFAULT nextval('fields_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY reviewers ALTER COLUMN id SET DEFAULT nextval('reviewers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY reviews ALTER COLUMN id SET DEFAULT nextval('reviews_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY user_mgmt ALTER COLUMN id SET DEFAULT nextval('user_mgmt_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY address (id, revid, address, type) FROM stdin;
13	8	6724 Emmalee Ports\nEast Owenside, DC 59082	other
14	8	385 Glover Islands Apt. 062\nSouth Wilfredstad, IN 93934	other
15	8	0616 Lambert Harbor Suite 998\nEast Margarete, IL 20805-2710	other
16	8	289 Lueilwitz Key Apt. 836\nNew Ubaldofurt, WV 19534	other
17	8	20879 Marjorie Valleys\nBauchmouth, WY 33487	other
18	9	8734 Hoppe Gardens\nSouth Bufordchester, IA 51578	other
19	9	8172 Green Crest Apt. 705\nAmayaville, ND 57219	other
20	9	56570 Ayana Course Apt. 234\nLake Generalburgh, VT 04661	other
21	9	5523 Genoveva Trail Suite 999\nEast Ned, WI 93226-8850	other
22	9	52416 Goodwin Parkway\nSouth Marley, WV 49214	other
23	10	8009 Bernadine Fall\nWest Roger, IL 06729	other
24	10	293 Hilll Row Apt. 684\nPort Jayland, MS 31267	other
25	11	170 Francesco Terrace\nPort Brookeside, NM 04136-5929	other
26	11	53294 Sage Brook\nChestermouth, NE 27286-2895	other
27	11	3936 Paula Terrace\nNorth Betteton, MI 12802	other
28	11	32499 Satterfield Court Suite 217\nHeaneyland, VT 77144-8851	other
29	12	2736 Kuhlman Circle Apt. 625\nStammmouth, TN 53015	other
30	12	9634 Witting Springs\nGrahammouth, ID 29754-0386	other
31	12	715 Estell Centers\nDachland, NH 84519-8181	other
32	13	376 Milford Parks\nNew Chase, TN 62055-5987	other
33	13	320 Mraz Creek Suite 649\nPort Ceasarport, MI 80922-0123	other
34	14	0840 Vandervort Plaza Suite 100\nLake Mckenna, NY 89559	other
35	14	67701 Schuster Mountains Apt. 545\nNorth Violet, MA 94579-7793	other
36	14	55728 Barton Lights Apt. 706\nJohnstown, AZ 66719-8247	other
37	14	86749 Lorena Gardens\nNorth Makenziefort, WI 31974	other
38	14	66962 Johns Burg\nAdanburgh, UT 44176	other
39	15	22092 Anderson Turnpike Apt. 519\nSouth Meganeport, NV 02041-4775	other
40	15	86671 Schumm Ville\nNew Oceanefurt, MN 69322	other
41	16	8657 Chyna Road\nEmilioborough, CO 15021	other
42	16	9585 Mueller Lake\nJasonville, SD 07346	other
46	18	522 Berge Well Suite 628\nHandstad, CT 76764-1213	other
47	18	628 Luettgen View Apt. 398\nJermainshire, NY 93849	other
48	18	16203 Gerda Dam\nLake Cecelia, AK 27391-7952	other
49	18	60204 Christy Course\nDarianashire, ND 33349	other
50	18	0779 Judy Estate Suite 925\nNorth Brantstad, ND 53827	other
51	19	59918 Geovanny Trail Apt. 308\nSouth Katrinefurt, HI 03763-7647	other
52	19	51985 Bechtelar Circle Suite 764\nKeiratown, ID 76725-5445	other
53	20	33241 Isai Land Apt. 581\nWuckerthaven, NC 92068	other
54	20	9515 Murphy Square\nFredport, NJ 14387	other
55	20	7513 Jules Centers Apt. 719\nSouth Koryhaven, VA 47840-5277	other
56	20	737 Strosin Heights\nEast Daisha, NM 19969	other
57	21	6737 Kuhlman Isle Apt. 525\nSouth Aliya, GA 89732-8353	other
58	21	1418 Fletcher Trail Apt. 661\nPort Ignatiusview, IA 07037	other
59	21	80896 Smitham Unions Suite 561\nMcLaughlinton, HI 94502-6821	other
60	21	627 Demarcus Alley\nEast Augustinebury, UT 51282-7376	other
61	21	296 Dejon Shore\nEbertburgh, WV 14895	other
62	22	112 VonRueden Knolls\nCesarland, MN 90638-6651	other
63	22	8409 Margaret Greens Suite 309\nNew Archibald, WA 86674-8526	other
64	23	683 Quinn View\nBettyland, CO 45375	other
65	23	820 Marta Fords Suite 621\nNew Bryceport, UT 52936	other
66	24	7243 Ryann Spur\nRosenbaummouth, DC 09696	other
67	24	30434 Rodrick Parks\nSouth Gina, AZ 49384	other
68	25	5658 Howe Court Suite 337\nGreenfelderburgh, CO 12245-6754	other
69	25	94925 Titus Lodge\nWest Garetthaven, CA 97617-1256	other
70	25	2080 Langworth Springs\nSeanton, VA 18978-0668	other
71	25	0381 Luther Prairie\nRueckerborough, TX 95230-4658	other
72	26	0987 Ana Field\nNew Ednaberg, NY 37700	other
73	26	529 Strosin Mountain Apt. 396\nSouth Clementina, VA 91422	other
74	27	2748 Wunsch Field Apt. 645\nNorth Reyna, MO 73572-1408	other
75	27	792 Dibbert Plain Apt. 015\nWest Karson, DC 55929	other
99	36	2284 Darren Cliff Suite 310\nSouth Brionna, MA 91233-7575	other
100	36	4914 Raheem Rest Suite 534\nNew Jalonbury, SD 56110-6089	other
113	41	xkmtgkh	other
76	28	67959 Bailey Port Apt. 045\nSouth Winston, NC 75827-9280	other
77	28	119 Abshire Curve Suite 958\nStanleyport, AZ 94376	other
78	28	23204 Kylie Prairie Apt. 218\nTimothymouth, MI 75626	other
79	29	18207 Heathcote Dale Suite 518\nPort Felicityshire, WY 32960	other
80	29	96519 Doyle Ridges Suite 036\nStrosinland, KS 89640	other
81	29	452 Davin Flats Apt. 312\nSouth Talia, MT 57441	other
82	29	6981 Brook Well Apt. 236\nWest Oren, WI 06389	other
83	30	0924 Monique Village Apt. 516\nMillerfort, IA 14254	other
84	30	79398 Orlo Ridge\nKatherineshire, KY 96959-5682	other
85	30	326 Kshlerin Coves Apt. 667\nWolffmouth, DE 91311	other
86	30	830 Shannon Port\nSouth Susannatown, WA 72085-4039	other
87	31	4109 Stephanie Summit\nPort Justine, ND 32770-3204	other
88	31	1266 Feeney Isle Apt. 477\nNaderbury, ME 07543-1062	other
89	31	07969 Hettinger Motorway\nNew Rebecca, WV 80526	other
90	31	3711 Elsa Fork\nNew Anastasiafurt, GA 55950-5524	other
91	31	226 Neoma Plains\nWest Sofiaport, MI 19515	other
92	32	5290 Shanel Divide\nWest Jermeyland, TX 41518-0448	other
93	32	245 Laverne Well\nWest Patience, TX 13304	other
94	32	11144 Hubert Junctions\nGibsonton, NH 23602	other
95	32	61423 Marcelino Landing\nWest Desmondstad, OR 44554	other
44	17	3778 Weber Underpass Suite 751\r\nSpencerfort, OH 87793	other
45	17	5649 Adolphus Knoll\r\nMurphystad, KS 84545	other
96	32	6802 Ziemann Mount\nJunestad, HI 98041	other
97	36	9747 Nicolette Mount Apt. 042\nRebekahside, MT 14271	other
98	36	7334 Greenfelder Expressway\nWest Max, ID 38397	other
101	37	503 Schuppe Parks\nNorth Alexandra, ND 50735-1058	other
102	37	78346 Violette Summit\nLake Davemouth, NJ 77031	other
103	38	26281 Clinton Brooks Suite 308\nSouth Sterling, RI 80077-5209	other
104	38	8056 Gleichner Shores Apt. 350\nKristyville, OK 47370	other
105	38	39287 Lavada Forges\nNorth Zella, AZ 57229	other
106	38	0574 Leon Port Suite 023\nLake Soledadport, MO 74039-8406	other
107	39	065 Lakin Club\nKennaville, AL 81226-9355	other
108	39	2102 Nichole Vista Suite 308\nRoxannestad, ND 82502	other
109	40	74109 Langosh Land\nBeerfort, TN 87167-0458	other
110	40	19889 Kovacek Green\nEichmannberg, CT 72502-8099	other
111	40	1297 Yost Plains Suite 073\nMedhurstchester, WV 60660	other
112	40	88438 Buford Gateway Suite 032\nSkileschester, AK 20665-2394	other
117	43	dtrzjb 5drzh\r\nziol\r\nu\r\n	other
118	43	ub 45rhrujhuuij0\r\n\r\n\r\nhz9h9ipß\r\n\r\n	other
119	43	h ebtgg\r\nfzghsfgh	other
120	44	rtz	other
125	50		other
126	54	vasdfvg	standard
128	56	VVVVVVVVVV\r\n20006	standard
129	56	Kommission für Mundartforschung\r\nBayerisches Wörterbuch\r\nBayerische Akademie der Wissenschaften\r\nzu München\r\nAlfons-Goppel-Str. 11 \r\n80539 München	standard
127	55		standard
130	57	BBBBB\r\n200008	standard
132	59		standard
133	60	jbljkbajbg\r\njabdövkjbvö\r\njböavn	standard
134	60	fbÖGJB\r\nKNDAJVÖBVGÖ\r\nJBAVSCNL	other
114	42	äpoü\r\nfzuhj\r\nfghz	other
115	42	rtgfgj\r\ngzhjk	other
116	42	gfijur6jj	other
136	62		standard
137	63	Romanistik IV\r\nHeinrich-Heine-Universität Düsseldorf\r\nUniversitätsstr. 1\r\n40225 Düsseldorf\r\n	standard
138	64	Universität Leipzig\r\nInstitut für Linguistik\r\nBeethovenstraße 15\r\nD-04107 Leipzig\r\n	standard
139	65	Institut für deutsche Philologie\r\nUniversität München\r\nSchellingstraße 3\r\n80799 München\r\n	standard
140	65	Mühlstraße 18\r\n82346  Andechs-Erling\r\n	other
131	58		standard
135	61		standard
141	61		other
142	66		standard
143	67		standard
144	68		standard
145	59		standard
43	17	134 Lora Street\r\nNew Bessie, WV 02169-3469	other
146	69		standard
147	70		standard
148	71	Musterweg 1\r\n12345 Musterstadt	standard
149	72	Musterstr. 1\r\n12345 Potsdam	standard
150	73		standard
\.


--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('address_id_seq', 150, true);


--
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY books (id, title, author, publisher, pub_country, price, note, pages, series, nr_in_series, pub_date, isbn, currency, media, acct) FROM stdin;
67	Funktionswörter | buch | forschung. Zur lexikographischen Darstellung von Partikeln, Konnektoren, Präpositionen und anderen Funktionswörtern	Breindl, Eva / Annette Klosa (Hg.) 	Olms	Hildesheim	78		422			2013		£	\N	\N
65	The Oxford Handbook of Sociolinguistics	Bayley, Robert / Richard Cameron / Ceil Luca (Hg.)	Oxford University Press	Oxford	95.00	Umfangreich	848			2013	9780199744084	£	\N	\N
64	Sprachgeschichten. Ein Lesebuch für WERNER KÖNIG zum 60. Geburtstag	Edith Funk / Stefan Kleiner / Manfred Renn / Bernadette Wecker (Hg.)	Winter	Heidelberg	100	Tolles Buch	313	Schriften zum Bayerischen Sprachatlas	7	2003	3-8253-1528-2	£	\N	\N
17	Multi-channelled cohesive moratorium	Magnolia Hamill	McKenzie-Dietrich	Nicaragua	99.53	\N	524	Integrated impactful opensystem	8	14.06.2009	4016100556893	€	\N	\N
45	jkgjkzkkkkkkkkk											€	\N	\N
20	Persevering multi-state application	Mrs. Kali Rudolfsdottir	Hayes, Ritchie and Kilback	Norway	92.06	Voluptatem odit sed quam ut eius quaerat.	947	Progressive modular throughput	4	01.06.2001	8921082509569	€	\N	\N
52	fsdfsd											€	\N	\N
39	Enterprise-wide system-worthy installation	Archibald Padberg	Renner and Sons	Angola	68.31		318	Polarised full-range capability	9	14.09.1997	0419985055538	€	\N	\N
40	zzzgzgzgz			hngb		rfddde						€	\N	\N
62	Multi-lateral hybrid task-force	Giovanni Von	Adams-Osinski	Wallis and Futuna	63.16	Non adipisci enim ratione.	221	Up-sized actuating complexity	1	01.09.2006	6246989751012	€	\N	\N
63	User-friendly global interface	Kyle Rogahn	Thiel Group	Seychelles	97.74	\N	365	User-friendly global time-frame	6	04.03.1990	9793167134701	€	\N	\N
61	Networked fault-tolerant task-force	Imani Toy II	VonRueden, Champlin and Raynor	Serbia	3.39		563	Organic analyzing processimprovement	7	19.03.1979	8144431682137	€	\N	\N
22	Stand-alone methodical parallelism	Ms. Keira Bernhard	Daniel, Simonis and Smith	Barbados	92.95	Esse rerum qui sint possimus a nam.	307	Pre-emptive 5thgeneration analyzer	5	21.11.2000	3045474407433	€	\N	\N
71	Duden: Die deutsche Rechtschreibung	Dudenredaktion (Hg.)	Dudenverlag	Mannheim	15.-		1216	Duden	1	2006	9783411040148	€	\N	\N
72	XXXXXXX—XXX	blfkdjbgkjnd	klböjsbdf	kfnmhnögfn 	käbg	äkngäkh	46785—467678	gnghlönhf	5	3465	462768	€	\N	\N
66	Sprachverfall? Dynamik â Wandel â Variation	Plewnia, Albrecht / Andreas Witt (Hg.)	De Gruyter	Berlin/Boston	99,95		371	Jahrbuch des Instituts fÃ¼r Deutsche Sprache 2013		2014	9783110342918	€	\N	\N
73	Atlas linguistique de la Wallonie, tome 17: Famille, vie et relations sociales	Esther Baiwir	Presses universitaires de Liège, Sciences humaines	Université de Liège	150,-	160 notices, 66 cartes	421			2011		€	\N	\N
69	GäÄbÖöööÄÄÄäääÜüüüüüÜÜÜÜÜssefg						———					€	\N	\N
74	Processing Syntax and Morphology. A Neurocognitive Perspective	Ina Bornkessel-Schlesewsky / Matthias Schlesewsky	Oxford University Press	Oxford	36,95		360	Oxford Surveys in Syntax and Morphology		2009		€	\N	\N
76	UUU											€	\N	\N
77	ß	ß	ß	ß	ß	ß	ß	ß	ß	ß	ß	€	\N	\N
16	Organic bi-directional synergy	Mckenzie Schmitt	Dickens-Strosin	Guadeloupe	84.36	Et doloribus perferendis reiciendis et sint.	718	Future-proofed multi-tasking internetsolution	8	25.12.1999	9621374160469	£	\N	\N
68	Der Status von bekommen + zu + Infinitiv zwischen Modalität und semantischer Perspektivierung	Jäger, Anne 	Lang	Frankfurt [a. M.]	57,95		311			2013	9783631643112 	€		\N
78	frhgj				5							¥		\N
59	Right-sized non-volatile processimprovement—	Charley King	Rippin-Considine	Turkey	90.42	Sit quia delectus sed ipsum rerum.	650—222	Extended multi-state hierarchy—	1	04.11.1970	4242134546092	€		\N
80	dgfhgfjhkhk	hkdztkh										€		\N
82	Essential Statistics for Applied Linguistics. 	Lowie, Wander	Palgrave Macmillan	Houndmills	21,65		168			2012		€		\N
83	Phraseme im bilingualen Diskurs. "All of a sudden geht mir ein Licht auf."	Mareike Keller	Lang	Frankfurt a. M.	62,95		301	Linguistik International	30	2014		€		\N
75	Sprache - Kultur - Geschichte. Sprachhistorische Studien zum Deutschen. Hans Moser zum 60. Geburtstag. Redaktion und Lektorat: Astrid Obernosterer	Maria Pümpel-Mader / Beatrix Schönherr (Hg.)	Institut für Germanistik 1999	Innsbruck	122,–		474	Innsbrucker Beiträge zur Kulturwissenschaft. Germanistische Reihe	Bd. 59			€		\N
84	ü											€		\N
21	Re-contextualized homogeneous complexity	Alvah Rosenbaum	Hauck-[k]Roberts[/k]	[i]Djibouti[/i]	26.89	Sed aut ut repellendus.	833	Compatible incremental time-frame	1	13.05.2004	6201888462841	€	DVD	admin
81	[i]Essential [/i]Statistics for Applied Linguistics. 	[k]Lowie[/k], [k]Wander[/k]	[k]Palgrave Macmillan[/k]	Houndmills	21,65		168			2012		€		bettina
15	[i]Total[/i] value-added GraphicalUserInterface	Prof. [k]Leanna Beatty[/k] Sr.	Rogahn-Dare	Poland	85.62		120	Down-sized 3rdgeneration adapter	8	05.11.1998	8709660096591	€		Marina
79	k-hg–gh						55—56	1–2				€		Marina
85	Niedersächsisches Wörterbuch. Lfg. 68 (IX, 5): perdaff – pliffen Bearbeiter [k]Martin Schröder[/k]. Sp. 513–640	[k]Busch[/k], [k]Albert [/k](Hg.)	Wachholtz	Kiel						2015		€		bettina
86	Language Change. Progress or Decay?	[k]Aitchison[/k], [k]Jean[/k]	Cambridge University Press	Cambridge	21,99		298			2013		€		bettina
88	Sprachwandel und soziale Systeme	Zeige, Lars	Olms	Hildesheim [u. a.]	39,80		XXIV, 289	Germanistische Linguistik. Monographien	27	2011		€		bettina
\.


--
-- Name: books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('books_id_seq', 88, true);


--
-- Data for Name: bugreports; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY bugreports (id, date, content, author, in_reply_to, done, hidden) FROM stdin;
14	2015-08-18	Was mir noch aufgefallen ist, manchmal verschwinden bei den BÃ¼chern oder bei den Rezensenten diese Trennstriche.... 	Gala 	\N	\N	\N
18	2015-08-18	Wo ich vielleicht auch noch eine Sicherung reinmachen wÃ¼rde ist nach der Aktion VerÃ¶ffentlicht. Ich habe grade mal danach noch einen Vorgang eingefÃ¼gt wie zb Abgesagt und das geht natÃ¼rlich, ist aber unlogisch, damit das vielleicht nciht mal irrtÃ¼mlicherweise passiert, kÃ¶nnte man da eine Sperre oder so einfÃ¼gen??	Gala	\N	\N	\N
15	2015-08-18	Ich habe jetzt eine Rezension einem Heft zugeordnet, das taucht auch bei Rezensionen im Verlauf auf, nur mÃ¼sste dann das vielleicht noch mit der Rezensenten Liste irgendwie verknÃ¼pft werden, damit dann beim Rezensenten bei VerÃ¶ffentlicht auch die Heftnummer steht und die Zahl um einen ZÃ¤hler hochgeht. Geht das?	Gala	\N	t	t
20	2015-08-18	Das gelb verschwindet nicht zu grÃ¼n, wenn ich das Buch vergebe.	Gala	\N	t	t
24	2015-08-20	Punkte 13, 19 und 20 sind erledigt	Tobias	\N	t	t
16	2015-08-18	Genau, und mir fÃ¤llt noch ein, dass ja dann das Buch bei dem Rezensenten: Hat zur Zeit X bei der VerÃ¶ffentlichung (also bei Heftzuweisung) auch verschwinden sollte	Gala	\N	t	t
19	2015-08-18	Wenn ich ein vorher vergebenes Buch erneut einem Rezensenten zuweise (also ein gelbes Buch), dann taucht das in den Rezensionen zweimal auf, einmal ohne Rezensent, also letzter Vorgang dan Rezensent abgesprungen und dann noch einmal mit dem neu zugewiesenen Rezensenten.	Gala	\N	t	t
13	2015-08-18	KÃ¶nnte man das bei einem Autorenwechsel vielleicht so machen, dass dann bei dem Autor der abgibt das Abgelehntfeld 1 hochgeht, weil theoretisch ist das ja abgelehnt!	Gala 	\N	t	t
30	2015-08-25	Wenn ein Buch einem neuen Rezensenten zugewiesen wird, müsste bei "angefragt" beim Rezensenten +1 stehen	Marina	\N	t	t
35	2015-08-25	Bei Rezensionen gibt es einige Bücher ohne Rezensenten (bzw. steht da einfach ein Bindestrich). Ist das ein Fehler? Eigentlich sollten in der Liste die anderen beiden ja verknüpft sein.\r\nEdit: Ich habe gerade ein neues Buch erstellt und es dann einem Rezensenten zugewiesen, den ich in diesem Schritt erst erstellt habe. Dann habe ich direkt auf "diesem Rezensenten zuweisen" geklickt. Als Rezensent erschien dann der Bindestrich und der Rezensent selbst wurde nicht gespeichert. Könnte man speichern dort noch als Zwischenschritt einfügen? Also dass der Rezensent automatisch gespeichert wird wenn man auf "diesem Rezensenten zuweisen" klickt?	Marina	\N	\N	t
27	2015-08-21	15 + 16: done	Tobias	\N	t	t
31	2015-08-25	Bei den Rezensionen: Könnte man die farblich sortieren? Also oben in einer Farbe die, die aktuell sind (d.h. noch nicht veröffentlicht), und unten in einer anderen Farbe die veröffentlichten sortiert nach Heftnummer (aktuellste oben).	Marina	\N	\N	\N
8	2015-08-18	Ah, und ich sehe grade auch hier funktionieren die Umlaute nicht. Das wÃ¤re sehr wichtig bald zu klaeren!!	Gala	\N	t	t
32	2015-08-25	Könnte bei den Listen die Kopfzeile fixiert werden? Zb. beim scrolln von der Rezensentenliste?	Gala 	\N	\N	\N
36	2015-08-25	Wenn man eine Rezension unter "ZDL" bearbeitet, wäre es vielleicht gut wenn die Haken bleiben würden, damit wenn man das nächste Mal darauf klickt man sieht, welche Schritte schon erledigt sind. Wäre das möglich?	Marina	\N	\N	\N
37	2015-08-26	Zu 35: Man kann es umgehen, indem man den Rezensenten zuerst speichert. Es ist also nicht so wichtig, aber wenn es nicht zu kompliziert ist, wäre es super, wenn speichern noch als zwischenschritt eingebaut werden würde :)	Marina	\N	\N	t
23	2015-08-19	Umlaute sollten jetzt klappen :)\r\näöü	Tobias	\N	t	t
38	2015-08-26	Würde es gehen, dass die Bücher innerhalb ihrer jeweiligen Kategorie alphabetisch sortiert sind? Momentan kann ich keine Sortierung erkennen..	Marina	\N	t	t
39	2015-08-26	Wenn ein Buch den Rezensenten wechselt, ist der erste Rezensent nicht mehr erkennbar.\r\nBsp:\r\nVorgang: Neu vergeben an Prof. Dr. Hans Xyz | vom: 26.08.15\r\nVorgang: Absage | vom: 26.08.15 | Bemerkung: Befangenheit\r\nVorgang: Neue Rezension | vom: 26.08.15\r\n\r\nZu Beginn steht nur neue Rezension und Absage, so kann man im Nachhinein gar nciht emhr nachvollziehen wer abgesagt hat.. Könnte da entweder noch ein "vergeben an XY" oder ein "Absage von XY" eingefügt werden?	Marina	\N	t	t
6	2015-08-12	Rezensenten werden jetzt nach Nachnamen sortiert	Tobias	\N	t	t
11	2015-08-18	Mir ist grad noch aufgefallen, dass wir bei graden Geldbetraegen ja den Betrag und dann ein KOmma plus wieder den gedankenstrich machen, der sollte also auch neben dem Pfundzeichen auftauchen. 	Gala	\N	t	t
29	2015-08-25	Könnte sich das Feld zum bearbeiten der Rezensenten in der Rubrik Rezensent vielleicht nur dann öffnen, wenn man auf den Namen klickt? Das ist irgendwie nervig, wenn man sobald man auf den Monitor klickt sich dieses Feld öffnet... das wäre cool!	Gala	\N	t	t
7	2015-08-18	Bei Buch spreichern:\r\n1. Wenn man das Pfund zeichen setzt, dann verschwindet zwar das Eurozeichen, aber es tauschen andere komische zeichen auf! Bitte prÃ¼fen.\r\n2. Bei Buch speichern: Auf einmal taucht hinter der normalen Maske, wo man die Daten eingeben muss, zweimal ein Fenster auf, wo das Buch mit Autor nochmal steht. (Sorry, ich kanns nicht anders erklÃ¤ren)\r\n3. Wenn man z.B. keinen Reihentitel oder Nummer angibt, dannn wird das trotzdem als (.) gesetzt. Das sollte am besten aber gar nicht auftauchen, damit man das in der Datenansicht nicht imer rauslÃ¶schen muss.	Gala	\N	t	t
12	2015-08-18	In dem Buch von Anne Jaeger muss das bekommen und das zu im Titel kursiviert werden. Das Ã¼bernimmmt die DB aus Word nicht und wenn ich das via Tastatur eingeben mÃ¶chhte, dann Ã¶ffnet sich unten so ein Fenster anstatt die Ã¼blichen Kursivierung.	Gala	\N	t	\N
33	2015-08-25	Nach Heftnummern suchen, bitte die Variante 1, also suchen, bei letztem Schritt "Heft veröffentlicht".	Gala	\N	t	t
40	2015-08-26	Bei der alphabetischen Sortierung der Rezensenten ist mir aufgefallen, dass ä,ö und ü erst nach z kommen.	Marina	\N	t	t
17	2015-08-18	Bei den Rezensionen stimmt irgendwas nicht mit ausblenden und wiederanzeigen funktion. Ich finde auch, dass man, wenn man die benutzt eigentlich nur ausblenden und alle anzeigen braucht oder? Oder braucht man das Ãberhaupt?? Ich finde das eher verwirrend. Gerade weil dieses wiederanzeigen nciht klappt. Was ich viel cooler fÃ¤nde wÃ¤re so eine Suchfunktion, wo man nach Heften filtern kann, oder wo man zb alle einblenden kann, die grade formatiert werden oder lektoriert etc. Damit man einen Ãberblick hat.	Gala	\N	t	\N
10	2015-08-18	Na toll... der wird auch nicht angezeigt. Strg und minus bei word!	Gala	\N	t	\N
45	2015-08-26	Könntet ihr bitte 9, 10 und 14 nochmal testen? Es kann sein, dass das mit den Umlauten gefixt wurde	Tobias	\N	t	\N
41	2015-08-26	Ich finde es ein bisschen störend, dass wenn man ein neues Buch erstellt, das in einer anderen Reihenfolge ist, als in der ZDL.\r\nBsp: bei neues Buch: Titel, Autor, Verlag, Verlagsort etc.\r\nin der ZDL: Autor, Jahr, Titel, Verlagsort, etc.\r\nIch finde es bei der Eingabe von alten \r\nRezensionen etwas umständlich. Da muss man dann immer so hin und her springen. Wenn es einfach zu ändern ist, wäre das super!	Marina	\N	t	t
42	2015-08-26	Nochmal zu 35:\r\nIch habe ein Buch erstellt, einem (bereits bestehenden) Rezensenten zugewiesen, dann alle Vorgänge abgehakt und gespeichert und schließlich das Buch einer Zeitschrift zugewiesen. Vor dem letzten schritt stand der Rezensent immer richtig da, doch sobald ich es einer Zeitschrift zugewiesen hatte, verschwand der Name des Rezensenten und stattdessen stand dort jener Bindestrich.	Marina	\N	t	t
43	2015-08-26	7.1 und 7.3 erledigt, 7.2 kann ich nicht nachvollziehen.\r\nDie "Medien" Position ist nur provisorisch, ich finde die richtige Position gerade nicht in meinen Notizen...	Tobias	\N	t	t
44	2015-08-26	Der Bindestrich bei den Rezensionen ist ein Workaround für "Rezensent hat zur Zeit:".\r\nWird noch geändert.	Tobias	\N	t	t
47	2015-09-02	Zu 38: funktioniert doch! Wenn ich zuerst die Bücher nach Alphabet sortiere und dann nach Status.	Marina	\N	t	t
49	2015-09-02	Könnten beim Rezensenten auch die Titel erscheinen, die er bisher rezensiert hat? Also nicht nur die Zahl der Veröffentlichungen, sondern nochmal eine extra Spalte, in der die Bücher nochmal aufgelistet werden?	Marina	\N	t	t
50	2015-09-02	Auf der Bücherseite haben sich die Schaltflächen rechts über die Statusspalte geschoben.	Marina	\N	\N	t
48	2015-09-02	Der Rezensentenwechsel funktioniert irgendwie gerade nicht mehr!	Marina	\N	t	t
53	2015-10-07	Ich habe das Gefühl, dass die DB etwas langsamer als vorher ist. Sonst klappt soweit alles super :)	Marina	\N	\N	\N
54	2015-10-12	Bei den Büchern werden unter "Daten anzeigen" noch die falschen Anführungszeichen (beide oben statt unten und oben) angezeigt	Bettina	\N	\N	\N
56	2015-10-12	Momentan kann z. B. beim Anlegen eines Buches das geschützte Leerzeichen nicht eingegeben werden, dies wäre aber bei manchen Verlagsorten wichtig, ebenso zwischen dem €-Zeichen und dem Preis	Bettina	\N	\N	\N
57	2015-10-16	Anlegen eines Vorgangs bei "Rezensionen": könnte Datumsangabe (Kalenderfunktion) bei "Deadline" und "Rezensionseingang" nach deutschem Standard (Reihenfolge TTMMJJJJ) angezeigt werden?	Bettina	\N	t	\N
55	2015-10-12	Wenn ich die Buchangaben unter "Daten anzeigen" in Word kopiere, erscheint der Text in Word grau hinterlegt, kann das geändert werden?	Bettina	\N	t	\N
9	2015-08-18	Nochmal bei Buch speichern: Die langen Gedankenstrich, die du eingefügt hast, werden auch nach der Speicherung zerschossen. Ich hab den Strich auch nochmal bei Word eingegeben und es handelt sich leider immer noch nicht um den Gedankenstrich, sondern der ist ein bisschen lÃ¤nger in der DAtenbank. Zum Vergleich: â das ist der ders sein soll!\r\n 	Gala	\N	t	\N
51	2015-09-03	Habe die Striche getestet und sie sind leider zu weit :( könntest du dann bitte, lieber tobias, die strg. minus striche einfügen? Und ich habe grade beim reinkopieren in Word gemerkt (bei Bücher und dann Daten anzeigen), dass die alternativen Schreibweisen verschwunden sind, es wird nur noch die Mitte angezeigt... hatten wir das gelöscht??	Gala	\N	t	\N
58	2015-10-16	Kurzer Hinweis: meistens markiere ich Punkte als erledigt sobald sie lokal behoben sind. Es dauert dann oft noch ein, zwei Tage bis ich die Änderungen auch hier vornehme.	Tobias	\N	t	\N
60	2015-10-28	Bei Titel, Reihe und Preis ist es der richtige Gedankenstrich. Bei der Seitenangabe sollte es aber der gleiche sein, der jetzige ist noch zu lang.	Marina	\N	t	\N
59	2015-10-28	Kapitälchen und kursiv funktioniert, super!! :)	Marina	\N	t	\N
\.


--
-- Name: bugreports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('bugreports_id_seq', 60, true);


--
-- Data for Name: email; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY email (id, revid, address, type) FROM stdin;
14	8	igoldner@example.org	private
15	8	ykuphal@example.com	other
16	8	hreinger@example.com	work
17	8	ezekiel.koss@example.com	private
18	9	hulda.kub@example.com	other
19	9	mylene75@example.org	private
20	9	kilback.nicolette@example.com	other
21	9	alene90@example.com	other
22	9	nathanial.jaskolski@example.org	other
23	10	bbogan@example.net	work
24	10	rice.amanda@example.com	work
25	10	kerluke.ressie@example.org	private
26	10	champlin.imogene@example.net	private
27	11	mitchell.adaline@example.org	private
28	11	vickie.berge@example.net	private
29	12	elittel@example.com	private
30	12	geovany.doyle@example.com	work
31	12	abbott.kirstin@example.com	private
32	13	braynor@example.org	private
33	13	bogisich.kenyatta@example.org	other
34	13	grayson57@example.net	work
35	13	jean.cremin@example.net	work
36	13	hermiston.jonas@example.net	private
37	14	iziemann@example.com	work
38	14	ashlee.o'hara@example.com	private
39	14	huel.bobby@example.org	other
40	14	janessa.boehm@example.com	work
41	15	sherman76@example.net	private
42	15	hhand@example.net	work
43	15	kulas.ottilie@example.org	work
44	15	gianni99@example.net	private
45	16	arch.terry@example.net	work
46	16	eduardo16@example.com	other
49	18	adams.noemie@example.org	other
50	18	qziemann@example.org	private
51	18	gabrielle59@example.net	other
52	18	kelton.ward@example.org	other
53	18	qgreenfelder@example.com	private
54	19	macejkovic.gregorio@example.net	work
55	19	hegmann.martine@example.com	private
56	20	adelbert89@example.com	work
57	20	shirley28@example.com	work
58	20	tyrell38@example.com	other
59	20	nellie.kreiger@example.org	private
60	20	jerrold46@example.org	work
61	21	hal.o'connell@example.com	private
62	21	parker.shakira@example.org	work
63	21	shakira.jaskolski@example.net	work
64	21	lillie58@example.com	other
65	22	tessie62@example.com	private
66	22	boehm.savion@example.com	other
67	22	kemmer.theo@example.org	work
68	22	lukas.fisher@example.org	private
69	23	yost.cordell@example.com	other
70	23	xmurray@example.org	work
71	23	vince.smitham@example.com	other
72	23	dorothy96@example.com	work
73	24	stoltenberg.whitney@example.net	other
74	24	robb45@example.net	private
75	24	icarter@example.net	work
76	25	kihn.margarita@example.com	other
77	25	oturner@example.org	private
78	25	dovie83@example.com	private
79	25	branson.kuhn@example.com	private
80	26	larkin.afton@example.org	work
81	26	mikayla.gusikowski@example.org	private
82	27	gwill@example.net	private
83	27	rod06@example.net	other
84	28	gmayert@example.net	work
85	28	sally58@example.com	private
86	28	anikolaus@example.net	private
87	28	labadie.holly@example.org	private
88	28	dwyman@example.org	work
89	29	tillman.reinger@example.net	private
90	29	anthony.boehm@example.net	private
91	30	sierra.cole@example.net	other
92	30	carolyne.carter@example.net	other
93	31	flo13@example.com	work
94	31	sernser@example.net	work
95	31	yost.keely@example.net	private
96	31	mcdermott.tony@example.org	work
97	31	ihalvorson@example.org	private
98	32	jeff92@example.com	other
99	32	braeden82@example.com	private
100	32	zemlak.ruben@example.net	other
101	36	ubailey@example.com	work
102	36	marion34@example.com	other
103	36	tthiel@example.com	private
104	36	sabina23@example.org	other
105	37	karolann.leannon@example.org	other
106	37	medhurst.kristian@example.org	work
107	37	nconnelly@example.org	private
108	37	destin12@example.net	private
109	38	lmurphy@example.net	private
110	38	mante.xavier@example.com	private
111	38	ksmitham@example.net	work
112	38	eduardo.thiel@example.org	private
113	38	abuckridge@example.org	private
114	39	delores92@example.com	work
115	39	camren98@example.com	private
116	39	gertrude.auer@example.net	work
117	40	tleffler@example.net	private
118	40	hauck.jany@example.com	work
119	40	schumm.carmela@example.net	other
120	41	ujkl,	
124	43	s5ttrfg6r5	standard
125	43		
126	43		
127	44	sdf	standard
132	49		standard
133	50		standard
134	54	vhrxdsgfg	standard
136	56	kr@web.de	standard
135	55		standard
121	42		standard
122	42		
123	42		
137	56	bwb@kmf.badw.de	standard
48	17	aflatley@example.com	private
138	57	abs@web.de	standard
140	59		standard
141	60	grjbköfd nlvnldnv	standard
142	60	jböjfadböjkgblödfgn	other
144	62		standard
145	63	schafroth@phil-fak.uni-duesseldorf.de	standard
146	64	andreas.opitz @ uni-leipzig.de	standard
147	65	elmar.seebold@lrz.uni-muenchen.de	standard
139	58		standard
143	61		standard
148	66		standard
149	67		standard
150	68		standard
151	59		standard
47	17	judson.feest@example.net	work
152	69		standard
153	70		standard
154	71	max.mustermann@mustermail.de	standard
155	72	wischer@uni-potsdam.de	standard
156	73		standard
\.


--
-- Name: email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('email_id_seq', 156, true);


--
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY events (reviewid, type, "timestamp", date, revid, payload, id, acct) FROM stdin;
2	Neue Rezension	2015-03-09 19:05:09.344	\N	28	\N	1	\N
3	Neue Rezension	2015-03-09 19:13:22.179	\N	11	\N	2	\N
2	Absage	2015-03-10 22:48:03.849	\N	\N	will doch nicht	3	\N
3	Wechsel	2015-03-10 22:49:58.765	\N	40	keine Zeit, hat Alternative vorgeschlagen	4	\N
3	Abgabetermin	2015-03-10 22:53:01.32	2015-06-20	\N	\N	5	\N
2	none	2015-03-11 03:30:11.326	\N	\N	\N	7	\N
4	Neue Rezension	2015-03-12 12:29:50.953	\N	24	\N	8	\N
5	Neue Rezension	2015-03-12 12:33:56.696	\N	11	\N	9	\N
56	Rezensionseingang	2015-10-29 17:14:01.244333	2015-10-29	13	test	188	admin
7	Neue Rezension	2015-03-14 22:02:53.014	\N	17	\N	11	\N
42	Veröffentlicht in Heft 81/2	2015-10-29 17:18:27.860845	\N	58		190	admin
61	Wechsel des Rezensenten	2015-10-30 11:09:52.237918	\N	73	Wechsel von Marina Frank zu Oliver Ernst | 	194	bettina
3	Heft_zugeordnet	2015-08-12 10:15:36.867178	\N	40		18	\N
4	Heft_zugeordnet	2015-08-12 10:15:36.956927	\N	24		20	\N
5	Heft_zugeordnet	2015-08-12 10:15:36.996917	\N	11		21	\N
3	Mahnung	2015-08-12 10:23:36.689682	\N	40		22	\N
7	Wechsel des Rezensenten	2015-08-12 10:24:19.945508	\N	14	Wechsel von Prof. Alivia Brakus zu Gudrun Walter | 	23	\N
2	Wechsel des Rezensenten	2015-08-12 10:24:44.820062	\N	18	Wechsel von zu Prof. Kattie Schulist | 	24	\N
7	Rezension angekommen	2015-08-12 10:27:15.486127	2015-08-12	14	testz	25	\N
7	Rezension angekommen	2015-08-12 10:27:29.43474	2015-08-12	14	Angekommen! Speicherort:  | testz2	26	\N
7	zum Setzen	2015-08-12 10:30:12.741739	\N	14	Erledigt! 	27	\N
7	Lektoriert	2015-08-12 10:30:13.392256	\N	14	Erledigt! 	28	\N
42	Neue_Rezension	2015-08-12 12:20:42.896957	\N	54	\N	29	\N
42	Mahnung	2015-08-12 13:38:24.522525	\N	54		30	\N
44	Neue_Rezension	2015-08-18 11:06:43.006946	\N	55	\N	31	\N
44	Wechsel des Rezensenten	2015-08-18 11:10:56.659936	\N	56	Wechsel von Gala Mehic zu Prof. Dr.  Anthony Rowley | 	32	\N
44	Mahnung	2015-08-18 11:11:34.677487	\N	56	nicht rechtzeitig abgegeben	33	\N
44	Umbruch	2015-08-18 11:14:33.158659	\N	56	Erledigt! 	34	\N
44	Imprimatur	2015-08-18 11:14:33.917364	\N	56	Erledigt! 	35	\N
44	zur. vom Setzen	2015-08-18 11:14:34.528338	\N	56	Erledigt! Seitenzahl: 	36	\N
44	zum Setzen	2015-08-18 11:14:35.211773	\N	56	Erledigt! 	37	\N
44	Lektoriert	2015-08-18 11:14:36.596797	\N	56	Erledigt! 	38	\N
44	Formatiert	2015-08-18 11:14:38.641232	\N	56	Erledigt! 	39	\N
44	Rezension angekommen	2015-08-18 11:14:39.437418	2015-08-18	56	Angekommen! Speicherort: 	40	\N
61	Mahnung	2015-10-30 11:27:56.83804	\N	73		204	bettina
42	Wechsel des Rezensenten	2015-08-20 13:08:19.826684	\N	12	Wechsel von 123123 zu Jazmin Lindgren | öäü	49	\N
7	Veröffentlicht in Heft 82/3 (2014)	2015-08-18 10:58:39.967125	\N	14		50	\N
42	Veröffentlicht in Heft 82/3 (2014)	2015-08-18 10:58:40.027056	\N	54		51	\N
44	Veröffentlicht in Heft 82/3 (2014)	2015-08-18 11:15:14.515163	\N	56		52	\N
42	Absage	2015-08-25 09:24:43.894294	\N	12	Buch ist zu schlecht	69	\N
42	Neu vergeben an Kathy Rys 	2015-08-25 09:25:30.04575	\N	57	\N	70	\N
46	Neue Rezension	2015-08-25 11:48:16.884151	\N	24	\N	73	\N
47	Neue Rezension	2015-08-25 12:36:45.644115	\N	0	\N	74	\N
47	Wechsel des Rezensenten	2015-08-25 12:44:33.224126	\N	58	Wechsel von - zu Marina Frank | 	75	\N
47	Rezensionseingang	2015-08-25 12:53:47.678457	2015-08-25	58	Angekommen! Speicherort: 	76	\N
47	Formatiert	2015-08-25 12:53:48.997539	\N	58	Erledigt! 	77	\N
48	Neue Rezension	2015-08-26 12:00:13.044122	\N	59	\N	78	\N
49	Neue Rezension	2015-08-26 12:14:05.264111	\N	60	\N	79	\N
49	Absage	2015-08-26 12:14:53.514288	\N	60	Befangenheit	80	\N
49	Neu vergeben an Prof. Dr. Hans Xyz 	2015-08-26 12:15:24.305838	\N	60	\N	81	\N
49	Absage	2015-08-26 12:17:23.234309	\N	60	Keine Zeit	82	\N
49	Neu vergeben an Prof. Alivia Brakus 	2015-08-26 12:17:38.305827	\N	17	\N	83	\N
3	zum_setzen	2015-08-26 12:22:19.353717	\N	40		84	\N
2	zum_setzen	2015-08-26 12:22:19.437705	\N	18		85	\N
3	zum_setzen	2015-08-26 12:22:29.836766	\N	40		86	\N
2	zum_setzen	2015-08-26 12:22:29.884204	\N	18		87	\N
3	vom_setzen	2015-08-26 12:22:47.664508	\N	40		88	\N
2	vom_setzen	2015-08-26 12:22:47.724099	\N	18		89	\N
3	vom_setzen	2015-08-26 12:22:55.564317	\N	40		90	\N
2	vom_setzen	2015-08-26 12:22:55.624795	\N	18		91	\N
48	zum_setzen	2015-08-26 12:23:05.729342	\N	59		92	\N
48	zum_setzen	2015-08-26 12:23:11.271198	\N	59		93	\N
46	zum_setzen	2015-08-26 12:23:23.878305	\N	24		94	\N
2	Veröffentlicht in Heft 83	2015-10-29 17:12:55.091234	\N	18		187	admin
56	Veröffentlicht in Heft 87/4	2015-10-29 17:15:52.948523	\N	13		189	admin
60	Neue Rezension, vergeben an Jan Berns 	2015-10-30 09:25:19.653924	\N	71	\N	191	bettina
61	Neue Rezension, vergeben an Marina Frank 	2015-10-30 10:46:53.975953	\N	58	\N	193	bettina
61	Neue Deadline	2015-10-30 11:15:57.233403	2015-04-30	73		195	bettina
61	Mahnung	2015-10-30 11:28:19.014775	\N	73	2. Mahnung 11.10.2015	205	bettina
42	Veröffentlicht in Heft 548654	2015-09-02 08:57:22.113435	\N	57		134	\N
55	Neue Rezension, vergeben an 123123 	2015-09-02 09:13:15.473123	\N	54	\N	135	\N
55	Rezensionseingang	2015-09-02 09:24:36.404959	2015-09-02	54	Angekommen! Speicherort: 	136	\N
55	Formatiert	2015-09-02 09:24:37.668925	\N	54	Erledigt! 	137	\N
56	Neue Rezension, vergeben an Raquel Cummings 	2015-09-02 09:33:23.505334	\N	36	\N	138	\N
56	Mahnung	2015-09-02 09:33:50.293354	\N	36		139	\N
49	Absage von Prof. Alivia Brakus 	2015-09-02 10:01:34.397723	\N	17		140	\N
4	Rezensionseingang	2015-09-02 10:03:21.477946	2015-09-02	24	Angekommen! Speicherort: 	141	\N
4	Formatiert	2015-09-02 10:03:22.106629	\N	24	Erledigt! 	142	\N
4	Lektoriert	2015-09-02 10:03:22.785726	\N	24	Erledigt! 	143	\N
4	zum Setzen	2015-09-02 10:03:24.248738	\N	24	Erledigt! 	144	\N
4	zur. vom Setzen	2015-09-02 10:03:26.879979	\N	24	Erledigt! Seitenzahl: 	145	\N
4	Imprimatur	2015-09-02 10:03:28.870042	\N	24	Erledigt! 	146	\N
4	Umbruch	2015-09-02 10:03:29.47975	\N	24	Erledigt! 	147	\N
55	Rezensionseingang	2015-09-02 10:17:15.504562	2015-09-02	54	Angekommen! Speicherort: 	148	\N
4	Rezensionseingang	2015-09-02 10:17:34.784115	2015-09-02	24	uzft	149	\N
46	Veröffentlicht in Heft 5465465	2015-09-02 11:07:43.335518	\N	24		150	\N
44	Absage von Prof. Dr.  Anthony Rowley 	2015-09-02 11:19:21.162927	\N	56	Buch ist zu schlecht	151	\N
44	Neu vergeben an Prof. Molly Cronin 	2015-09-02 11:19:44.926957	\N	15	\N	152	\N
56	Wechsel des Rezensenten	2015-09-02 18:18:22.015313	\N	13	Wechsel von Raquel Cummings zu Prof. Otilia Conroy | 	153	\N
55	Wechsel des Rezensenten	2015-09-03 13:44:07.425315	\N	55	Wechsel von 123123 zu Gala Mehic | 	154	\N
42	Wechsel des Rezensenten	2015-10-07 03:15:24.803476	\N	58	Wechsel von Kathy Rys zu Marina Frank | 	155	\N
55	Imprimatur	2015-10-07 03:50:55.71692	\N	55	Erledigt! 	156	\N
55	Umbruch	2015-10-07 03:50:55.90962	\N	55	Erledigt! 	157	\N
55	Neue Deadline	2015-10-07 03:51:06.955693	2016-04-07	55		158	\N
44	Neue Deadline	2015-10-07 03:51:37.5955	2015-10-07	15		159	\N
44	Absage von Prof. Molly Cronin 	2015-10-07 03:52:08.809744	\N	15	Buch ist zu schlecht	160	\N
57	Neue Rezension, vergeben an Marina Frank 	2015-10-07 05:53:27.930424	\N	58	\N	161	\N
57	Mahnung	2015-10-07 05:57:23.211309	\N	58		162	\N
57	Wechsel des Rezensenten	2015-10-07 05:59:20.120736	\N	14	Wechsel von Marina Frank zu Gudrun Walter | 	163	\N
58	Neue Rezension, vergeben an Prof. Kattie Schulist 	2015-10-12 03:34:32.897048	\N	18	\N	164	\N
57	Neue Deadline	2015-10-12 03:43:19.073303	2016-04-12	14	15.12.2015	165	\N
57	Mahnung	2015-10-12 03:45:41.311005	\N	14	letzte Mahnung per Brief	166	\N
59	Neue Rezension, vergeben an Mark Louden 	2015-10-12 04:04:10.42339	\N	69	\N	167	\N
59	Neue Deadline	2015-10-12 04:04:49.898444	2016-04-12	69		168	\N
59	Neue Deadline	2015-10-12 04:10:12.056177	2016-04-22	69	1 Monat vorher erinnern	169	\N
47	Neue Deadline	2015-10-14 04:44:37.073254	2016-05-16	58		170	\N
55	Absage von Gala Mehic 	2015-10-14 05:03:23.681447	\N	55	Buch ist zu schlecht	171	\N
5	Neue Deadline	2015-10-15 03:04:23.130143	2016-04-15	11		172	\N
5	Mahnung	2015-10-15 03:05:06.542354	\N	11	z	173	\N
5	Rezensionseingang	2015-10-15 03:05:25.628856	2015-10-15	11	Angekommen! Speicherort: 	174	\N
7	Neue Deadline	2015-10-15 03:06:12.153748	2016-04-15	14		175	\N
2	Mahnung	2015-10-16 04:01:38.824864	\N	18		176	\N
2	Neue Deadline	2015-10-16 04:02:01.890162	2016-04-16	18		177	\N
2	Rezensionseingang	2015-10-16 04:03:29.388387	2015-10-09	18		178	\N
59	Mahnung	2015-10-29 15:19:33.856645	\N	69	letzte Mahnung per Brief	183	bettina
3	Veröffentlicht in Heft 76/3	2015-10-29 17:05:13.135069	\N	40		184	admin
48	Veröffentlicht in Heft 76/3	2015-10-29 17:05:24.551358	\N	59		185	admin
\.


--
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('events_id_seq', 205, true);


--
-- Data for Name: fields; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY fields (id, name) FROM stdin;
1	Althochdeutsch
3	Computerlinguistik
4	Deutsch als Fremdsprache
5	Dialektologie
6	Dialektometrie
7	Friesisch
8	Frühneuhochdeutsch
9	Indogermanisch
10	Jiddistik
11	Klinische Linguistik
12	Lexikographie
13	Lexikologie
14	Luxemburgistik
15	Mittelhochdeutsch
16	Morphologie
17	Niederdeutsch
18	Niederlandistik
19	Oberdeutsch
20	Onomastik
21	Phonetik
22	Phonologie
23	Pragmatik
24	Prosodie
25	Romanistik
26	Semantik
27	Skandinavistik
28	Slavistik
29	Soziolinguistik
30	Sprachdidaktik
31	Sprache und Politik
32	Spracherwerb
33	Sprachgeschichte
34	Sprachinsel
35	Sprachkontakt
36	Sprachphilosophie
37	Sprachtheorie
38	Sprachtypologie
39	Syntax
40	Textlinguistik
41	Variationslinguistik
42	Zimbrisch
2	Anglistik
44	Etymologie
\.


--
-- Name: fields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('fields_id_seq', 44, true);


--
-- Data for Name: rev_has_fields; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY rev_has_fields (revid, fieldid) FROM stdin;
8	16
8	42
9	2
9	23
9	13
9	9
10	31
10	22
10	41
10	20
11	30
11	5
11	1
11	8
12	19
13	1
14	38
14	16
15	27
15	11
16	23
16	26
16	4
17	6
18	5
19	38
19	36
19	34
20	16
20	13
20	26
20	40
21	42
21	6
21	14
22	25
22	39
22	7
22	11
23	41
24	1
24	12
24	24
24	33
25	29
25	3
25	22
26	8
26	17
26	2
26	28
27	30
27	32
28	10
28	4
28	37
28	27
29	21
30	31
30	9
30	18
30	23
31	35
31	19
32	20
32	15
36	7
36	31
37	32
37	25
37	21
37	17
38	37
39	10
39	42
39	33
40	26
40	19
40	9
40	4
41	1
41	3
42	1
42	2
42	5
44	2
\.


--
-- Data for Name: reviewers; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY reviewers (id, firstname, lastname, title, gender, published, requests, declined, warned, note, published_in, form, content, formnote, contentnote, isblocked, acct) FROM stdin;
41	guvhmnb	ugvhj	kohujk	female	567	45	64	54	.fuz		25	25	form ok	inhalt ok	f	\N
23	Darrel	Hodkiewicz		male	1	0	0	0	Multi-layered explicit portal	88/99 (2025)	18	8	\N	\N	f	\N
0		-			0	1	1	0			0	0			f	\N
42	jftgjh	öuioöli	fdhgfg	female	56	78	345	657	dtzdz\r\nhujkfh		10	30	formell meh	inhalt ok	f	\N
63	Elmar	Schafroth	Dr.	male	3	3	0	0		75/1 (2008), 75/2 (2008), 81/1 (2014)	0	0			f	\N
47	fruppp	frapp	\N	\N	0	0	0	0	\N	\N	\N	\N	\N	\N	\N	\N
16	Dallas	Mosciski	Prof.	other	0	0	0	0	User-friendly coherent solution		13	21	\N	\N	f	\N
65	Elmar	Seebold	Prof. Dr.	male	4	4	0	0	zuverlässig, sehr genau, faßt sich auch kurz	71-1, 72-1, 68/1 (2001), 88/7 (64)	30	30			f	\N
19	Rachael	Rogahn	Prof.	other	0	0	0	0	Seamless needs-based initiative		5	21	\N	\N	f	\N
20	Royal	Kessler	Prof.	female	0	0	0	0	Implemented context-sensitive firmware		24	13	\N	\N	f	\N
21	Kevon	Hessel		other	0	0	0	0	\N		13	22	\N	\N	f	\N
22	Jazlyn	Daniel	Dr.	male	0	0	0	0	Universal homogeneous localareanetwork		16	18	\N	\N	f	\N
25	Rickey	O'Reilly	Dr.	other	0	0	0	0	Multi-lateral didactic openarchitecture		7	30	\N	\N	f	\N
61	Umlaut	üüüü		female	0	0	0	0			25	20			f	\N
66	sdftizg	küü			0	0	0	0			0	0			f	\N
68	sdfsdf	kv			0	0	0	0			0	0			f	\N
24	Dell	Lueilwitz		male	1	1	0	0	\N	5465465	11	7	\N	\N	f	\N
54	123123			female	0	1	4	1	aywetrestg		0	0			f	\N
10	Leonard	Doyle		other	2	0	0	0	\N	88/99 (2025), 99999	25	19	\N	\N	f	\N
12	Jazmin	Lindgren		female	0	0	1	0	\N		24	10	\N	\N	f	\N
26	Molly	Howe		female	0	0	0	0	\N		14	12	\N	\N	f	\N
27	Elisabeth	Huels	Dr.	other	0	0	0	0	Self-enabling contextually-based approach		10	16	\N	\N	f	\N
28	Britney	Schroeder	Dr.	other	0	0	0	0	\N		10	20	\N	\N	f	\N
29	Marcelle	Lubowitz	Prof.	male	0	0	0	0	Switchable dynamic architecture		26	8	\N	\N	f	\N
30	Cristobal	Maggio	Prof.	female	0	0	0	0	Optimized directional archive		7	28	\N	\N	f	\N
31	Kyleigh	Mante	Prof.	female	0	0	0	0	Enterprise-wide client-server service-desk		28	5	\N	\N	f	\N
32	Ransom	Morar		other	0	0	0	0	\N		12	20	\N	\N	f	\N
33	Josie	Jakubowski	Dr.	male	0	0	0	0	Up-sized needs-based productivity		30	24	\N	\N	f	\N
9	Boris	Kertzmann		male	0	0	0	0	\N		22	27	\N	\N	f	\N
8	Enrique	Jacobson	Prof.	female	0	0	0	0	\N		29	14	\N	\N	f	\N
37	Reilly	Gutmann		other	0	0	0	0	Programmable assymetric pricingstructure		30	28	\N	\N	f	\N
38	Gunnar	Terry		female	0	0	0	0	Persevering hybrid hardware		20	20	\N	\N	f	\N
39	Hayden	Rutherford		other	0	0	0	0	Triple-buffered actuating flexibility		25	30	\N	\N	f	\N
43	567zh	trg5e6uzj	trz	female	0	0	0	0			15	15			f	\N
44	he5h	he5tr	rtz	female	0	0	0	0	hrt		15	30			f	\N
49	quotetest	quotetest		female	0	0	0	0			15	15			\N	\N
50	dfsaet	ztrdzdrft		female	0	0	0	0			15	15			\N	\N
60	Hans	Xyz	Prof. Dr.	male	0	4	3	0	fdhgfxnxc	65/2 (1980)	20	30			f	\N
62		ää			0	0	0	0			0	0			f	\N
64	Andreas	Opitz		male	1	2	0	1	Mahnung\r\n6.12.13\r\nRez.eingang\r\n15.1.14\r\n	81/1 (2014)	0	0			f	\N
67	jhasdbfkj	kt			0	0	0	0			0	0			f	\N
56	Anthony	Rowley	Prof. Dr. 	male	1	1	1	2		77/2 (2010)	10	20			f	\N
36	Raquel	Cummings		female	0	1	1	1	\N		12	22	\N	\N	f	\N
57	Kathy	Rys		other	1	0	2	0		548654	20	30			f	\N
15	Molly	Cronin	Prof.	male	0	1	1	0	Synergistic disintermediate infrastructure		6	27	\N	\N	f	\N
17	Alivia	Brakus	Prof.	other	0	1	2	0	Total discrete methodology		17	23			f	\N
70	Mark	Louden		male	0	0	0	0			0	0			f	\N
18	Kattie	Schulist	Prof.	female	3	1	1	1	Realigned analyzing complexity	88/99 (2025), 83/1, 83	29	26	\N	\N	f	\N
55	Gala	Mehic		female	2	3	2	0		81/3 (2014), 83/1 (2015)	5	5			f	\N
11	Florence	Huels	Prof.	male	0	0	0	1	\N		24	7	\N	\N	f	\N
13	Otilia	Conroy	Prof.	other	2	2	0	0	Phased encompassing framework	575, 87/4	26	19	\N	\N	f	\N
14	Gudrun	Walter		female	0	1	0	1	Assimilated assymetric architecture		22	24	\N	\N	f	\N
58	Marina	Frank		female	1	4	7	1		81/2	5	20			f	\N
71	Jan	Berns		male	0	6	3	2		75 (1), 78(2)	15	30			f	bettina
69	Mark	Louden		male	0	1	0	1			0	0			f	\N
40	Blake	Hodkiewicz		female	2	0	0	0	Open-source optimal securedline	76/3, 76/3	22	24	\N	\N	f	\N
59	456				2	1	0	0		76/3, 76/3	0	0			f	\N
72	Ilse	Wischer		female	0	0	0	0			0	0			f	bettina
73	Oliver	Ernst		female	0	1	0	2			0	0			f	bettina
\.


--
-- Name: reviewers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('reviewers_id_seq', 73, true);


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY reviews (id, bookid, revid, acct, heft) FROM stdin;
4	17	24	\N	\N
5	16	11	\N	\N
7	61	14	\N	\N
47	71	58	\N	\N
49	72	\N	\N	\N
44	66	\N	\N	\N
57	82	14	\N	\N
58	20	18	\N	\N
59	83	69	\N	\N
55	69	\N	\N	\N
3	45	40	\N	76/3
48	65	59	\N	76/3
2	40	18	\N	83
56	77	13	\N	87/4
42	64	58	\N	81/2
46	62	24	\N	546
60	85	71	bettina	\N
61	88	73	bettina	\N
\.


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('reviews_id_seq', 61, true);


--
-- Data for Name: user_mgmt; Type: TABLE DATA; Schema: public; Owner: zdl
--

COPY user_mgmt (id, acct, pw, last_seen, role) FROM stdin;
8	Tobias	sha256:1000:UHSE+Jo8/0MRg0piFsegZrDtM0XpXtgD:eFBv+c8qJppZtAuw9rtfzmcPQjOt6EWR	2015-10-18 13:35:44.050745	editor
9	gsdf	sha256:1000:BdlZhsCxQswApajPVufTE9LJ2t31WhNN:31xqypN0eSSnp43gbSM3BpQvo9nkvXZx	2015-10-18 13:38:03.48473	editor
10	Marina	sha256:1000:iia9+nHSupj5Ho6wOX0olVr35AGtGZpK:O1NzjW0tlIvn/CylVJjzPYtGJxoY5Ool	2015-10-18 22:21:08.821858	editor
11	admin	sha256:1000:u0F9icPzz9UoL5RO40jRVz5U+KAu27kh:bq59RcPlL4iMtZQczcGaPJYZiREc01EW	2015-10-20 12:52:36.961371	editor
12	bettina	sha256:1000:kEpxGZ1mhWCepjeNnokK4l1s8h11hCD5:/JSQr3gB+EgmFM8ZiTxjeXEmU9t2mfWO	2015-10-21 11:38:23.11761	editor
13	Ganswindt	sha256:1000:HdZ00+nVFpkxpl1cInPTvuN6UIWh2Gyg:k5ipQ0lF8kHj/eiiMd8X3vCD7aytZAfh	2015-10-26 09:05:46.842452	editor
14	test	sha256:1000:uBL6FnnPoe/c2RuXtLSklabIVu0C+02r:WL3RS074wAyWw7NR+J1guA5CxddnI16O	2015-10-31 15:12:52.269893	editor
\.


--
-- Name: user_mgmt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zdl
--

SELECT pg_catalog.setval('user_mgmt_id_seq', 14, true);


--
-- Name: address_pkey; Type: CONSTRAINT; Schema: public; Owner: zdl; Tablespace: 
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: books_pkey; Type: CONSTRAINT; Schema: public; Owner: zdl; Tablespace: 
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: bugreports_pkey; Type: CONSTRAINT; Schema: public; Owner: zdl; Tablespace: 
--

ALTER TABLE ONLY bugreports
    ADD CONSTRAINT bugreports_pkey PRIMARY KEY (id);


--
-- Name: email_pkey; Type: CONSTRAINT; Schema: public; Owner: zdl; Tablespace: 
--

ALTER TABLE ONLY email
    ADD CONSTRAINT email_pkey PRIMARY KEY (id);


--
-- Name: fields_pkey; Type: CONSTRAINT; Schema: public; Owner: zdl; Tablespace: 
--

ALTER TABLE ONLY fields
    ADD CONSTRAINT fields_pkey PRIMARY KEY (id);


--
-- Name: reviewers_pkey; Type: CONSTRAINT; Schema: public; Owner: zdl; Tablespace: 
--

ALTER TABLE ONLY reviewers
    ADD CONSTRAINT reviewers_pkey PRIMARY KEY (id);


--
-- Name: reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: zdl; Tablespace: 
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: fki_address_revid_fkey; Type: INDEX; Schema: public; Owner: zdl; Tablespace: 
--

CREATE INDEX fki_address_revid_fkey ON address USING btree (revid);


--
-- Name: fki_email_revid_fkey; Type: INDEX; Schema: public; Owner: zdl; Tablespace: 
--

CREATE INDEX fki_email_revid_fkey ON email USING btree (revid);


--
-- Name: fki_event_revid_fkey; Type: INDEX; Schema: public; Owner: zdl; Tablespace: 
--

CREATE INDEX fki_event_revid_fkey ON events USING btree (revid);


--
-- Name: fki_rev_has_fields_revid_fkey; Type: INDEX; Schema: public; Owner: zdl; Tablespace: 
--

CREATE INDEX fki_rev_has_fields_revid_fkey ON rev_has_fields USING btree (revid);


--
-- Name: address_revid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_revid_fkey FOREIGN KEY (revid) REFERENCES reviewers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: email_revid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY email
    ADD CONSTRAINT email_revid_fkey FOREIGN KEY (revid) REFERENCES reviewers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: event_revid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY events
    ADD CONSTRAINT event_revid_fkey FOREIGN KEY (revid) REFERENCES reviewers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: event_reviewid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY events
    ADD CONSTRAINT event_reviewid_fkey FOREIGN KEY (reviewid) REFERENCES reviews(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rev_has_fields_fieldid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY rev_has_fields
    ADD CONSTRAINT rev_has_fields_fieldid_fkey FOREIGN KEY (fieldid) REFERENCES fields(id);


--
-- Name: rev_has_fields_revid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY rev_has_fields
    ADD CONSTRAINT rev_has_fields_revid_fkey FOREIGN KEY (revid) REFERENCES reviewers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reviews_bookid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_bookid_fkey FOREIGN KEY (bookid) REFERENCES books(id);


--
-- Name: reviews_revid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zdl
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_revid_fkey FOREIGN KEY (revid) REFERENCES reviewers(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: zdl
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM zdl;
GRANT ALL ON SCHEMA public TO zdl;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: user_mgmt; Type: ACL; Schema: public; Owner: zdl
--

REVOKE ALL ON TABLE user_mgmt FROM PUBLIC;
REVOKE ALL ON TABLE user_mgmt FROM zdl;
GRANT ALL ON TABLE user_mgmt TO zdl;


--
-- Name: user_mgmt.acct; Type: ACL; Schema: public; Owner: zdl
--

REVOKE ALL(acct) ON TABLE user_mgmt FROM PUBLIC;
REVOKE ALL(acct) ON TABLE user_mgmt FROM zdl;
GRANT SELECT(acct),INSERT(acct),UPDATE(acct) ON TABLE user_mgmt TO zdl;


--
-- Name: user_mgmt.pw; Type: ACL; Schema: public; Owner: zdl
--

REVOKE ALL(pw) ON TABLE user_mgmt FROM PUBLIC;
REVOKE ALL(pw) ON TABLE user_mgmt FROM zdl;
GRANT SELECT(pw),INSERT(pw),UPDATE(pw) ON TABLE user_mgmt TO zdl;


--
-- Name: user_mgmt.last_seen; Type: ACL; Schema: public; Owner: zdl
--

REVOKE ALL(last_seen) ON TABLE user_mgmt FROM PUBLIC;
REVOKE ALL(last_seen) ON TABLE user_mgmt FROM zdl;
GRANT SELECT(last_seen),INSERT(last_seen),UPDATE(last_seen) ON TABLE user_mgmt TO zdl;


--
-- Name: user_mgmt.role; Type: ACL; Schema: public; Owner: zdl
--

REVOKE ALL(role) ON TABLE user_mgmt FROM PUBLIC;
REVOKE ALL(role) ON TABLE user_mgmt FROM zdl;
GRANT SELECT(role),INSERT(role),UPDATE(role) ON TABLE user_mgmt TO zdl;


--
-- PostgreSQL database dump complete
--

