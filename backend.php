<?php

// struktur:
// if ($_POST["action"] == "login") {
// }elseif ($_POST["action"] == "register") {
// }elseif ($_POST["action"] == "change_pass") {
// }elseif ($_POST["action"] == "logout") {
// }elseif ($_POST["action"] == "loginstate") {
// }elseif ($_SESSION["loggedin"] && $_SESSION["role"] == ("editor" || "admin")) {
//   if($_POST["action"] == "books"){
//   }elseif($_POST["action"] == "booksearch"){
//   }elseif($_POST["action"] == "bookdata"){
//   }elseif($_POST["action"] == "bookupdate"){
//   }elseif($_POST["action"] == "bookdelete"){
//
//   }elseif($_POST["action"] == "reviewers"){
//   }elseif($_POST["action"] == "rersearch"){
//   }elseif($_POST["action"] == "rerdata"){
//   }elseif($_POST["action"] == "rerupdate"){
//
//   }elseif($_POST["action"] == "newreview") {
//   }elseif($_POST["action"] == "reviews"){
//   }elseif($_POST["action"] == "resupdate"){
//   }elseif($_POST["action"] == "resdata"){
//   }elseif($_POST["action"] == "delete_event"){
//
//   }elseif($_POST["action"] == "bugreports"){
//   }elseif($_POST["action"] == "mark_bugreport_done"){
//   }elseif($_POST["action"] == "hide_bugreport"){
//   }elseif($_POST["action"] == "delete_bugreport"){
//
//   }elseif($_POST["action"] == "admin"){
//   }elseif($_POST["action"] == "fieldupdate"){
//   }elseif($_POST["action"] == "fielddelete"){
// echo json_encode($out);

// review state reviews states rezension zustand rezensionen zustände zahlencode
// 0 "Absage von"
// 1 "Neu vergeben an"
// 1 "Neue Rezension"
// 1 "Wechsel des Rezensenten"
// 2 "Neue Deadline"
// 3 "Mahnung"
// 4 "Rezensionseingang"
// 5 "Formatiert"
// 6 "Lektoriert"
// 7 "zum Setzen"
// 8 "zur. vom Setzen"
// 9 "Imprimatur"
// 10 "Umbruch"
// 11 "Veröffentlicht in Heft"

header('Content-Type:text/html; charset=UTF-8');
/*
 * Password Hashing With PBKDF2 (http://crackstation.net/hashing-security.htm).
 * Copyright (c) 2013, Taylor Hornby
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// These constants may be changed without breaking existing hashes.
define("PBKDF2_HASH_ALGORITHM", "sha256");
define("PBKDF2_ITERATIONS", 1000);
define("PBKDF2_SALT_BYTE_SIZE", 24);
define("PBKDF2_HASH_BYTE_SIZE", 24);

define("HASH_SECTIONS", 4);
define("HASH_ALGORITHM_INDEX", 0);
define("HASH_ITERATION_INDEX", 1);
define("HASH_SALT_INDEX", 2);
define("HASH_PBKDF2_INDEX", 3);

function create_hash($password)
{
    // format: algorithm:iterations:salt:hash
    $salt = base64_encode(mcrypt_create_iv(PBKDF2_SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));
    return PBKDF2_HASH_ALGORITHM . ":" . PBKDF2_ITERATIONS . ":" .  $salt . ":" .
        base64_encode(pbkdf2(
            PBKDF2_HASH_ALGORITHM,
            $password,
            $salt,
            PBKDF2_ITERATIONS,
            PBKDF2_HASH_BYTE_SIZE,
            true
        ));
}

function validate_password($password, $correct_hash)
{
    $params = explode(":", $correct_hash);
    if(count($params) < HASH_SECTIONS)
       return false;
    $pbkdf2 = base64_decode($params[HASH_PBKDF2_INDEX]);
    return slow_equals(
        $pbkdf2,
        pbkdf2(
            $params[HASH_ALGORITHM_INDEX],
            $password,
            $params[HASH_SALT_INDEX],
            (int)$params[HASH_ITERATION_INDEX],
            strlen($pbkdf2),
            true
        )
    );
}

// Compares two strings $a and $b in length-constant time.
function slow_equals($a, $b)
{
  $diff = strlen($a) ^ strlen($b);
  for($i = 0; $i < strlen($a) && $i < strlen($b); $i++)
  {
      $diff |= ord($a[$i]) ^ ord($b[$i]);
  }
  return $diff === 0;
}

/*
 * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
 * $algorithm - The hash algorithm to use. Recommended: SHA256
 * $password - The password.
 * $salt - A salt that is unique to the password.
 * $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
 * $key_length - The length of the derived key in bytes.
 * $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
 * Returns: A $key_length-byte key derived from the password and salt.
 *
 * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
 *
 * This implementation of PBKDF2 was originally created by https://defuse.ca
 * With improvements by http://www.variations-of-shadow.com
 */
function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false)
{
  $algorithm = strtolower($algorithm);
  if(!in_array($algorithm, hash_algos(), true))
      trigger_error('PBKDF2 ERROR: Invalid hash algorithm.', E_USER_ERROR);
  if($count <= 0 || $key_length <= 0)
      trigger_error('PBKDF2 ERROR: Invalid parameters.', E_USER_ERROR);

  if (function_exists("hash_pbkdf2")) {
      // The output length is in NIBBLES (4-bits) if $raw_output is false!
      if (!$raw_output) {
          $key_length = $key_length * 2;
      }
      return hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
  }

  $hash_length = strlen(hash($algorithm, "", true));
  $block_count = ceil($key_length / $hash_length);

  $output = "";
  for($i = 1; $i <= $block_count; $i++) {
      // $i encoded as 4 bytes, big endian.
      $last = $salt . pack("N", $i);
      // first iteration
      $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
      // perform the other $count - 1 iterations
      for ($j = 1; $j < $count; $j++) {
          $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
      }
      $output .= $xorsum;
  }

  if($raw_output)
      return substr($output, 0, $key_length);
  else
      return bin2hex(substr($output, 0, $key_length));
}


/* mein code beginnt hier */


$dbconn = pg_connect("host=localhost port=5432 dbname=zdl user=zdl password=Y2ZY3undfZZsh5Mb8E connect_timeout=5");
$out = array();

session_start();
/*
$out["header"], $out["body"]
*/
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function sonderzeichen($string) {
  $search = array("Ä", "Ö", "Ü", "ä", "ö", "ü", "ß", "´");
  $replace = array("A", "O", "U", "a", "o", "u", "ss", "");
  return str_replace($search, $replace, $string);
}

if ($_POST["action"] == "login") {
  $account = pg_escape_string($_POST["user"]);
  $password_sent = pg_escape_string($_POST["password"]);
  $query = pg_query("select pw, role from user_mgmt where acct = '" . $account . "'");
  $password_db = pg_fetch_row($query);
  if (validate_password($password_sent, $password_db[0])) {
    $_SESSION["loggedin"] = true;
    $_SESSION["role"] = $password_db[1];
    $_SESSION["user"] = $account;
    $out["role"] = $password_db[1];
    $out["loggedin"] = true;
  } else {
    $out["error"] = "falsches Passwort";
  }
}elseif ($_POST["action"] == "register") {
  $account = pg_escape_string($_POST["user"]);
  $password_sent = pg_escape_string($_POST["password"]);
  $password = create_hash($password_sent);
  $query = pg_query("insert into user_mgmt (acct, pw, last_seen, role) values ('" . $account . "', '" . $password . "', 'NOW', 'editor')");
  $_SESSION["loggedin"] = true;
  $_SESSION["role"] = "editor";
  $_SESSION["user"] = $account;

  $out["html"] = pg_last_error();

}elseif ($_POST["action"] == "change_pass") {

  $account = pg_escape_string($_POST["user"]);
  $password_sent = pg_escape_string($_POST["old_pass"]);
  $query = pg_query("select pw from user_mgmt where acct = '" . $account . "'");
  $password_db = pg_fetch_row($query);
  if (validate_password($password_sent, $password_db[0])) {
    $new_pass = pg_escape_string($_POST["new_pass"]);
    $password = create_hash($new_pass);
    $query = pg_query("update user_mgmt set (pw, last_seen) = ('" . $password . "', 'NOW') where acct = '" . $account . "'");
    $out["error"] = pg_last_error();
  } else {
    $out["error"] = "falsches Passwort";
  }


}elseif ($_POST["action"] == "logout") {
  session_unset();
  session_destroy();
  $out["html"] = "Abgemeldet";

}elseif ($_POST["action"] == "loginstate") {
  if(isset($_SESSION["loggedin"])){
    $out["loggedin"] = $_SESSION["loggedin"];
    $out["user"] = $_SESSION["user"];
  }else {
    $out["loggedin"] = false;
  }

}elseif ($_SESSION["loggedin"] && $_SESSION["role"] == ("editor" || "admin")) {

  if($_POST["action"] == "books"){
	   $out["header"] = "
		   <form class='form-horizontal' role='form'>
			    <div class='form-group'>
  				<label class='control-label col-sm-1' for='titlesearch'>Titel</label>
  				<div class='col-sm-4'>
  					<input type='text' class='booksearch form-control' name='titlesearch' id='titlesearch'>
  				</div>
  				<label class='control-label col-sm-1' for='authorsearch'>Autor</label>
  				<div class='col-sm-4'>
  					<input type='text' class='booksearch form-control' name='authorsearch' id='authorsearch'>
  				</div>
  				<div class='col-sm-2'>
  					<button type='button' id='newbook_button' name='newbook_button' class='btn btn-success' data-toggle='modal' data-target='#bookmodal' data-bookid='0'>Neues Buch<span class='fa fa-arrow-right'></span></button>
  				</div>
  			</div>
			</form>
			<div class='modal fade' id='bookmodal' tabindex='-1' role='dialog' aria-labelledby='book_modal_label' aria-hidden='true'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
						<ul class='nav nav-tabs' id='tabContent'>
					    <li class='active'><a href='#modify_book_data' data-toggle='tab'><h4 class='modal-title' id='book_modal_label'>Daten &auml;ndern</h4></a></li>
					    <li><a href='#show_book_data' data-toggle='tab'><h4 class='modal-title' id='show_book_data_tab'>Daten anzeigen</h4></a></li>
						</ul>
				</div>
				<div class='modal-body tab-content'>
				  <div class='tab-pane active' id='modify_book_data'>
					<form class='form-horizontal' role='form'>

    <!-- author -->
		<div id='author-group' class='form-group'>
			<label class='control-label col-sm-2' for='author'>Autor</label>
			<div class='col-sm-10'>
				<input type='text' class='form-control book_inputs' name='author' id='author'>
			</div>
			<!-- errors will go here -->
		</div>

    <!-- pub_date -->
		<div id='pub_date-group' class='form-group'>
			<label class='control-label col-sm-2' for='pub_date'>Publikations-jahr</label>
			<div class='col-sm-3'>
				<input type='text' class='form-control book_inputs' name='pub_date' id='pub_date'>
			</div>
			<div class='col-sm-5 col-sm-offset-2'>
      <button id='nbsp_btn' type='button'>ges. Leer.</button>
      <button id='quotes_btn' type='button'>&#132; &#147;</button>
      <button id='kursiv_btn' type='button'><span class='glyphicon glyphicon-italic'></span></button>
      <button id='kapit_btn' type='button'><span class='glyphicon glyphicon-font'></span><span class='glyphicon glyphicon-font small_glyph'></span></button></div>
			<!-- errors will go here -->
		</div>

		<!-- title -->
		<div id='title-group' class='form-group'>
			<label class='control-label col-sm-2' for='title'>Titel</label>
			<div class='col-sm-7'>
				<input type='text' class='form-control book_inputs' name='title' id='title'>
			</div>
			<div class='col-sm-3'>
        <button onclick='addText(\"title\",\"&ndash;\");' type='button'>&ndash;</button>
			</div>
			<!-- errors will go here -->
		</div>

    <!-- pub_country -->
		<div id='pub_country-group' class='form-group'>
			<label class='control-label col-sm-2' for='pub_country'>Verlagsort</label>
			<div class='col-sm-10'>
				<input type='text' class='form-control book_inputs' name='pub_country' id='pub_country'>
			</div>
			<!-- errors will go here -->
		</div>

		<!-- publisher -->
		<div id='publisher-group' class='form-group'>
			<label class='control-label col-sm-2' for='publisher'>Verlag</label>
			<div class='col-sm-10'>
				<input type='text' class='form-control book_inputs' name='publisher' id='publisher'>
			</div>
			<!-- errors will go here -->
		</div>

    <!-- pages -->
		<div id='pages-group' class='form-group'>
			<label class='control-label col-sm-2' for='pages'>Seiten</label>
			<div class='col-sm-5'>
				<div class='input-group'>
					<input type='text' class='form-control book_inputs' name='pages' id='pages'>
					<span class='input-group-addon'>&nbsp;S.</span>
				</div>
			</div>
			<div class='col-sm-1'>
				<button onClick='addText(\"pages\",\"&ndash;\");' type='button'>&ndash;</button>
			</div>
			<!-- errors will go here -->
		</div>

    <!-- media -->
		<div id='series-group' class='form-group'>
			<label class='control-label col-sm-2' for='media'>Weitere Medien</label>
			<div class='col-sm-9'>
				<input type='text' class='form-control book_inputs' name='media' id='media'>
			</div>
			<!-- errors will go here -->
		</div>

    <!-- series -->
		<div id='series-group' class='form-group'>
			<label class='control-label col-sm-2' for='series'>Reihe</label>
			<div class='col-sm-9'>
				<input type='text' class='form-control book_inputs' name='series' id='series'>
			</div>
			<div class='col-sm-1'>
				<button onclick='addText(\"series\",\"&ndash;\");' type='button'>&ndash;</button>
			</div>
			<!-- errors will go here -->
		</div>

		<!-- nr_in_series -->
		<div id='nr_in_series-group' class='form-group'>
			<label class='control-label col-sm-2' for='nr_in_series'>Nummer in Reihe</label>
			<div class='col-sm-4'>
				<input type='text' class='form-control book_inputs' name='nr_in_series' id='nr_in_series'>
			</div>
			<div class='col-sm-6'></div>
			<!-- errors will go here -->
		</div>

		<!-- price -->
		<div id='price-group' class='form-group'>
			<label class='control-label col-sm-2' for='price'>Preis</label>
			<div class='col-sm-2'>
				<input type='text' class='form-control book_inputs' name='price' id='price'>
			</div>
      <div class='col-sm-1'>
				<button onclick='addText(\"price\",\"&#8211;\");' type='button'>&#8211;</button>
			</div>
			<label class='control-label col-sm-2' for='currency'>W&auml;hrung</label>
			<div class='col-sm-2'>
				<input type='text' class='form-control book_inputs' name='currency' id='currency'>
			</div>
      <div class='col-sm-3'>
        <button onClick='replaceText(\"currency\",\"&#128;\");' type='button'>&#128;</button>
        <button onClick='replaceText(\"currency\",\"&#36;\");' type='button'>&#36;</button>
        <button onClick='replaceText(\"currency\",\"&#163;\");' type='button'>&#163;</button>
        <button onClick='replaceText(\"currency\",\"&#165;\");' type='button'>&#165;</button>
			</div>
			<!-- errors will go here -->
		</div>

		<!-- note -->
		<div id='note-group' class='form-group'>
			<label class='control-label col-sm-2' for='note'>Bemerkungen</label>
			<div class='col-sm-10'>
				<textarea class='form-control book_inputs' rows='4' name='note' id='note'></textarea>
			</div>
			<!-- errors will go here -->
		</div>

		<!-- isbn -->
		<div id='isbn-group' class='form-group'>
			<label class='control-label col-sm-2' for='isbn'>ISBN</label>
			<div class='col-sm-4'>
				<input type='text' class='form-control book_inputs' name='isbn' id='isbn'>
			</div>
			<div class='col-sm-6'></div>
			<!-- errors will go here -->
		</div>
			</form>
			</div>
			<div class='tab-pane' id='show_book_data'>
				  <pre>
				  <div id='show_book_data1'></div>
				  </pre>
				  <div class='well'>
				  <samp><div id='show_book_data2'></div></samp>
				  </div>
				  <div class='well'>
				  <div id='show_book_data3'></div>
				  </div>
				  </div>
				</div>
				<div class='modal-footer'>
					<button type='button' id='deletebook' name='deletebook' class='btn btn-danger pull-left'><span class='glyphicon glyphicon-trash'></span>L&ouml;schen</button>
					<button type='button' id='findreviewermodal' name='findreviewermodal' class='btn btn-primary'><span id='findreviewermodal_label'></span><span class='fa fa-arrow-right'></span></button>
					<button type='button' id='updatebook' name='updatebook' class='btn btn-success'>Speichern<span class='fa fa-arrow-right'></span></button>
				</div>
			</div>
		</div>
	</div>";

}elseif($_POST["action"] == "booksearch"){
	$out["body"] = "<div class='container-fluid'> <div class='row'> <div class='col-sm-9'>
		<table id='book_table' class='sortable table table-striped table-hover'>
			<thead><tr class='info'>
				<th>Titel:</th>
				<th>Autor:</th>
				<th>Bemerkungen:</th>
				<th>Status:&nbsp;&nbsp;&nbsp;</th>
			</tr></thead><tbody>";
	$title = pg_escape_string($_POST["title"]);
	$author = pg_escape_string($_POST["author"]);
	$unassigned_books = pg_query("select books.id, title, author, note from books LEFT OUTER JOIN reviews on books.id = reviews.bookid where reviews.id is null and books.title ilike '%" . $title . "%' and books.author ilike '%" . $author . "%'");
	$halfassigned_books = pg_query("select books.id, title, author, note from books inner JOIN reviews on books.id = reviews.bookid where reviews.revid is null and books.title ilike '%" . $title . "%' and books.author ilike '%" . $author . "%'");
	$assigned_books = pg_query("select books.id, title, author, note from books inner join reviews on books.id = reviews.bookid where reviews.revid is not null and books.title ilike '%" . $title . "%' and books.author ilike '%" . $author . "%'");

	while ($b=pg_fetch_row($unassigned_books)) {
    $b_title = sonderzeichen($b[1]);
    $b_author = sonderzeichen($b[2]);
    $b_notes = sonderzeichen($b[3]);
		$out["body"] .= "<tr>
				<td class='book' data-toggle='modal' data-target='#bookmodal' data-bookid='" . $b[0] . "' data-type='0' data-value='" . $b_title . "'> " . $b[1] . " </td>
				<td data-value='" . $b_author . "'> " . $b[2] . " </td>
				<td data-value='" . $b_notes . "'> " . $b[3] . " </td>
				<td data-value='0' class='danger'></td>
			</tr>";
	}
	while ($b=pg_fetch_row($halfassigned_books)) {
    $b_title = sonderzeichen($b[1]);
    $b_author = sonderzeichen($b[2]);
    $b_notes = sonderzeichen($b[3]);
		$out["body"] .= "<tr>
				<td class='book' data-toggle='modal' data-target='#bookmodal' data-bookid='" . $b[0] . "' data-type='1' data-value='" . $b_title . "'> " . $b[1] . " </td>
				<td data-value='" . $b_author . "'> " . $b[2] . " </td>
				<td data-value='" . $b_notes . "'> " . $b[3] . " </td>
				<td data-value='1' class='warning'></td>
			</tr>";
	}
	while ($b=pg_fetch_row($assigned_books)) {
    $b_title = sonderzeichen($b[1]);
    $b_author = sonderzeichen($b[2]);
    $b_notes = sonderzeichen($b[3]);
		$out["body"] .= "<tr>
				<td class='book' data-toggle='modal' data-target='#bookmodal' data-bookid='" . $b[0] . "' data-type='2' data-value='" . $b_title . "'> " . $b[1] . " </td>
				<td data-value='" . $b_author . "'> " . $b[2] . " </td>
				<td data-value='" . $b_notes . "'> " . $b[3] . " </td>
				<td data-value='2' class='success'></td>
			</tr>";
	}
	$out["body"] .= "</tbody></table> </div>
		<div class='col-sm-3'>
			  <button type='button' id='unassigned_books' name='unassigned_books' onclick='toggleBookRow(0)' class='btn unassigned_books'>Noch nicht vergebene B&uuml;cher</button>
				<button type='button' id='halfassigned_books' name='halfassigned_books' onclick='toggleBookRow(1)' class='btn halfassigned_books'>Nicht mehr vergebene B&uuml;cher</button>
				<button type='button' id='assigned_books' name='assigned_books' onclick='toggleBookRow(2)' class='btn assigned_books'>Vergebene B&uuml;cher</button>
		</div></div></div>";

}elseif($_POST["action"] == "bookdata"){
	$result = pg_query("select * from books where id = " . $_POST["bookid"]);
	while ($b=pg_fetch_row($result)){
		$out["id"] = $b[0];
		$out["title"] = $b[1];
		$out["author"] = $b[2];
		$out["publisher"] = $b[3];
		$out["pub_country"] = $b[4];
		$out["price"] = $b[5];
		$out["note"] = $b[6];
		$out["pages"] = $b[7];
		$out["series"] = $b[8];
		$out["nr_in_series"] = $b[9];
		$out["pub_date"] = $b[10];
		$out["isbn"] = $b[11];
		$out["currency"] = $b[12];
		$out["media"] = $b[13];
	}

}elseif($_POST["action"] == "bookupdate"){
	$title = pg_escape_string($_POST["title"]);
	$author = pg_escape_string($_POST["author"]);
	$publisher = pg_escape_string($_POST["publisher"]);
	$pub_country = pg_escape_string($_POST["pub_country"]);
	$price = pg_escape_string($_POST["price"]);
	$note = pg_escape_string($_POST["note"]);
	$pages = pg_escape_string($_POST["pages"]);
	$series = pg_escape_string($_POST["series"]);
	$nr_in_series = pg_escape_string($_POST["nr_in_series"]);
	$pub_date = pg_escape_string($_POST["pub_date"]);
	$isbn = pg_escape_string($_POST["isbn"]);
	$currency = pg_escape_string($_POST["currency"]);
	$media = pg_escape_string($_POST["media"]);
	$bookid = $_POST["bookid"];

	if ($bookid==0) {
		$result = pg_query("insert into books (title, author, publisher, pub_country, price, note,
			pages, series, nr_in_series, pub_date, isbn, currency, media, acct) VALUES ('" .  $title . "', '" . $author . "', '" .
			$publisher . "', '" . $pub_country . "', '" .
			$price . "', '" . $note . "', '" . $pages . "', '" .
			$series . "', '" . $nr_in_series . "', '" . $pub_date . "', '" .
			$isbn . "', '" . $currency . "', '" . $media . "', '" . $_SESSION["user"] . "') returning id");
		$last_bookid = pg_fetch_all($result)[0]["id"];
		$out["bookid"] = $last_bookid;
	}else {
		$result = pg_query("update books set (title, author, publisher, pub_country, price, note,
			pages, series, nr_in_series, pub_date, isbn, currency, media, acct) = ('" .  $title . "', '" . $author . "', '" .
			$publisher . "', '" . $pub_country . "', '" .
			$price . "', '" . $note . "', '" . $pages . "', '" .
			$series . "', '" . $nr_in_series . "', '" . $pub_date . "', '" .
			$isbn . "', '" . $currency . "', '" . $media . "', '" . $_SESSION["user"] . "') where id = " . $bookid);
	}

	$out["response"] = pg_last_error();

}elseif($_POST["action"] == "bookdelete"){
	$result = pg_query("delete from books where id = " . $_POST["bookid"]);
	$out["response"] = pg_last_error();

}elseif($_POST["action"] == "reviewers"){
	$out["header"] = "
		<form class='form-horizontal' role='form'>
		<!-- namesearch -->
					<div id='namesearch-group' class='form-group'>
							<label class='control-label col-sm-3' for='namesearch'>Name</label>
							<div class='col-sm-5'>
									<input type='text' class='form-control' name='namesearch' id='namesearch'>
							</div>
							<div class='col-sm-4'></div>
							<!-- errors will go here -->
					</div>

		<!-- addresssearch -->
		<div id='addresssearch-group' class='form-group'>
			<label class='control-label col-sm-3' for='addresssearch'>Adresse</label>
			<div class='col-sm-5'>
				<input type='text' class='form-control' name='addresssearch' id='addresssearch'>
			</div>
			<div class='col-sm-4'></div>
			<!-- errors will go here -->
		</div>

		<!-- fieldssearch -->
		<div id='fieldssearch-group' class='form-group'>
				<label class='control-label col-sm-3 col-sm-offset-1' for='fieldssearch'>Forschungsgebiete</label>
				<div class='col-sm-4'>
						<select class='form-control' name='fieldssearch' id='fieldssearch'>
		<option></option>
								<option value='1' >Althochdeutsch</option>
								<option value='2' >Anglistik</option>
								<option value='3' >Computerlinguistik</option>
								<option value='4' >Deutsch als Fremdsprache</option>
								<option value='5' >Dialektologie</option>
								<option value='6' >Dialektometrie</option>
								<option value='7' >Friesisch</option>
								<option value='8' >Fr&uuml;hneuhochdeutsch</option>
								<option value='9' >Indogermanisch</option>
								<option value='10' >Jiddistik</option>
								<option value='11' >Klinische Linguistik</option>
								<option value='12' >Lexikographie</option>
								<option value='13' >Lexikologie</option>
								<option value='14' >Luxemburgistik</option>
								<option value='15' >Mittelhochdeutsch</option>
								<option value='16' >Morphologie</option>
								<option value='17' >Niederdeutsch</option>
								<option value='18' >Niederlandistik</option>
								<option value='19' >Oberdeutsch</option>
								<option value='20' >Onomastik</option>
								<option value='21' >Phonetik</option>
								<option value='22' >Phonologie</option>
								<option value='23' >Pragmatik</option>
								<option value='24' >Prosodie</option>
								<option value='25' >Romanistik</option>
								<option value='26' >Semantik</option>
								<option value='27' >Skandinavistik</option>
								<option value='28' >Slavistik</option>
								<option value='29' >Soziolinguistik</option>
								<option value='30' >Sprachdidaktik</option>
								<option value='31' >Sprache und Politik</option>
								<option value='32' >Spracherwerb</option>
								<option value='33' >Sprachgeschichte</option>
								<option value='34' >Sprachinsel</option>
								<option value='35' >Sprachkontakt</option>
								<option value='36' >Sprachphilosophie</option>
								<option value='37' >Sprachtheorie</option>
								<option value='38' >Sprachtypologie</option>
								<option value='39' >Syntax</option>
								<option value='40' >Textlinguistik</option>
								<option value='41' >Variationslinguistik</option>
								<option value='42' >Zimbrisch</option>
						</select>
				</div>
			 <button type='button' id='newreviewer_button' name='newreviewer_button' class='btn btn-success' data-toggle='modal' data-target='#rermodal' data-reviewerid='0'>Neuer Rezensent<span class='fa fa-arrow-right'></span></button>

				<!-- errors will go here -->
			</div>
		</form>
		<div class='modal fade' id='rermodal' tabindex='-1' role='dialog' aria-labelledby='rermodallabel' aria-hidden='true'>
			<div class='modal-dialog modal-lg'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
						<h4 class='modal-title' id='rermodallabel'>Daten &auml;ndern</h4>
					</div>
					<div class='modal-body'>
						<form class='form-horizontal' role='form'>

			<!-- title -->
				<div id='title-group' class='form-group'>
					<label class='control-label col-sm-3' for='title'>Titel</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='title' id='title'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- firstname -->
				<div id='firstname-group' class='form-group'>
					<label class='control-label col-sm-3' for='firstname'>Vorname</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='firstname' id='firstname'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- lastname -->
				<div id='lastname-group' class='form-group'>
					<label class='control-label col-sm-3' for='lastname'>Nachname</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='lastname' id='lastname'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- gender -->
				<div id='gender-group' class='form-group'>
					<label class='control-label col-sm-3' for='gender'>Geschlecht</label>
					<div class='col-sm-9'>
						<div class='btn-group' data-toggle='buttons'>
							<label for='female' class='btn btn-default'>
								<input type='radio' value='female' name='gender' id='female' autocomplete='off'>
								Weiblich
							</label>
							<label for='male' class='btn btn-default'>
								<input type='radio' value='male' name='gender' id='male' autocomplete='off'>
								M&auml;nnlich
							</label>
							<label for='other' class='btn btn-default'>
								<input type='radio' value='other' name='gender' id='other' autocomplete='off'> Anderes
							</label>
						</div>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- address -->
				<div class='form-group address-group'></div>
				<div class='form-group'>
					<button type='button' id='addaddress' name='addaddress' class='btn col-sm-3 col-sm-offset-3'>Adresse hinzuf&uuml;gen</button>
				</div>

				<!-- email -->
				<div class='form-group email-group'></div>
				<div class='form-group'>
					<button type='button' id='addemail' name='addemail' class='btn col-sm-3 col-sm-offset-3'>E-Mail-Adresse hinzuf&uuml;gen</button>
				</div>

				<!-- note -->
				<div id='note-group' class='form-group'>
					<label class='control-label col-sm-3' for='note'>Bemerkungen</label>
					<div class='col-sm-9'>
						<textarea rows='4' class='form-control' name='note' id='note'></textarea>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- fields -->
				<div id='fields-group' class='form-group'>
					<label class='control-label col-sm-3' for='fields'>Forschungsgebiete</label>
					<div class='col-sm-9'>
						<select style='width: 100%;' class='form-control' name='fields' id='fields' multiple>
							<option value='1' >Althochdeutsch</option>
							<option value='2' >Anglistik</option>
							<option value='3' >Computerlinguistik</option>
							<option value='4' >Deutsch als Fremdsprache</option>
							<option value='5' >Dialektologie</option>
							<option value='6' >Dialektometrie</option>
							<option value='7' >Friesisch</option>
							<option value='8' >Fr&uuml;hneuhochdeutsch</option>
							<option value='9' >Indogermanisch</option>
							<option value='10' >Jiddistik</option>
							<option value='11' >Klinische Linguistik</option>
							<option value='12' >Lexikographie</option>
							<option value='13' >Lexikologie</option>
							<option value='14' >Luxemburgistik</option>
							<option value='15' >Mittelhochdeutsch</option>
							<option value='16' >Morphologie</option>
							<option value='17' >Niederdeutsch</option>
							<option value='18' >Niederlandistik</option>
							<option value='19' >Oberdeutsch</option>
							<option value='20' >Onomastik</option>
							<option value='21' >Phonetik</option>
							<option value='22' >Phonologie</option>
							<option value='23' >Pragmatik</option>
							<option value='24' >Prosodie</option>
							<option value='25' >Romanistik</option>
							<option value='26' >Semantik</option>
							<option value='27' >Skandinavistik</option>
							<option value='28' >Slavistik</option>
							<option value='29' >Soziolinguistik</option>
							<option value='30' >Sprachdidaktik</option>
							<option value='31' >Sprache und Politik</option>
							<option value='32' >Spracherwerb</option>
							<option value='33' >Sprachgeschichte</option>
							<option value='34' >Sprachinsel</option>
							<option value='35' >Sprachkontakt</option>
							<option value='36' >Sprachphilosophie</option>
							<option value='37' >Sprachtheorie</option>
							<option value='38' >Sprachtypologie</option>
							<option value='39' >Syntax</option>
							<option value='40' >Textlinguistik</option>
							<option value='41' >Variationslinguistik</option>
							<option value='42' >Zimbrisch</option>
						</select>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- published -->
				<div id='published-group' class='form-group'>
					<label class='control-label col-sm-3' for='published'>Ver&ouml;ffentlichungen</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='published' id='published'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- requests -->
				<div id='requests-group' class='form-group'>
					<label class='control-label col-sm-3' for='requests'>Angefragt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='requests' id='requests'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- declined -->
				<div id='declined-group' class='form-group'>
					<label class='control-label col-sm-3' for='declined'>Davon Abgelehnt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='declined' id='declined'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- warned -->
				<div id='warned-group' class='form-group'>
					<label class='control-label col-sm-3' for='warned'>Gemahnt</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='warned' id='warned'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- published_in -->
				<div id='published_in-group' class='form-group'>
					<label class='control-label col-sm-3' for='published_in'>Ver&ouml;ffentlicht in</label>
					<div class='col-sm-9'>
						<input type='text' class='form-control' name='published_in' id='published_in'>
					</div>
					<!-- errors will go here -->
				</div>

				<!-- rate -->
				<div id='rate-group' class='form-group'>
					<label class='control-label col-sm-3' for='rate_content_div'>Inhaltlich</label>
					<div id='rate_content_div' class='rateit col-sm-3 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-min='0' data-rateit-max='3' data-rateit-step='0.5'>
					</div>
					<div id='rate_form_div' class='rateit col-sm-3 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-min='0' data-rateit-max='3' data-rateit-step='0.5'>
					</div>
					<label class='control-label col-sm-1' for='rate_form_div'>Formell</label>
				</div>

								<!-- ratenotes -->
				<div id='ratenotes-group' class='form-group'>
					<label class='control-label col-sm-3' for='ratecontent_note'></label>
					<div class='col-sm-4'>
						<textarea rows='4' class='form-control' name='ratecontent_note' id='ratecontent_note'></textarea>
					</div>
					<label class='control-label col-sm-1' for='rateform_note'></label>
					<div class='col-sm-4'>
						<textarea rows='4' class='form-control' name='rateform_note' id='rateform_note'></textarea>
					</div>
				</div>

				</form>
				<div class='hidden' id='rerid'></div>
					</div>
					<div class='modal-footer'>
						<button type='button' id='deleterer' name='deleterer' class='btn btn-danger pull-left'><span class='glyphicon glyphicon-trash'></span>L&ouml;schen</button>

            <button id='new_review_popover_btn' type='button' class='btn btn-primary' data-toggle='popover' data-placement='top' data-title='Deadline?'>Diesem Rezensenten zuweisen</button>
            <div class='hide' id='new_review_popover_content'>
              <input id='new_review_deadline'><br><br><button id='assign_rer_to_book' class='btn btn-primary'>Zuweisen</button>
            </div>

						<button type='button' id='findreviewmodal' name='findreviewmodal' class='btn btn-info'>Rezensionen anzeigen</span></button>
						<button type='button' id='updaterer' name='updaterer' class='btn btn-success'>Speichern <span class='glyphicon glyphicon-floppy-disk'></span></button>
					</div>
				</div>
			</div>
		</div>";

}elseif($_POST["action"] == "rersearch"){
	$out["body"] = "
		<table id='rer_table' class='added table table-bordered table-hover sortable'>
		<thead><tr class='info'>
			<th data-defaultsort='asc' data-defaultsign='AZ'>Name:</th>
			<th data-defaultsign='_13'>Inhaltlich:</th>
			<th data-defaultsign='_13'>Formell:</th>
			<th>Bemerkungen:</th>
			<th data-defaultsign='_19'>Ver&ouml;ffentl.:</th>
			<th data-defaultsign='_19'>Angefragt:</th>
			<th data-defaultsign='_19'>Abgelehnt:</th>
			<th data-defaultsign='_19'>Gemahnt:</th>
			<th>In&nbsp;Heft:</th>
			<th>Hat&nbsp;zur&nbsp;Zeit:</th>
		</tr></thead><tbody>";
	$fields = $_POST["fields"];
	$address = pg_escape_string($_POST["address"]);
	$name = pg_escape_string($_POST["name"]);
	if (!empty($_POST["fields"])){
		$result = pg_query("select distinct on (reviewers.id) reviewers.id, reviewers.firstname, reviewers.lastname, reviewers.title, reviewers.published, reviewers.requests, reviewers.declined, reviewers.warned, reviewers.note, reviewers.published_in, reviewers.form, reviewers.content, reviewers.formnote, reviewers.contentnote from reviewers inner join rev_has_fields on rev_has_fields.revid = reviewers.id inner join address on address.revid = reviewers.id where rev_has_fields.fieldid = '" .  $fields . "' and(reviewers.firstname ilike '%" . $name . "%' or reviewers.lastname ilike '%" . $name . "%') and address.address ilike '%" . $address . "%'");
	}else{
		$result = pg_query("select distinct on (reviewers.id) reviewers.id, reviewers.firstname, reviewers.lastname, reviewers.title, reviewers.published, reviewers.requests, reviewers.declined, reviewers.warned, reviewers.note, reviewers.published_in, reviewers.form, reviewers.content, reviewers.formnote, reviewers.contentnote from reviewers inner join address on address.revid = reviewers.id where (reviewers.firstname ilike '%" . $name . "%' or reviewers.lastname ilike '%" . $name . "%') and address.address ilike '%" . $address . "%'");
	}
	while ($b=pg_fetch_row($result)) {
    $r_name = sonderzeichen($b[2]);
    $r_notes = sonderzeichen($b[8]);
		$out["body"] .= "<tr class='reviewer' data-reviewerid='" . $b[0] . "'>
			<td data-value='" . $r_name . "' data-toggle='modal' data-target='#rermodal' data-reviewerid='" . $b[0] . "'> " . $b[3] . " " . $b[1] . " " . $b[2] . " " . " </td>
			<td data-value='" . $b[11] . "'><span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[11]/10 . "></span><br>" . $b[13] . "</td>
			<td data-value='" . $b[10] . "'><span class='rateit' data-rateit-readonly='true' data-rateit-max='3' data-rateit-value=" . $b[10]/10 . "></span><br>" . $b[12] . "</td>
			<td data-value='" . $r_notes . "'>" . $b[8] . "</td>
			<td>" . $b[4] . "</td>
			<td>" . $b[5] . "</td>
			<td>" . $b[6] . "</td>
			<td>" . $b[7] . "</td>
			<td>" . $b[9] . "</td>
			<td>";
    $query = pg_query("select bookid, id from reviews where revid = " . $b[0]);
		while ($q=pg_fetch_row($query)) {
      $check_if_published = pg_query("select type from events where reviewid = " . $q[1]);
      $show = true;
      while ($c=pg_fetch_row($check_if_published)) {
        if (startsWith($c[0], "Veröffentlicht")) {
          $show = false;
        }
      }
      if ($show) {
  			$query2 = pg_query("select title from books where id = " . $q[0]);
  			$q2 = pg_fetch_row($query2);
  			$out["body"] .= "<div>" . $q2[0] . "</div><br>";
  		}
    }
		$out["body"] .= "</td></tr>";
	}
	$out["body"] .= "</tbody></table>";

}elseif($_POST["action"] == "rerdata"){
	$reviewer = pg_query("select * from reviewers where id = " . $_POST["rerid"]);
	while ($b=pg_fetch_row($reviewer)){
		$out["id"] = $b[0];
		$out["firstname"] = $b[1];
		$out["lastname"] = $b[2];
		$out["title"] = $b[3];
		$out["gender"] = $b[4];
		$out["published"] = $b[5];
		$out["requests"] = $b[6];
		$out["declined"] = $b[7];
		$out["warned"] = $b[8];
		$out["note"] = $b[9];
		$out["published_in"] = $b[10];
		$out["form"] = $b[11];
		$out["content"] = $b[12];
		$out["formnote"] = $b[13];
		$out["contentnote"] = $b[14];
	}
	$address = pg_query("select address, type, id from address where revid = " . $_POST["rerid"]);
	$addressarray = array();
	while ($a=pg_fetch_row($address)){
		$addressarray[] = $a;
	}
	$out["address"] = $addressarray;
	$email = pg_query("select address, type, id from email where revid = " . $_POST["rerid"]);
	$emailarray = array();
	while ($e=pg_fetch_row($email)){
		$emailarray[] = $e;
	}
	$out["email"] = $emailarray;
	$fields = pg_query("select * from rev_has_fields where revid = " . $_POST["rerid"]);
	$fieldarray = "";
	while ($f=pg_fetch_row($fields)){
		$fieldarray .= $f[1] . ",";
	}
	$fieldarray = rtrim($fieldarray, ",");
	//$fieldarray .= "]";
	$out["fields"] = $fieldarray;

}elseif($_POST["action"] == "rerupdate"){
	(isset($_POST["id"])?$id = $_POST["id"]:$id = false);
	$firstname = pg_escape_string($_POST["firstname"]);
	$lastname = pg_escape_string($_POST["lastname"]);
	$title = pg_escape_string($_POST["title"]);
	$gender = $_POST["gender"];
	$published = pg_escape_string($_POST["published"]);
	$requests = pg_escape_string($_POST["requests"]);
	$declined = pg_escape_string($_POST["declined"]);
	$warned = pg_escape_string($_POST["warned"]);
	$note = pg_escape_string($_POST["note"]);
	$published_in = pg_escape_string($_POST["published_in"]);
	$form = $_POST["form"];
	$content = $_POST["content"];
	$formnote = pg_escape_string($_POST["formnote"]);
	$contentnote = pg_escape_string($_POST["contentnote"]);
	$isblocked = $_POST["isblocked"];
	// if(isset($_POST["fields"])){
	// 	$fields = $_POST["fields"];
	// 	foreach ($fields as $current_field_id) {
	// 		pg_query('insert into rev_has_fields (revid, fieldid) values (' .  $id . ', ' . $current_field_id . ')');
	// 	}
	// }
	if ($id) {
		$reviewer = pg_query("UPDATE reviewers SET (firstname, lastname, title,
			gender, published, requests, declined, warned, note, published_in, form,
			content, formnote, contentnote, isblocked) = ('" . $firstname . "', '" .
			$lastname . "', '" . $title . "', '" . $gender . "', '" . $published . "',
			'" . $requests . "', '" . $declined . "', '" . $warned . "','"	. $note . "', '"
			. $published_in . "', '" . $form . "', '" . $content . "', '" . $formnote . "'
			, '" . $contentnote . "', '" . $isblocked . "') WHERE id = " . $id);
	} else {
		(!$published?$published = 0:$published);
		(!$requests?$requests = 0:$requests);
		(!$declined?$declined = 0:$declined);
		(!$warned?$warned = 0:$warned);
		$reviewer = pg_query("insert into reviewers (firstname, lastname, title,
			gender, published, requests, declined, warned, note, published_in, form,
			content, formnote, contentnote, isblocked, acct) values ('" . $firstname . "', '" .
			$lastname . "', '" . $title . "', '" . $gender . "', '" . $published . "',
			'" . $requests . "', '" . $declined . "', '" . $warned . "','"	. $note . "', '"
			. $published_in . "', '" . $form . "', '" . $content . "', '" . $formnote . "'
			, '" . $contentnote . "', '" . $isblocked . "', '" . $_SESSION["user"] . "') returning id");
			$id = pg_fetch_all($reviewer)[0]["id"];
	}
	$out["response"] = "1: " . pg_last_error();
	$a = 0;
	$address = $_POST["address"];
	while ( $a < sizeof($address) ){
		if ($address[$a+2]==0) {
			pg_query("insert into address (revid, address, type) values (" .  $id . ", '" . $address[$a] . "', '" . $address[$a+1] . "')");
		} else {
			pg_query("update address set (revid, address, type) = (" .  $id . ", '" . $address[$a] . "', '" . $address[$a+1] . "') where id = " . $address[$a+2]);
		}
		$a = $a+3;
	}
	$out["response"] .= "; 2: " . pg_last_error();
	$e = 0;
	$email = $_POST["email"];
	while ( $e < sizeof($email) ){
		if ($email[$e+2]==0) {
			pg_query("insert into email (revid, address, type) values (" .  $id . ", '" . $email[$e] . "', '" . $email[$e+1] . "')");
		} else {
			pg_query("update email set (revid, address, type) = (" .  $id . ", '" . $email[$e] . "', '" . $email[$e+1] . "') where id = " . $email[$e+2]);
		}
		$e = $e+3;
	}
	$out["response"] .= "; 3: " . pg_last_error();

}elseif($_POST["action"] == "newreview") {
	$exists = pg_query("select id from reviews where bookid = " . $_POST["bookid"]);
	$out["html"] = "foo: ";
	$e = pg_fetch_row($exists);
	if ($e[0]) {
		$changeres = pg_query("update reviews set revid = " . $_POST["rerid"] . ", state = 1, deadline = '" . $_POST["deadline"] . "' where id = " . $e[0]);
		$new_rer_name1 = pg_query("select title, firstname, lastname from reviewers where id =" . $_POST["rerid"]);
		$new_rer_name2 = pg_fetch_row($new_rer_name1);
		$changeres_event = pg_query("INSERT INTO events(reviewid, type, timestamp, date, revid, payload, acct) VALUES (" . $e[0] . ", 'Neu vergeben an " . ($new_rer_name2[0]?$new_rer_name2[0] . " ":"") . ($new_rer_name2[1]?$new_rer_name2[1] . " ":"") . ($new_rer_name2[2]?$new_rer_name2[2] . " ":"") ." bis zum " . $_POST["deadline"] . "' , 'NOW', '" . $_POST["deadline"] . "', " . $_POST["rerid"] . ", null, '" . $_SESSION["user"] . "')");
		$update_rer = pg_query("update reviewers set requests = requests + 1 where id = " . $_POST["rerid"]);
	} else {
    $new_rer_name1 = pg_query("select title, firstname, lastname from reviewers where id =" . $_POST["rerid"]);
    $new_rer_name2 = pg_fetch_row($new_rer_name1);
    $zdl_stage = '{"Rezensionseingang":"false","Eingangsdatum":"","Dateispeicherort":"","Formatiert":"false","Lektoriert":"false","zum_Setzen":"false","vom_Setzen":"false","Seitenzahl":"","Imprimatur":"false","Umbruch":"false","Kommentar":""}';
    $newres = pg_query("INSERT INTO reviews(bookid, revid, acct, deadline, state, zdl_stage) VALUES ('" . $_POST["bookid"] . "', '" . $_POST["rerid"] . "', '" . $_SESSION["user"] . "', '" . $_POST["deadline"] . "', 1, '" . $zdl_stage . "')  returning id");
		$last_resid = pg_fetch_all($newres)[0]["id"];
		$newres_event = pg_query("INSERT INTO events(reviewid, type, timestamp, date, revid, payload, acct) VALUES (" . $last_resid . ", 'Neue Rezension, vergeben an " . ($new_rer_name2[0]?$new_rer_name2[0] . " ":"") . ($new_rer_name2[1]?$new_rer_name2[1] . " ":"") . ($new_rer_name2[2]?$new_rer_name2[2] . " ":"") ."', 'NOW', null, " . $_POST["rerid"] . ", null, '" . $_SESSION["user"] . "')");
		$update_rer = pg_query("update reviewers set requests = requests + 1 where id = " . $_POST["rerid"]);
	}

	$out["response"] = pg_last_error();

}elseif($_POST["action"] == "reviews"){
	// $out            = array();      // array to pass back data

	$out["html2"] = "<div class='modal fade' id='bookmodal' tabindex='-1' role='dialog' aria-labelledby='book_modal_label' aria-hidden='true'>
  <div class='modal-dialog'>
    <div class='modal-content'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        <ul class='nav nav-tabs' id='tabContent'>
          <li class='active'><a href='#modify_book_data' data-toggle='tab'><h4 class='modal-title' id='book_modal_label'>Daten &auml;ndern</h4></a></li>
          <li><a href='#show_book_data' data-toggle='tab'><h4 class='modal-title' id='show_book_data_tab'>Daten anzeigen</h4></a></li>
        </ul>
    </div>
    <div class='modal-body tab-content'>
      <div class='tab-pane active' id='modify_book_data'>
      <form class='form-horizontal' role='form'>

  <!-- author -->
  <div id='author-group' class='form-group'>
  <label class='control-label col-sm-2' for='author'>Autor</label>
  <div class='col-sm-10'>
    <input type='text' class='form-control book_inputs' name='author' id='author'>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- pub_date -->
  <div id='pub_date-group' class='form-group'>
  <label class='control-label col-sm-2' for='pub_date'>Publikations-jahr</label>
  <div class='col-sm-3'>
    <input type='text' class='form-control book_inputs' name='pub_date' id='pub_date'>
  </div>
  <div class='col-sm-5 col-sm-offset-2'>
  <button id='nbsp_btn' type='button'>ges. Leer.</button>
  <button id='quotes_btn' type='button'>&#132; &#147;</button>
  <button id='kursiv_btn' type='button'><span class='glyphicon glyphicon-italic'></span></button>
  <button id='kapit_btn' type='button'><span class='glyphicon glyphicon-font'></span><span class='glyphicon glyphicon-font small_glyph'></span></button></div>
  <!-- errors will go here -->
  </div>

  <!-- title -->
  <div id='title-group' class='form-group'>
  <label class='control-label col-sm-2' for='title'>Titel</label>
  <div class='col-sm-7'>
    <input type='text' class='form-control book_inputs' name='title' id='title'>
  </div>
  <div class='col-sm-3'>
    <button onclick='addText(\"title\",\"&ndash;\");' type='button'>&ndash;</button>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- pub_country -->
  <div id='pub_country-group' class='form-group'>
  <label class='control-label col-sm-2' for='pub_country'>Verlagsort</label>
  <div class='col-sm-10'>
    <input type='text' class='form-control book_inputs' name='pub_country' id='pub_country'>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- publisher -->
  <div id='publisher-group' class='form-group'>
  <label class='control-label col-sm-2' for='publisher'>Verlag</label>
  <div class='col-sm-10'>
    <input type='text' class='form-control book_inputs' name='publisher' id='publisher'>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- pages -->
  <div id='pages-group' class='form-group'>
  <label class='control-label col-sm-2' for='pages'>Seiten</label>
  <div class='col-sm-5'>
    <div class='input-group'>
      <input type='text' class='form-control book_inputs' name='pages' id='pages'>
      <span class='input-group-addon'>&nbsp;S.</span>
    </div>
  </div>
  <div class='col-sm-1'>
    <button onClick='addText(\"pages\",\"&ndash;\");' type='button'>&ndash;</button>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- media -->
  <div id='series-group' class='form-group'>
  <label class='control-label col-sm-2' for='media'>Weitere Medien</label>
  <div class='col-sm-9'>
    <input type='text' class='form-control book_inputs' name='media' id='media'>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- series -->
  <div id='series-group' class='form-group'>
  <label class='control-label col-sm-2' for='series'>Reihe</label>
  <div class='col-sm-9'>
    <input type='text' class='form-control book_inputs' name='series' id='series'>
  </div>
  <div class='col-sm-1'>
    <button onclick='addText(\"series\",\"&ndash;\");' type='button'>&ndash;</button>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- nr_in_series -->
  <div id='nr_in_series-group' class='form-group'>
  <label class='control-label col-sm-2' for='nr_in_series'>Nummer in Reihe</label>
  <div class='col-sm-4'>
    <input type='text' class='form-control book_inputs' name='nr_in_series' id='nr_in_series'>
  </div>
  <div class='col-sm-6'></div>
  <!-- errors will go here -->
  </div>

  <!-- price -->
  <div id='price-group' class='form-group'>
  <label class='control-label col-sm-2' for='price'>Preis</label>
  <div class='col-sm-2'>
    <input type='text' class='form-control book_inputs' name='price' id='price'>
  </div>
  <div class='col-sm-1'>
    <button onclick='addText(\"price\",\"&#8211;\");' type='button'>&#8211;</button>
  </div>
  <label class='control-label col-sm-2' for='currency'>W&auml;hrung</label>
  <div class='col-sm-2'>
    <input type='text' class='form-control book_inputs' name='currency' id='currency'>
  </div>
  <div class='col-sm-3'>
    <button onClick='replaceText(\"currency\",\"&#128;\");' type='button'>&#128;</button>
    <button onClick='replaceText(\"currency\",\"&#36;\");' type='button'>&#36;</button>
    <button onClick='replaceText(\"currency\",\"&#163;\");' type='button'>&#163;</button>
    <button onClick='replaceText(\"currency\",\"&#165;\");' type='button'>&#165;</button>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- note -->
  <div id='note-group' class='form-group'>
  <label class='control-label col-sm-2' for='note'>Bemerkungen</label>
  <div class='col-sm-10'>
    <textarea class='form-control book_inputs' rows='4' name='note' id='note'></textarea>
  </div>
  <!-- errors will go here -->
  </div>

  <!-- isbn -->
  <div id='isbn-group' class='form-group'>
  <label class='control-label col-sm-2' for='isbn'>ISBN</label>
  <div class='col-sm-4'>
    <input type='text' class='form-control book_inputs' name='isbn' id='isbn'>
  </div>
  <div class='col-sm-6'></div>
  <!-- errors will go here -->
  </div>
  </form>
  </div>
  <div class='tab-pane' id='show_book_data'>
      <pre>
      <div id='show_book_data1'></div>
      </pre>
      <div class='well'>
      <samp><div id='show_book_data2'></div></samp>
      </div>
      <div class='well'>
      <div id='show_book_data3'></div>
      </div>
      </div>
    </div>
    <div class='modal-footer'>
      <button type='button' id='updatebook' name='updatebook' class='btn btn-success'>Speichern<span class='fa fa-arrow-right'></span></button>
    </div>
  </div>
  </div>
  </div>

  <div class='modal fade' id='rermodal' tabindex='-1' role='dialog' aria-labelledby='rermodallabel' aria-hidden='true'>
    <div class='modal-dialog modal-lg'>
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
          <h4 class='modal-title' id='rermodallabel'>Daten &auml;ndern</h4>
        </div>
        <div class='modal-body'>
          <form class='form-horizontal' role='form'>

    <!-- title -->
      <div id='title-group' class='form-group'>
        <label class='control-label col-sm-3' for='title'>Titel</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='title' id='title'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- firstname -->
      <div id='firstname-group' class='form-group'>
        <label class='control-label col-sm-3' for='firstname'>Vorname</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='firstname' id='firstname'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- lastname -->
      <div id='lastname-group' class='form-group'>
        <label class='control-label col-sm-3' for='lastname'>Nachname</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='lastname' id='lastname'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- gender -->
      <div id='gender-group' class='form-group'>
        <label class='control-label col-sm-3' for='gender'>Geschlecht</label>
        <div class='col-sm-9'>
          <div class='btn-group' data-toggle='buttons'>
            <label for='female' class='btn btn-default'>
              <input type='radio' value='female' name='gender' id='female' autocomplete='off'>
              Weiblich
            </label>
            <label for='male' class='btn btn-default'>
              <input type='radio' value='male' name='gender' id='male' autocomplete='off'>
              M&auml;nnlich
            </label>
            <label for='other' class='btn btn-default'>
              <input type='radio' value='other' name='gender' id='other' autocomplete='off'> Anderes
            </label>
          </div>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- address -->
      <div class='form-group address-group'></div>
      <div class='form-group'>
        <button type='button' id='addaddress' name='addaddress' class='btn col-sm-3 col-sm-offset-3'>Adresse hinzuf&uuml;gen</button>
      </div>

      <!-- email -->
      <div class='form-group email-group'></div>
      <div class='form-group'>
        <button type='button' id='addemail' name='addemail' class='btn col-sm-3 col-sm-offset-3'>E-Mail-Adresse hinzuf&uuml;gen</button>
      </div>

      <!-- note -->
      <div id='note-group' class='form-group'>
        <label class='control-label col-sm-3' for='note'>Bemerkungen</label>
        <div class='col-sm-9'>
          <textarea rows='4' class='form-control' name='note' id='note'></textarea>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- fields -->
      <div id='fields-group' class='form-group'>
        <label class='control-label col-sm-3' for='fields'>Forschungsgebiete</label>
        <div class='col-sm-9'>
          <select style='width: 100%;' class='form-control' name='fields' id='fields' multiple>
            <option value='1' >Althochdeutsch</option>
            <option value='2' >Anglistik</option>
            <option value='3' >Computerlinguistik</option>
            <option value='4' >Deutsch als Fremdsprache</option>
            <option value='5' >Dialektologie</option>
            <option value='6' >Dialektometrie</option>
            <option value='7' >Friesisch</option>
            <option value='8' >Fr&uuml;hneuhochdeutsch</option>
            <option value='9' >Indogermanisch</option>
            <option value='10' >Jiddistik</option>
            <option value='11' >Klinische Linguistik</option>
            <option value='12' >Lexikographie</option>
            <option value='13' >Lexikologie</option>
            <option value='14' >Luxemburgistik</option>
            <option value='15' >Mittelhochdeutsch</option>
            <option value='16' >Morphologie</option>
            <option value='17' >Niederdeutsch</option>
            <option value='18' >Niederlandistik</option>
            <option value='19' >Oberdeutsch</option>
            <option value='20' >Onomastik</option>
            <option value='21' >Phonetik</option>
            <option value='22' >Phonologie</option>
            <option value='23' >Pragmatik</option>
            <option value='24' >Prosodie</option>
            <option value='25' >Romanistik</option>
            <option value='26' >Semantik</option>
            <option value='27' >Skandinavistik</option>
            <option value='28' >Slavistik</option>
            <option value='29' >Soziolinguistik</option>
            <option value='30' >Sprachdidaktik</option>
            <option value='31' >Sprache und Politik</option>
            <option value='32' >Spracherwerb</option>
            <option value='33' >Sprachgeschichte</option>
            <option value='34' >Sprachinsel</option>
            <option value='35' >Sprachkontakt</option>
            <option value='36' >Sprachphilosophie</option>
            <option value='37' >Sprachtheorie</option>
            <option value='38' >Sprachtypologie</option>
            <option value='39' >Syntax</option>
            <option value='40' >Textlinguistik</option>
            <option value='41' >Variationslinguistik</option>
            <option value='42' >Zimbrisch</option>
          </select>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- published -->
      <div id='published-group' class='form-group'>
        <label class='control-label col-sm-3' for='published'>Ver&ouml;ffentlichungen</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='published' id='published'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- requests -->
      <div id='requests-group' class='form-group'>
        <label class='control-label col-sm-3' for='requests'>Angefragt</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='requests' id='requests'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- declined -->
      <div id='declined-group' class='form-group'>
        <label class='control-label col-sm-3' for='declined'>Davon Abgelehnt</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='declined' id='declined'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- warned -->
      <div id='warned-group' class='form-group'>
        <label class='control-label col-sm-3' for='warned'>Gemahnt</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='warned' id='warned'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- published_in -->
      <div id='published_in-group' class='form-group'>
        <label class='control-label col-sm-3' for='published_in'>Ver&ouml;ffentlicht in</label>
        <div class='col-sm-9'>
          <input type='text' class='form-control' name='published_in' id='published_in'>
        </div>
        <!-- errors will go here -->
      </div>

      <!-- rate -->
      <div id='rate-group' class='form-group'>
        <label class='control-label col-sm-3' for='rate_content_div'>Inhaltlich</label>
        <div id='rate_content_div' class='rateit col-sm-3 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-min='0' data-rateit-max='3' data-rateit-step='0.5'>
        </div>
        <div id='rate_form_div' class='rateit col-sm-3 bigstars' data-rateit-starwidth='32' data-rateit-starheight='32' data-rateit-min='0' data-rateit-max='3' data-rateit-step='0.5'>
        </div>
        <label class='control-label col-sm-1' for='rate_form_div'>Formell</label>
      </div>

              <!-- ratenotes -->
      <div id='ratenotes-group' class='form-group'>
        <label class='control-label col-sm-3' for='ratecontent_note'></label>
        <div class='col-sm-4'>
          <textarea rows='4' class='form-control' name='ratecontent_note' id='ratecontent_note'></textarea>
        </div>
        <label class='control-label col-sm-1' for='rateform_note'></label>
        <div class='col-sm-4'>
          <textarea rows='4' class='form-control' name='rateform_note' id='rateform_note'></textarea>
        </div>
      </div>

      </form>
      <div class='hidden' id='rerid'></div>
        </div>
        <div class='modal-footer'>
          <button type='button' id='findreviewmodal' name='findreviewmodal' class='btn btn-info'>Rezensionen anzeigen</span></button>
          <button type='button' id='updaterer' name='updaterer' class='btn btn-success'>Speichern <span class='glyphicon glyphicon-floppy-disk'></span></button>
        </div>
      </div>
      </div>
      </div>

  <div class='modal fade' id='reviewmodal' tabindex='-1' role='dialog' aria-labelledby='rev_request_nav' aria-hidden='true'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header'>
				<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				<ul class='nav nav-tabs' id='tabContent'>
<li class='active'><a href='#rev_decline' data-toggle='tab'><h4 class='modal-title' id='rev_decline_nav'>Absage</h4></a></li>
<li><a href='#rev_deadline' data-toggle='tab'><h4 class='modal-title' id='rev_deadline_nav'>Deadline</h4></a></li>
<li><a href='#rev_warning' data-toggle='tab'><h4 class='modal-title' id='rev_warning_nav'>Mahnung</h4></a></li>
<li><a href='#rev_change' data-toggle='tab'><h4 class='modal-title' id='rev_change_nav'>Wechsel</h4></a></li>
<li><a href='#rev_zdl' data-toggle='tab'><h4 class='modal-title' id='rev_zdl_nav'>ZDL</h4></a></li>
<li><a href='#rev_events' data-toggle='tab'><h4 class='modal-title' id='rev_events_nav'>Vorg&auml;nge</h4></a></li>
</ul>
			</div>
			<div class='modal-body tab-content'>
				<div class='tab-pane active' id='rev_decline'>
				<div class='container-fluid'> <div class='row'> <div class='col-sm-3'>
					<label for='rev_decline_reason'>Begr&uuml;ndung:</label><br>
					<textarea rows='4' name='rev_decline_reason' id='rev_decline_reason'></textarea>
					<button onclick='$(\"#rev_decline_reason\").val(\"\")' class='btn btn-default'>Feld leeren</button>
					</div><div class='col-sm-7'>"
					// <button onclick='$(\"#rev_decline_reason\").val(\"Zu viel anderweitige Arbeit\")' class='btn btn-default'>Zu viel anderweitige Arbeit</button>
					."<button onclick='$(\"#rev_decline_reason\").val(\"Pers&ouml;nliche Beteiligung am Buch\")' class='btn btn-default'>Pers&ouml;nliche Beteiligung am Buch</button>
					<button onclick='$(\"#rev_decline_reason\").val(\"Themengebiet passt nicht\")' class='btn btn-default'>Themengebiet passt nicht</button>
					<button onclick='$(\"#rev_decline_reason\").val(\"Buch ist zu schlecht\")' class='btn btn-default'>Buch ist zu schlecht</button>
					<button onclick='$(\"#rev_decline_reason\").val(\"Befangenheit\")' class='btn btn-default'>Befangenheit</button><br>
					<button onclick='$(\"#rev_decline_reason\").val(\"Keine Zeit\")' class='btn btn-default'>Keine Zeit</button>
					<button onclick='$(\"#rev_decline_reason\").val(\"Keine Zeit, aber im Zeitraum X wieder ansprechbar\")' class='btn btn-default'>Keine Zeit, aber im Zeitraum X wieder ansprechbar</button>
					<button onclick='$(\"#rev_decline_reason\").val(\"Hat bereits ZDL-Rezension und m&ouml;chte keine neue\")' class='btn btn-default'>Hat bereits ZDL-Rezension und m&ouml;chte keine neue</button>
					<br><br></div>
					<button type='button' id='rev_decline_btn' name='rev_decline_btn' class='btn btn-primary'>Rezensent hat abgesagt</button>
				</div></div></div>
				<div class='tab-pane' id='rev_deadline'>
					<label for='rev_deadline_date'>Datum:</label><br>
					<input id='rev_deadline_date' name='rev_deadline_date'></input><br><br>
					<label for='rev_deadline_note'>Kommentar:</label><br>
					<textarea rows='4' name='rev_deadline_note' id='rev_deadline_note'></textarea><br><br>
					<button type='button' id='rev_deadline_btn' name='rev_deadline_btn' class='btn btn-primary'>Deadline festlegen</button>
				</div>
				<div class='tab-pane' id='rev_warning'>
					<label for='rev_warning_note'>Kommentar:</label><br>
					<textarea rows='4' name='rev_warning_note' id='rev_warning_note'></textarea><br><br>
					<button type='button' id='rev_warning_btn' name='rev_warning_btn' class='btn btn-primary'>Rezensent wurde gemahnt</button>
				</div>
				<div class='tab-pane' id='rev_change'>
					<label for='rev_change_newrev'>Neuer Rezensent:</label><br>
					<input type='text' id='rev_change_newrev' name='rev_change_newrev'><br><br>
					<label for='rev_change_note'>Kommentar:</label><br>
					<textarea rows='4' name='rev_change_note' id='rev_change_note'></textarea><br><br>
					<button type='button' id='rev_change_btn' name='rev_change_btn' class='btn btn-primary'>Rezensent hat gewechselt</button><br>
				</div>
        <div class='tab-pane' id='rev_zdl'>
        <div class='container-fluid'>
          <div class='row'>
            <div class='col-sm-4'>
              <span class='cb_container'>
                <input type='checkbox' id='review_arrived' name='review_arrived'> Rezension ist angekommen
              </span><br>
            </div>
            <div class='col-sm-4'>
              Datum: <input id='review_arrived_date' name='review_arrived_date'><br>
              Dateispeicherort: <input type='text' id='review_filelocation' name='review_filelocation' size='3'>
            </div>
          </div><br>
          <div class='row'>
            <div class='col-sm-4'>
              <span class='cb_container'><input type='checkbox' id='formatiert' name='formatiert'> Formatiert </span>
            </div>
          </div><br>
          <div class='row'>
            <div class='col-sm-4'>
              <span class='cb_container'><input type='checkbox' id='lektoriert' name='lektoriert'> Lektoriert </span>
            </div>
          </div><br>
          <div class='row'>
            <div class='col-sm-4'>
              <span class='cb_container'><input type='checkbox' id='zum_setzen' name='zum_setzen'> zum Setzen </span>
            </div>
          </div><br>
          <div class='row'>
            <div class='col-sm-4'>
              <span class='cb_container'><input type='checkbox' id='vom_setzen' name='vom_setzen'> vom Setzen zur&uuml;ck, </span><br>
            </div>
            <div class='col-sm-4'>
              Anzahl Seiten: <input id='vom_setzen_seitenzahl' name='vom_setzen_seitenzahl' size='3'></ut>
            </div>
          </div><br>
          <div class='row'>
            <div class='col-sm-4'>
              <span class='cb_container'><input type='checkbox' id='imprimatur' name='imprimatur'> Imprimatur erteilt </span>
            </div>
          </div><br>
          <div class='row'>
            <div class='col-sm-4'>
              <span class='cb_container'><input type='checkbox' id='umbruch' name='umbruch'> Umbruch angekommen </span>
            </div>
          </div><br>
          <div class='row'>
          <div class='col-sm-3'>Kommentar: <textarea id='zdl_comment' name='zdl_comment'></textarea><button id='zdl_stage_btn' class='btn btn-success zdl_btn'>Speichern <span class='glyphicon glyphicon-ok-sign'></span></button><div id='zdl-toast' class='toast col-sm-2 alert alert-success' role='alert'>Erfolgreich gespeichert!</div></div>
          </div>
        </div>
        </div>
				<div class='tab-pane' id='rev_events'>
				</div>
			</div>
			<div class='modal-footer'>
			</div>
		</div>
	</div>
</div>
<div>
	 <form id='findreviewform' name='findreviewform' method='POST' class='form-horizontal' role='form'>
	<!-- top -->
				<div id='name-group' class='form-group'>
						<label class='control-label col-sm-3' for='firstname'>Vorname</label>
						<div class='col-sm-4'>
								<input type='text' class='form-control reviewsearch' name='firstname' id='firstname'>
						</div>
						<label class='control-label col-sm-2' for='heftnr'>Heftnummer</label>
						<div class='col-sm-2'>
								<input type='text' class='form-control reviewsearch' name='heftnr' id='heftnr'>
						</div>
				</div>
	<!-- middle -->
				<div id='name-group' class='form-group fixed'>
						<label class='control-label col-sm-2' for='lastname'>Nachname</label>
            <div class='col-sm-3'>
								<input type='text' class='form-control reviewsearch' name='lastname' id='lastname'>
						</div>
            <label class='control-label col-sm-2' for='them'>Bei Rez.</label>
            <div class='col-sm-1'>
              <input type='checkbox' name='them' id='them' class='res_filter' data-res_state='[1,3]' checked>
						</div>
            <label class='control-label col-sm-2' for='us'>Bei ZDL </label>
            <div class='col-sm-1' style='white-space:nowrap;font-size: 1.5em;'>
              <input type='checkbox' name='us' id='us' class='res_filter' data-res_state='[4,10]' checked>
              <span class='glyphicon glyphicon-triangle-bottom' id='zdl_filter_popover' data-toggle='popover' data-placement='bottom'></span>
						</div>
            <div class='hide' id='zdl_filter_content'><ul style='padding-left: 0px; white-space:nowrap; list-style-type: none;'>
              <li><label for='us0'>Formatiert</label>
              <input type='checkbox' name='us0' id='us0' class='res_filter2' data-res_state='5' checked>
              </li>
              <li><label for='us1'>Lektoriert</label>
                <input type='checkbox' name='us1' id='us1' class='res_filter2' data-res_state='6' checked>
              </li>
              <li><label for='us2'>beim Satz</label>
                <input type='checkbox' name='us2' id='us2' class='res_filter2' data-res_state='7' checked>
              </li>
              <li><label for='us3'>vom Satz</label>
                <input type='checkbox' name='us3' id='us3' class='res_filter2' data-res_state='8' checked>
              </li>
              <li><label for='us4'>alle außer Eingang</label>
                <input type='checkbox' name='us4' id='us4' class='res_filter2' data-res_state='[5,10]' checked>
              </li>
            </ul></div>
				</div>
	<!-- bottom -->
				<div id='book-group' class='form-group'>
						<label class='control-label col-sm-3' for='book'>Buch</label>
						<div class='col-sm-4'>
								<input type='text' class='form-control reviewsearch' name='book' id='book'>
						</div>
            <label class='control-label col-sm-2' for='done'>Veröffentlicht</label>
            <div class='col-sm-1'>
              <input type='checkbox' name='done' id='done' class='res_filter' data-res_state='11' checked>
						</div>
						<!-- errors will go here -->
				</div></form></div>";
	$out["html"] = "<div class='container-fluid'> <div class='row'> <div class='col-sm-11'>";

  $out["events_table"] = "";
  $table1 = "<table id='reviews_table1' class='table added table-bordered table-hover sortable'>
	<thead><tr class='bg-info'>
		<th>Buch:<span id='counter1' class='badge pull-right'></span></th>
		<th>Rezensent:</th>
		<th data-defaultsort='asc' >Aktuell:</th>
		<th>Auswahl <span class='res_table_counter'></span></th>
	</tr></thead><tbody>";
  $table2 = "<table id='reviews_table2' class='table added table-bordered table-hover sortable'>
	<thead><tr class='bg-danger'>
		<th>Buch:<span id='counter2' class='badge pull-right'></span></th>
		<th>Rezensent:</th>
		<th data-defaultsort='asc' >Aktuell:</th>
		<th>Auswahl <span class='res_table_counter'></span></th>
	</tr></thead><tbody>";
  $table3 = "<table id='reviews_table3' class='table added table-bordered table-hover sortable'>
	<thead><tr class='bg-warning'>
		<th>Buch:<span id='counter3' class='badge pull-right'></span></th>
		<th>Rezensent:</th>
		<th data-defaultsort='asc' >Aktuell:</th>
		<th>Auswahl <span class='res_table_counter'></span></th>
	</tr></thead><tbody>";
  $table4 = "<table id='reviews_table4' class='table added table-bordered table-hover sortable'>
	<thead><tr class='bg-success'>
		<th>Buch:<span id='counter4' class='badge pull-right'></span></th>
		<th></th>
		<th>Aktuell:</th>
		<th data-defaultsort='desc' ></th>
	</tr></thead><tbody>";
	$firstname = pg_escape_string($_POST["firstname"]);
	$lastname = pg_escape_string($_POST["lastname"]);
	$book = pg_escape_string($_POST["book"]);
	if(empty($_POST["heftnr"])){
		$heftnr = "";
  } else {
		$heftnr = "and (nev.type ilike 'Veröffentlicht in Heft " . $_POST["heftnr"] . "%')";
  }
	// if(empty($_POST["heftnr2"])){
	// 	$heftnr2 = "";
  // } else {
	// 	$heftnr2 = "inner join events on reviews.id=events.reviewid";
	// 	$heftnr = "and (events.type ilike 'Veröffentlicht in Heft " . $_POST["heftnr2"] . "%')";
  // }
	// select * from reviews left join events on reviews.id=events.reviewid where type = 'Veröffentlicht in Heft: 55' order by reviews.id
	// if(empty($_POST["name"]) && empty($_POST["book"])){
	// 	$result = pg_query("with nev as (select e1.* from events as e1 left join events as e2 on e1.reviewid=e2.reviewid and e1.timestamp < e2.timestamp where e2.timestamp is null) select reviews.id, nev.type, nev.timestamp, nev.date, nev.payload, books.title, reviewers.title, reviewers.firstname, reviewers.lastname, reviewers.id from nev inner join reviews on nev.reviewid=reviews.id inner join books on reviews.bookid=books.id left outer join reviewers on reviews.revid=reviewers.id order by nev.timestamp asc");
	// }elseif(empty($_POST["name"])){
	// 	$result = pg_query("with nev as (select e1.* from events as e1 left join events as e2 on e1.reviewid=e2.reviewid and e1.timestamp < e2.timestamp where e2.timestamp is null) select reviews.id, nev.type, nev.timestamp, nev.date, nev.payload, books.title, reviewers.title, reviewers.firstname, reviewers.lastname, reviewers.id from nev inner join reviews on nev.reviewid=reviews.id inner join books on reviews.bookid=books.id left outer join reviewers on reviews.revid=reviewers.id where books.title ilike '%" . $book . "%' order by nev.timestamp asc");
	// }else{
  // reviews.id, nev.type, nev.timestamp, nev.date, nev.payload
  // books.title, reviewers.title, reviewers.firstname, reviewers.lastname, reviewers.id
  // reviews.heft, books.author, books.pub_date, books.id, reviews.deadline
  // reviews.state
		$result = pg_query("with nev as (select e1.* from events as e1 left join events as e2 on e1.reviewid=e2.reviewid and e1.timestamp < e2.timestamp where e2.timestamp is null) select reviews.id, nev.type, nev.timestamp, nev.date, nev.payload, books.title, reviewers.title, reviewers.firstname, reviewers.lastname, reviewers.id, reviews.heft, books.author, books.pub_date, books.id, reviews.deadline, reviews.state from nev inner join reviews on nev.reviewid=reviews.id inner join books on reviews.bookid=books.id left outer join reviewers on reviews.revid=reviewers.id where (books.title ilike '%" . $book . "%' or books.author ilike '%" . $book . "%' or books.pub_date ilike '%" . $book . "%') and (reviewers.firstname ilike '%" . $firstname . "%' and reviewers.lastname ilike '%" . $lastname . "%') " . $heftnr . " order by nev.timestamp asc");
	// }
	while ($b=pg_fetch_row($result)) {
		$dt = DateTime::createFromFormat('Y-m-d H:i:s.u', $b[2]);
		$date = $dt->format('d.m.y');
    $sort_date = $dt->format('ymd');
		$deadline = "";
		$note = "";
		if (!empty($b[3])){
      $dt2 = DateTime::createFromFormat('Y-m-d', $b[3]);
      $date2 = $dt2->format('d.m.y');
			$deadline = " | Datum: " . $date2;
		}
		if (!empty($b[4])){
			$note = " | Bemerkung: " . $b[4];
		}
		$events = pg_query("select * from events where reviewid=" . $b[0] . " order by events.timestamp desc");
		$hidden = false;
		$events_table = "";
    $res_eingang = "";

		while($c=pg_fetch_row($events)){
			if ($c[1]=="hidden"){
				$hidden = true;
			}
			$event_dt = DateTime::createFromFormat('Y-m-d H:i:s.u', $c[2]);
			$event_date = $event_dt->format('d.m.y');
      $event_sort_date = $event_dt->format('ymd');
			$event_deadline = "";
			$event_note = "";
			if (!empty($c[3])){
        $event_dt2 = DateTime::createFromFormat('Y-m-d', $c[3]);
        $event_date2 = $event_dt2->format('d.m.y');
				$event_deadline = " | Datum: " . $event_date2;
			}
			if (!empty($c[5])){
				$event_note = " | Bemerkung: " . $c[5];
			}
      if (startsWith($c[1], "Rezensionseingang")) {
        $res_eingang = $event_sort_date;
      }
			$events_table .= "<tr data-toggle='modal' data-target='#reviewmodal' class='event' id='event" . $b[0] . "' data-reviewid='" . $b[0] . "' data-reviewerid='" . $b[9] . "' style='padding:1px;'>
				<td colspan='4' style='padding:1px; background-color:#dddddd;'>
					Vorgang: " . $c[1] . " | vom: " . $event_date . $event_deadline . $event_note . "
				</td>
			</tr>";

		}

    // switch (true) {
    //   case startsWith($b[1], "Absage von"):
    //     $res_color = "danger";
    //     $res_state = 0;
    //     break;
    //   case startsWith($b[1], "Neu vergeben an"):
    //   case startsWith($b[1], "Neue Rezension"):
    //   case startsWith($b[1], "Wechsel des Rezensenten"):
    //     $res_color = "";
    //     $res_state = 1;
    //     break;
    //   case startsWith($b[1], "Neue Deadline"):
    //     $res_state = 2;
    //     break;
    //   case startsWith($b[1], "Mahnung"):
    //     $res_state = 3;
    //     break;
    //   case startsWith($b[1], "Rezensionseingang"):
    //     $res_color = "warning";
    //     $res_state = 4;
    //     break;
    //   case startsWith($b[1], "Formatiert"):
    //     $res_state = 5;
    //     break;
    //   case startsWith($b[1], "Lektoriert"):
    //     $res_color = "warning";
    //     $res_state = 6;
    //     break;
    //   case startsWith($b[1], "zum Setzen"):
    //     $res_color = "warning";
    //     $res_state = 7;
    //     break;
    //   case startsWith($b[1], "zur. vom Setzen"):
    //     $res_color = "warning";
    //     $res_state = 8;
    //     break;
    //   case startsWith($b[1], "Imprimatur"):
    //     $res_state = 9;
    //     break;
    //   case startsWith($b[1], "Umbruch"):
    //     $res_color = "warning";
    //     $res_state = 10;
    //     break;
    //   case startsWith($b[1], "Veröffentlicht in Heft"):
    //     $res_color = "success";
    //     $res_state = 11;
    //     break;
    //
    //   default:
    //     $res_color = "";
    //     $res_state = 0;
    //     break;
    // }

    $res_title = sonderzeichen($b[5]);
    $res_name = sonderzeichen($b[8]);
    $sort_heftnr = explode("/", $b[10], 2);
    $book_pubdate = substr($b[12], -4);
    if ($res_eingang!="") {
      $sort_date = $res_eingang;
    }
    $res_state = $b[15];
    $res_color = "active";

    if (strtotime($b[14]) < time() && $res_state > 0 && $res_state < 4) {
      $res_color = "danger";
      $res_date = DateTime::createFromFormat('Y-m-d', $b[14]);
      $res_eingang = $res_date->format('ymd');
    }

		$table = "<tr class='" . $res_color . "' data-res_state='" .  $res_state . "' data-res_deadline='" . $b[14] . "'>
			<td data-toggle='modal' data-target='#bookmodal' data-bookid='" . $b[13] . "' data-value='" . $res_title . "' data-reviewid='" . $b[0] . "'> " . $b[5] . "<br>" . $b[11] . " | " . $book_pubdate . "</td>
			<td data-toggle='modal' data-target='#rermodal' data-reviewerid='" . $b[9] . "' ata-value='" . $res_name . "' data-reviewid='" . $b[0] . "'> " . $b[6] . " " . $b[7] . " " . $b[8] . " " . " </td>
			<td class='reviews' data-value='" . $res_eingang . "' data-reviewid='" . $b[0] . "'> Vorgang: " . $b[1] . " | vom: " . $date . $deadline . $note . "</td>
			<td class='cb_container' align='center' valign='middle' data-res_state='" . $res_state . "' data-value='" . $sort_heftnr[0] . "'><input type='checkbox' class='hidden' id='review_select_" . $b[0] . "' name='review_select_" . $b[0] . "' data-reviewid='" . $b[0] . "' data-reviewerid='" . $b[9] . "'><span class='glyphicon glyphicon-ok hidden'></span></td>
			</tr>";
			// <tr><td colspan='3' data-toggle='modal' data-target='#reviewmodal' class='hiddenRow accordion-body collapse event active' id='event" . $b[0] . "' data-reviewid='" . $b[0] . "' data-reviewerid='" . $b[9] . "'><div class='alert'><table>

      // table1: bei zdl
      // table2: über deadline
      // table3: bei Rez
      // table4: veröffentlicht
    if (strtotime($b[14]) < time() && $res_state > 0 && $res_state < 4) {
      $table2 .= $table;
    } else {
      switch ($res_state) {
        case 0:
        case 1:
        case 2:
        case 3:
          $table3 .= $table;
          break;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
          $table1 .= $table;
          break;
        case 11:
          $table4 .= $table;
          break;

        default:
          $table1 .= $table;
          break;
      }

    }

		$out["events_table"] .= $events_table;
		// $out["html"] .= "</table></div></td></tr>";
	}

  $table1 .= "</tbody></table>";
  $table2 .= "</tbody></table>";
  $table3 .= "</tbody></table>";
  $table4 .= "</tbody></table>";
	$out["html"] .= $table1 . $table2 . $table3 . $table4 . "</div> <div class='col-sm-1'>
	<button id='batch_zum_setzen' name='batch_zum_setzen' class='btn added'>Zum Setzen</button><br>
	<button id='batch_vom_setzen' name='batch_vom_setzen' class='btn added'>Vom Setzen</button><br>
	<button id='batch_heft_zuordnung' name='batch_heft_zuordnung' class='btn added'>Heft zuordnen</button>".
  // <br><br><br>
	// <button id='batch_hide' name='batch_hide' class='btn added'>Ausblenden</button><br>
	// <button id='batch_show' name='batch_show' class='btn added'>Wieder anzeigen</button><br>
	// <button id='batch_show_all' name='batch_show' class='btn added'>Alle anzeigen</button><br>
	"</div></div></div>";
	// show a message of success and provide a true success variable
	$out["success"] = true;
	$out["message"] = "Success!";

}elseif($_POST["action"] == "resupdate"){

	$out["html"] = '';
	switch ($_POST["type"]) {
		case 'declined':
      $old_rer_name1 = pg_query("select title, firstname, lastname from reviewers where id = " . $_POST["revid"]);
      $old_rer_name2 = pg_fetch_row($old_rer_name1);
			$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) VALUES ('" . $_POST["id"] . "', 'Absage von " . ($old_rer_name2[0]?$old_rer_name2[0] . " ":"") . ($old_rer_name2[1]?$old_rer_name2[1] . " ":"") . ($old_rer_name2[2]?$old_rer_name2[2] . " ":"") . "', 'NOW', NULL, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "')");
			$query2 = pg_query("update reviews set revid = null, state = 0 where id =" . $_POST["id"]);
			$query3 = pg_query("update reviewers set declined = declined + 1 where id = " . $_POST["revid"]);
			$out["html"] = pg_last_error();
			break;

		case 'new_deadline':
			$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) VALUES ('" . $_POST["id"] . "', 'Neue Deadline', 'NOW', '" . $_POST["date"] . "', '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "')");
      $query2 = pg_query("update reviews set state = 2, deadline = '" . $_POST["date"] . "' where id =" . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'warning':
			$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) VALUES ('" . $_POST["id"] . "', 'Mahnung', 'NOW', NULL, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "')");
			$query2 = pg_query("update reviewers set warned = warned + 1 where id = " . $_POST["revid"]);
      $query3 = pg_query("update reviews set state = 3 where id =" . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'change_of_rer':
			$old_rer_name1 = pg_query("select title, firstname, lastname from reviewers where id = (select revid from reviews where id = " . $_POST["id"] . ")");
			$old_rer_name2 = pg_fetch_row($old_rer_name1);
			$new_rer_name1 = pg_query("select title, firstname, lastname from reviewers where id =" . $_POST["revid"]);
			$new_rer_name2 = pg_fetch_row($new_rer_name1);
			$query3 = pg_query("update reviewers set declined = declined + 1 where id = (select revid from reviews where id = " . $_POST["id"] . ")");
			$query4 = pg_query("update reviewers set requests = requests + 1 where id = " . $_POST["revid"]);
			$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) VALUES ('" . $_POST["id"] . "', 'Wechsel des Rezensenten', 'NOW', NULL, '" . $_POST["revid"] . "', 'Wechsel von " . ($old_rer_name2[0]?$old_rer_name2[0] . " ":"") . ($old_rer_name2[1]?$old_rer_name2[1] . " ":"") . ($old_rer_name2[2]?$old_rer_name2[2] . " ":"") . "zu " . ($new_rer_name2[0]?$new_rer_name2[0] . " ":"") . ($new_rer_name2[1]?$new_rer_name2[1] . " ":"") . ($new_rer_name2[2]?$new_rer_name2[2] . " ":"") . "| " . $_POST["payload"] . "', '" . $_SESSION["user"] . "')");
			$query2 = pg_query("update reviews set revid=" . $_POST["revid"] . ", state = 1 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'zdl_stage':
			$payload = pg_escape_string(json_encode($_POST["payload"]));
      $query = pg_query("update reviews set zdl_stage = '" . $payload . "' where id = " . $_POST["id"]);
			// $query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload) VALUES ('" . $_POST["id"] . "', 'Redaktionell', 'NOW', NULL, '" . $_POST["revid"] . "', '" . $payload . "')");
			$out["html"] = "zdl_stage error: " . pg_last_error();
			$out["foo"] = $payload;
			break;

		case 'review_arrived':
      if (empty($_POST["date"])) {
        $date = "null";
      } else {
        $date = $_POST["date"];
      }
			$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) select '" . $_POST["id"] . "', 'Rezensionseingang', 'NOW', '" . $date . "', '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "' where not exists (select * from events where (reviewid =  " . $_POST["revid"] . " and type = 'Rezensionseingang'))");
      $query2 = pg_query("update reviews set state = 4 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'formatiert':
			$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) select '" . $_POST["id"] . "', 'Formatiert', 'NOW', null, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "' where not exists (select * from events where (reviewid =  " . $_POST["revid"] . " and type = 'Formatiert'))");
      $query2 = pg_query("update reviews set state = 5 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'lektoriert':
  		$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) select '" . $_POST["id"] . "', 'Lektoriert', 'NOW', null, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "' where not exists (select * from events where (reviewid =  " . $_POST["revid"] . " and type = 'Lektoriert'))");
      $query2 = pg_query("update reviews set state = 6 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'zum_setzen':
  		$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) select '" . $_POST["id"] . "', 'zum Setzen', 'NOW', null, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "' where not exists (select * from events where (reviewid =  " . $_POST["revid"] . " and type = 'zum Setzen'))");
      $query2 = pg_query("update reviews set state = 7 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'vom_setzen':
  		$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) select '" . $_POST["id"] . "', 'zur. vom Setzen', 'NOW', null, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "' where not exists (select * from events where (reviewid =  " . $_POST["revid"] . " and type = 'zur. vom Setzen'))");
      $query2 = pg_query("update reviews set state = 8 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'imprimatur':
  		$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) select '" . $_POST["id"] . "', 'Imprimatur', 'NOW', null, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "' where not exists (select * from events where (reviewid =  " . $_POST["revid"] . " and type = 'Imprimatur'))");
      $query2 = pg_query("update reviews set state = 9 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'umbruch':
  		$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) select '" . $_POST["id"] . "', 'Umbruch', 'NOW', null, '" . $_POST["revid"] . "', '" . $_POST["payload"] . "', '" . $_SESSION["user"] . "' where not exists (select * from events where (reviewid =  " . $_POST["revid"] . " and type = 'Umbruch'))");
      $query2 = pg_query("update reviews set state = 10 where id = " . $_POST["id"]);
			$out["html"] = pg_last_error();
			break;

		case 'to_set':
			$a = 0;
			$array = $_POST["payload"];
			while ($a < sizeof($array) ){
				$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) VALUES (" . $array[$a] . ", 'zum Setzen', 'NOW', NULL, '" . $array[$a+1] . "', '', '" . $_SESSION["user"] . "')");
        $query2 = pg_query("update reviews set state = 7 where id = " . $array[$a]);
				$a+=2;
			}
			break;

		case 'from_set':
			$a = 0;
			$array = $_POST["payload"];
			while ($a < sizeof($array) ){
				$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) VALUES (" . $array[$a] . ", 'zur. vom Setzen', 'NOW', NULL, '" . $array[$a+1] . "', '', '" . $_SESSION["user"] . "')");
        $query2 = pg_query("update reviews set state = 8 where id = " . $array[$a]);
				$a+=2;
			}
			break;

		case 'heft':
			$a = 0;
			$array = $_POST["payload"];
			while ($a < sizeof($array) ){
        $heftnummer = pg_escape_string($_POST["id"]);
				// Missbrauch von $_POST["id"] zur Übertragung der Heftnummer
				$query = pg_query("insert into events (reviewid, type, timestamp, date, revid, payload, acct) VALUES (" . $array[$a] . ", 'Veröffentlicht in Heft " . $heftnummer . "', 'NOW', NULL, '" . $array[$a+1] . "', '', '" . $_SESSION["user"] . "')");
        $query2 = pg_query("update reviews set state = 11, heft = '" . $heftnummer . "' where id = " . $array[$a]);
				$test_if_published = pg_query("select published_in from reviewers where id = " . $array[$a+1]);
				$t_i_p = pg_fetch_row($test_if_published);
				if ($t_i_p[0]) {
					$increase_published = pg_query("update reviewers set published = published + 1, published_in = published_in || ', " . $heftnummer . "' where id = " . $array[$a+1]);
				} else {
					$increase_published = pg_query("update reviewers set published = published + 1, published_in = published_in || '" . $heftnummer . "' where id = " . $array[$a+1]);
				}
				$a+=2;
			}
			break;

		default:
			$out["html"] = "unbekannter typ";
			break;
	}

}elseif($_POST["action"] == "resdata"){

	$resid = $_POST["resid"];
  $zdl_stage = pg_query("select zdl_stage from reviews where id=" . $resid);
  while ($zdl = pg_fetch_row($zdl_stage)) {
    $out["zdl_stage"] = $zdl[0];
  }
	$events = pg_query("select reviewid, type, timestamp, date, revid, payload, id from events where reviewid='" . $resid . "' order by events.timestamp desc");
	$out["events"] = "<table>";
  $out["ev_data"] = "{";
	while($c=pg_fetch_row($events)){
		$dt = DateTime::createFromFormat('Y-m-d H:i:s.u', $c[2]);
		$date = $dt->format('d.m.y');
		$deadline = "";
		$note = "";
		if (!empty($c[3])){
      $dt2 = DateTime::createFromFormat('Y-m-d', $c[3]);
      $date2 = $dt2->format('d.m.y');
			$deadline = " | Datum: " . $date2;
		}
		if (!empty($c[5])){
			$note = " | Bemerkung: " . $c[5];
		}
    if (strlen($out["ev_data"]) > 2) {
      $out["ev_data"] .= ",";
    }
    $out["ev_data"] .= ' "' . $c[1] . '" : "' . $c[5] . '" ';
		$out["events"] .= "<tr>
			<td>
				<div> Vorgang: " . $c[1] . " | vom: " . $date . $deadline . $note . "</div><br>
			</td>
			<td>
				&nbsp;&nbsp;<span data-event_id='" . $c[6] . "' class='glyphicon glyphicon-trash'></span>
			</td>
		</tr>";
	}
  $out["ev_data"] .= "}";
	$out["events"] .= "</table>";

}elseif($_POST["action"] == "delete_event"){
	$result = pg_query("delete from events where id = '" . $_POST["event_id"] . "'");

}elseif($_POST["action"] == "bugreports"){
	$out["html"] = "<div id='bugreports_overview' class='container-fluid'><div class='row'><div class='col-sm-8'>";
	$query = pg_query("select * from bugreports order by id");
	while($q=pg_fetch_row($query)){
		//id date content author
		$dt = DateTime::createFromFormat('Y-m-d', $q[1]);
		$date = $dt->format('d.m.Y');
		$out["html"] .= "<div class='row" . ($q[6]?" hide_me' style='display: none'":"'") . ">
      <div class='col-sm-4 " . ($q[5]?"alert alert-success":"") . "'>" . $q[3] . "<br> am " . $date . "<br>ID: " . $q[0] . "<br><br>
      <span data-id='" . $q[0] . "' class='glyphicon glyphicon-trash'></span>
      <span data-id='" . $q[0] . "' class='glyphicon glyphicon-ok'></span>
      <button type='button' data-id='" . $q[0] . "' class='hide_this btn btn-primary btn-xs'>Ausblenden</button></div>
      <div class='col-sm-8'><textarea id='" . $q[0] . "' rows='5'>" . $q[2] . "</textarea></div></div>" . ($q[5]||$q[6]?"":"<br>");
	}
	$out["html"] .= "<div class='row'>
    <div class='col-sm-4'>Name: <input id='author'></div>
    <div class='col-sm-8'><textarea id='new_bugreport' rows='5'></textarea><br><button type='button' id='save_bugreport' name='save_bugreport' class='btn btn-success'>Speichern<span class='fa fa-arrow-right'></span></button></div>
    </div></div>
    <div class='col-sm-3'><button type='button' id='show_all' name='show_all' class='btn btn-success'>Alle anzeigen</button></div></div></div>";

	if (isset($_POST["content"])) {
		$content = pg_escape_string($_POST["content"]);
		$author = pg_escape_string($_POST["author"]);
		$save = pg_query("insert into bugreports (date, content, author) values ('NOW', '" . $content . "', '" . $author . "' )");
	}

}elseif($_POST["action"] == "mark_bugreport_done"){
	$query = pg_query("update bugreports set done = true where id = " . $_POST["id"]);

}elseif($_POST["action"] == "hide_bugreport"){
	$query = pg_query("update bugreports set hidden = true where id = " . $_POST["id"]);

}elseif($_POST["action"] == "delete_bugreport"){
	$query = pg_query("delete from bugreports where id = " . $_POST["id"]);

}elseif($_POST["action"] == "admin"){
	$out["html"] = "Forschungsgebiete:
		<div class='container-fluid'><div class='col-sm-3'>";
  $query = pg_query("select id, name from fields order by name asc");
	$count = 0;
	while($q=pg_fetch_row($query)){
	 if ($count%15==0) {
		 $out["html"] .= "</div><div class='col-sm-3'>";
	 }
	 $out["html"] .= "<input class='fields' id='" . $q[0] . "' value='" . $q[1] . "'>
	 	&nbsp;&nbsp;<span data-id='" . $q[0] . "' class='glyphicon glyphicon-trash'></span><br>";
	 $count++;
	}
	$out["html"] .= "<button id='new_field' class='btn'>Neues Forschungsgebiet</button></div></div>";

}elseif($_POST["action"] == "fieldupdate"){
	$name = pg_escape_string($_POST["name"]);
	if ($_POST["id"]=="new") {
		$query = pg_query("insert into fields(name) values('" . $name . "')");
	} else {
		$query = pg_query("update fields set name = '" . $name . "' where id = " . $_POST["id"]);
	}

}elseif($_POST["action"] == "fielddelete"){
	$query = pg_query("delete from fields where id = " . $_POST["id"]);

}elseif($_POST["action"] == "foo"){
	$exists = pg_query("select id from reviews where bookid = " . $_POST["bookid"]);
	$out["html"] = "foo: ";
	$e = pg_fetch_row($exists);
	if ($e[0]) {
		$out["html"] .= "yay";
	} else {
		$out["html"] .= "nay";
	}

	// $newres = pg_query("INSERT INTO reviews(bookid, revid) VALUES ('" . $_POST["bookid"] . "', '" . $_POST["rerid"] . "')  returning id");

}else{
	$out["header"] = "nicht im backend gefunden(header)";
	$out["body"] = "nicht im backend gefunden(body)";
}}else { //session abfrage
  $out["html"] = "nicht eingeloggt";
  $out["header"] = "nicht eingeloggt";
  $out["body"] = "nicht eingeloggt";
}

	echo json_encode($out);
