﻿
create table books( 
	id serial PRIMARY KEY, 
	title text,
	author text,
	publisher text,
	pub_country text,
	price text,
	note text,
	pages text,
	series text,
	nr_in_series text,
	pub_date text,
	isbn text
);
create table reviewers(
	id serial PRIMARY KEY,
	firstname text,
	lastname text,
	title text,
	gender text,
	published text,
	requests text,
	denied text,
	warned text,
	note text,
	published_in text,
	form int,
	formnote text,
	content int,
	contentnote text,
	isblocked boolean
);
create table address(
	id serial PRIMARY KEY,
	revid int references reviewers(id),
	address text,
	type text
);
create table email(
	id serial PRIMARY KEY,
	revid int references reviewers(id),
	address text,
	type text
);
create table fields(id serial primary key, name varchar);

create table rev_has_fields(
	revid int references reviewers(id), 
	fieldid int references fields(id)
);

create table reviews(
	id serial PRIMARY KEY, 
	bookid int references books(id),
	revid int references reviewers(id)
);

CREATE TABLE events(
  reviewid integer references reviews(id),
  type text,
  "timestamp" timestamp without time zone NOT NULL,
  date date,
  revid int references reviewers(id),
  payload text
);

insert into fields (name) values
('Althochdeutsch'),
('Anglistik'),
('Computerlinguistik'),
('Deutsch als Fremdsprache'),
('Dialektologie'),
('Dialektometrie'),
('Friesisch'),
('Frühneuhochdeutsch'),
('Indogermanisch'),
('Jiddistik'),
('Klinische Linguistik'),
('Lexikographie'),
('Lexikologie'),
('Luxemburgistik'),
('Mittelhochdeutsch'),
('Morphologie'),
('Niederdeutsch'),
('Niederlandistik'),
('Oberdeutsch'),
('Onomastik'),
('Phonetik'),
('Phonologie'),
('Pragmatik'),
('Prosodie'),
('Romanistik'),
('Semantik'),
('Skandinavistik'),
('Slavistik'),
('Soziolinguistik'),
('Sprachdidaktik'),
('Sprache und Politik'),
('Spracherwerb'),
('Sprachgeschichte'),
('Sprachinsel'),
('Sprachkontakt'),
('Sprachphilosophie'),
('Sprachtheorie'),
('Sprachtypologie'),
('Syntax'),
('Textlinguistik'),
('Variationslinguistik'),
('Zimbrisch'); 
-- select * from fg, rez_has_fg WHERE fg.id = rez_has_fg.fgID AND rez_has_fg.rezID = 6;

-- insert into 