// rer = rezensenten (REvieweR) | res = rezensionen (REviewS)

function addText(a, b) {
	document.getElementById(a).value += b;
	document.getElementById(a).focus();
};
function replaceText(a, b) {
	document.getElementById(a).value = b;
	document.getElementById(a).focus();
};
// function isValidISBN(isbn) {
//   isbn = isbn.replace(/[^\dX]/gi, '');
//   if(isbn.length != 10){
//     return false;
//   }
//   var chars = isbn.split('');
//   if(chars[9].toUpperCase() == 'X'){
//     chars[9] = 10;
//   }
//   var sum = 0;
//   for (var i = 0; i < chars.length; i++) {
//     sum += ((10-i) * parseInt(chars[i]));
//   };
//   return ((sum % 11) == 0);
// }
function getBooks(title, author, cb){
	$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"booksearch", "title": title, "author": author},
			dataType    : "json"
		})
		.done(function(e) {
			var book_body = e.body.replace(/\[i\]/g,'<i>').replace(/\[\/i\]/g,'</i>').replace(/\[k\]/g,'<span style="font-variant:small-caps;">').replace(/\[\/k\]/g,'</span>');
			$("#output_body").empty().html(book_body);
			$.bootstrapSortable();
			$("#book_table").floatThead({
				scrollContainer: function($table){
					return $table.closest('.wrapper');
				}
			});
			// if (cb) cb();
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			$("#output_body").empty().html("Beim Laden der B&uuml;cher ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
		});
};

// getbooks("wuff", "miau", function(){
// 	 console.log("hello");
// })

function toggleBookRow(target) {
	$("td[data-value='" + target + "']").parent().toggle();
	// hintergrundfarbe des buttons anpassen
	if (target==0) { $("#unassigned_books").toggleClass("unassigned_books");
	} else if (target==1) { $("#halfassigned_books").toggleClass("halfassigned_books");
	} else { $("#assigned_books").toggleClass("assigned_books");}
};

function getReviewers(name, address, fields){
	$.ajax({
		type        : "POST",
		url         : "backend.php",
		data        : {"action":"rersearch", "name": name, "address": address, "fields": fields},
		dataType    : "json"
	})
	.done(function(e) {
		var reviewers_body = e.body.replace(/\[i\]/g,'<i>').replace(/\[\/i\]/g,'</i>').replace(/\[k\]/g,'<span style="font-variant:small-caps;">').replace(/\[\/k\]/g,'</span>');
		$("#output_body").empty().html(reviewers_body);
		$.bootstrapSortable();
		$(document).ready( function() {
			$(".rateit").rateit();
			$("#fields").select2();
			$("#rer_table").floatThead({
				scrollContainer: function($table){
					return $table.closest('.wrapper');
				}
			});
			var max_fields	= 5; //maximum input boxes allowed
			var nr_of_address = 1;
			var nr_of_email = 1;
			$(document).off("click", "#addaddress").on("click","#addaddress", function(e) {
				e.preventDefault();
				if(nr_of_address < max_fields){
					nr_of_address++;
					$(".address-group:last").after("\
						<div id='address-group" + nr_of_address + "' class='added form-group address-group'>\
							<label class='control-label col-sm-3' for='address" + nr_of_address + "'>Weitere Adresse</label>\
							<div class='col-sm-5'>\
								<textarea rows='4' class='form-control address' name='address" + nr_of_address + "' id='address" + nr_of_address + "' data-type='other'></textarea>\
							</div>\
							<div class='col-sm-2'>\
								<a href='#' id='remaddress" + nr_of_address + "' class='remove_address'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
							</div></div>\
					");
				}
			});
			$(document).off("click",".remove_address").on("click",".remove_address", function(e){
				e.preventDefault();
				if(confirm("Wirklich entfernen?")){
					$(this).parent("div").parent("div").remove(); nr_of_address--;
				}
			});
			$(document).off("click","#addemail").on("click","#addemail", function(e) {
				e.preventDefault();
				if(nr_of_email < max_fields){
					nr_of_email++;
					$(".email-group:last").after("\
						<div id='email-group" + nr_of_email + "' class='added form-group email-group'>\
							<label class='control-label col-sm-3' for='email" + nr_of_email + "'>Weitere E-Mail-Adresse</label>\
							<div class='col-sm-5'>\
								<input type='text' class='form-control email' name='email" + nr_of_email + "' id='email" + nr_of_email + "' data-type='other'>\
							</div>\
							<div class='col-sm-2'>\
								<a href='#' id='rememail" + nr_of_email + "' class='remove_email'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
							</div></div>\
					");
				}
			});
			$(document).off("click",".remove_email").on("click",".remove_email", function(e){
				e.preventDefault();
				if(confirm("Wirklich entfernen?")){
					$(this).parent("div").parent("div").remove(); nr_of_email--;
				}
			});
		});
	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		// , jqXHR, textStatus, errorThrown
		$("#output_body").empty().html("Beim Laden der Rezensenten ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
		console.log("Beim Laden der Rezensenten ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
	});
};

function updateReview(id, type, date, revid, payload) {
	$.ajax({
		type        : "POST",
		url         : "backend.php",
		data        : {
			"action":"resupdate",
			"id" : id,
			"type" : type,
			"date" : date,
			"revid" : revid,
			"payload" : payload
		},
		dataType    : "json",
		encode      : true

	})
	.done(function(e) {
		$.bootstrapSortable();
		$(document).ready( function() {
			// $("#fields").select2();
			// console.log(e);
			// console.log(e.foo);
		});
	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		console.log(jqXHR);
		console.log(jqXHR["payload"]);
		$("#output_body").empty().html("Beim Laden der Rezensenten ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
	});
};
$(function() {


/*Note: At present, using .val() on textarea elements strips carriage return characters from the browser-reported value. When this value is sent to the server via XHR however, carriage returns are preserved (or added by browsers which do not include them in the raw value). A workaround for this issue can be achieved using a valHook as follows:*/

	$.valHooks.textarea = {
		get: function( elem ) {
			return elem.value.replace( /\r?\n/g, "\r\n" );
		}
	};
	$("a").click(function(e) {
		e.preventDefault();
	});
	$(".nav.navbar-nav > li").on("click", function(e) {
		$(".nav.navbar-nav > li").removeClass("active");
		$(this).addClass("active");
	});

	$.ajax({
		type        : "POST",
		url         : "backend.php",
		data        : {"action":"loginstate"},
		dataType    : "json"
	})
	.done(function(e) {

		$("#nav_login").off("click").click(function() {
			$(".output").empty();
			$("#main").hide(1000);
			if (e.loggedin == true) {
				$("#new_pass, #new_pass2").remove();
				$("#pass").attr("placeholder", "Passwort").hide();
				$("#login").val("Abmelden");
			}else {
				$("#login").val("Anmelden");
			}
			$("#login").off("click").click(function() {
				var user = $("#user").val();
				var pass = $("#pass").val();
				$("#failed_login").remove();
				if ($("#nav_login").data("loggedin") != true) {
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"login", "user":user, "password":pass},
						dataType    : "json"
					})
					.done(function(e) {
						console.log(e);
						if (e.loggedin == true) {
							location.reload();
						} else {
							$("#pass").after("<span id='failed_login'>Die Anmeldung ist fehlgeschlagen, bitte versuchen Sie es erneut!</span>")
						}
					})
					.fail(function(e) {
						$("#pass").after("Da ist etwas Technisches schiefgegangen!")
					});
				} else {
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"logout"},
						dataType    : "json"
					})
					.done(function(e) {
						console.log(e);
						location.reload();
					})
					.fail(function(e) {
						$("#pass").after("Da ist etwas Technisches schiefgegangen!")
					});

				}
			});
			$("#register").off("click").click(function() {
				var user = $("#user").val();
				var pass = $("#pass").val();
				if (user && pass) {
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"register", "user":user, "password":pass},
						dataType    : "json"
					})
					.done(function(e) {
						console.log(e);
						location.reload();
					})
					.fail(function(e) {
						console.log(e);
						console.log(e.html);
						$("#pass").after("Da ist etwas Technisches schiefgegangen!")
					});
				} else {
					alert(unescape("Bitte gew%FCnschten Benutzernamen und Passwort eintragen!"));
				}
			});
			$("#change_pass").off("click").click(function() {
				$("#new_pass, #new_pass2").remove();
				$("#pass").attr("placeholder", "Altes Passwort").show();
				$("#pass").after("<input type='password' id='new_pass' name='new_pass' placeholder='Neues Passwort'><input type='password' id='new_pass2' name='new_pass2' placeholder='Neues Passwort wiederholen'>");
				$("#login").val(unescape("Passwort %E4ndern"));


				$("#login").off("click").click(function() {
					var user = $("#user").val();
					var pass = $("#pass").val();
					var new_pass = $("#new_pass").val();
					var new_pass2 = $("#new_pass2").val();
					if (new_pass == new_pass2) {
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action":"change_pass", "user":user, "old_pass":pass, "new_pass":new_pass},
							dataType    : "json"
						})
						.done(function(e) {
							console.log(e);
							$("#login-modal").modal("hide");
						})
						.fail(function(e) {
							$("#new_pass2").after("Da ist etwas Technisches schiefgegangen!")
						});

					} else {
						$("#new_pass2").after("Die neuen Passw&ouml;rter sind unterschiedlich");
					}
				});
			});
		});

		if (e.loggedin == true) {
			$("#nav_login").html(e.user).data("user", e.user);
			$("#nav_login").data("loggedin", true);
			$("#nav_books, #nav_reviewers, #nav_reviews, #nav_bugreports, #nav_admin").parent().removeClass("disabled");
			$("#user, #pass, h1, #register").hide();

// console.log($("#nav_login").data("loggedin"));

	$("#nav_books").off("click").click(function() {
		$(".output").empty();
		$("#main").hide(1000);
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"books"},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output_header").empty().html(e.header);
			getBooks("","");
			$(".booksearch").off("keyup").keyup(function() {
				var	titlesearch	= $("input[name=titlesearch]").val();
				var	authorsearch  = $("input[name=authorsearch]").val();
				getBooks(titlesearch, authorsearch);
			});
			$("#bookmodal").off("show.bs.modal").on("show.bs.modal", function(e) {
				$("#authorresult").remove();
				$("#titleresult").remove();
				var active_input = "";
				$(".book_inputs").off("click").click(function() {
					active_input = this.id;
					console.log(active_input);
				});
				$("#nbsp_btn").off("click").click(function() {
					addText(active_input, "&nbsp;");
				});
				$("#quotes_btn").off("click").click(function() {
					$("#" + active_input).surroundSelectedText("&#132;","&#147;");
				});
				$("#kursiv_btn").off("click").click(function() {
					$("#" + active_input).surroundSelectedText("[i]","[/i]");
				});
				$("#kapit_btn").off("click").click(function() {
					$("#" + active_input).surroundSelectedText("[k]","[/k]");
				});
				if(e.relatedTarget.id!="newbook_button"){
					var bookid = $(e.relatedTarget).data("bookid");
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"bookdata", "bookid": bookid},
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$("#title").val(e.title);
						$("#author").val(e.author);
						$("#publisher").val(e.publisher);
						$("#pub_country").val(e.pub_country);
						$("#price").val(e.price);
						$("#note").val(e.note);
						$("#pages").val(e.pages);
						$("#series").val(e.series);
						$("#nr_in_series").val(e.nr_in_series);
						$("#pub_date").val(e.pub_date);
						$("#isbn").val(e.isbn);
						$("#currency").val(e.currency);
						$("#media").val(e.media);
						var pub_year = e.pub_date.slice(-4);
						var price = e.price.replace(".", ",");
						if (e.series) {
							if (e.nr_in_series) {
								var series = "(" + e.series + ". ";
								var nr_in_series = e.nr_in_series + "). ";
							} else {
								var series = "(" + e.series + "). ";
								var nr_in_series = "";
							}
						} else {
							var series = "";
							var nr_in_series = "";
						}
						if (e.media) {
							var media = "(" + e.media + ") ";
						} else {
							var media = "";
						}

						var book_data = e.author + " (" + pub_year + "): " + e.title + ((e.title.slice(-1)=="?"||e.title.slice(-1)=="!")?" ":". ") + e.pub_country + ": " + e.publisher + ". " + e.pages + "&nbsp;S. " + media + series + nr_in_series + e.currency + "&nbsp;" + price;

						book_data = book_data.replace(/\[i\]/g,'<i>').replace(/\[\/i\]/g,'</i>').replace(/\[k\]/g,'<span style="font-variant:small-caps;">').replace(/\[\/k\]/g,'</span>');

						$("#show_book_data1").html(book_data);
						$("#show_book_data2").html(book_data);
						$("#show_book_data3").html(book_data);

					})
					.fail(function(jqXHR, textStatus, errorThrown) {
						$("#output_body").empty().html("Beim Laden der B&uuml;chersuche ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
					});

					$("#updatebook").off("click").click(function(e) {
						var bookupdate_formdata = {
							"action" 		: "bookupdate",
							"bookid"		: bookid,
							"title"			: $('input[name=title]').val(),
							"author"		: $('input[name=author]').val(),
							"publisher"		: $('input[name=publisher]').val(),
							"pub_country"	: $('input[name=pub_country]').val(),
							"price"			: $('input[name=price]').val(),
							"note"			: $('textarea[name=note]').val(),
							"pages"			: $('input[name=pages]').val(),
							"series"		: $('input[name=series]').val(),
							"nr_in_series"	: $('input[name=nr_in_series]').val(),
							"pub_date"		: $('input[name=pub_date]').val(),
							"isbn"			: $('input[name=isbn]').val(),
							"currency"	: $('input[name=currency]').val(),
							"media"	: $('input[name=media]').val()
						};
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : bookupdate_formdata,
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							console.log(e);
							console.log("done");
							// $("#bookmodal").modal("hide");
							alert("Gespeichert!");
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							console.log("fail");
							$("#output_error").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + bookupdate_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
						});
					});
					// "(noch nicht|nicht mehr) vergebene bücher" vs "vergebene bücher"
					if($(e.relatedTarget).data("type")==2){
						$("#findreviewermodal_label").html("Rezension suchen");
						var button_type = "vergeben";
					}else{
						$("#findreviewermodal_label").html("Rezensenten suchen");
						var button_type = "nicht";
					};
					$("#findreviewermodal").off("click").click(function(e) {
						if (button_type=="nicht") {
							$("#nav_reviewers").data("bookid", bookid);
							$("#nav_reviewers").trigger("click");
						} else {
							var bookname = $('input[name=title]').val();
							$("#nav_reviews").data("bookn", bookname);
							$("#nav_reviews").trigger("click");
						}
					});
					$("#deletebook").off("click").click(function(e) {
						var bookdelete_formdata = {
							"action" 		: "bookdelete",
							"bookid"		: bookid
						};
						if(confirm("Wirklich entfernen?")){
							$.ajax({
								type        : "POST",
								url         : "backend.php",
								data        : bookdelete_formdata,
								dataType    : "json",
								encode      : true
							})
							.done(function(e) {
								console.log(e.response);
								$("#bookmodal").modal("hide");
							})
							.fail(function(jqXHR, textStatus, errorThrown) {
								console.log(jqXHR);
								$("#output_error").html("Oops: Beim L&ouml;schen des Buches ist etwas schiefgelaufen<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
							});
						}
					});
					$("#show_book_data_tab").show();
					$("#book_modal_label").html("Daten &auml;ndern");
					$("#deletebook").show();
				}else{
					$("#title").val("");
					$("#author").val("");
					$("#publisher").val("");
					$("#pub_country").val("");
					$("#price").val("");
					$("#note").val("");
					$("#pages").val("");
					$("#series").val("");
					$("#nr_in_series").val("");
					$("#pub_date").val("");
					$("#isbn").val("");
					$("#currency").val("\u20ac");
					$("#media").val("");
					$("#titleresult").remove();
					$("#authorresult").remove();
					$("#title").keyup(function() {
						$("#titleresult").remove();
						var	title	= $("input[name=title]").val();
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action":"booksearch", "title": title, "author": ""},
							dataType    : "json"
						})
						.done(function(e) {
							if (e.body.length >883){
								$("#title-group").append("<div id='titleresult'></div>");
								var table = $("<div>" + e.body + "</div>");
								$("#titleresult").html(table.find("table").parent().html());
								console.log(e.body.length);
							}else{
								console.log(e.body.length);
							}
						});
					});
					$("#author").keyup(function() {
						$("#authorresult").remove();
						var	author  = $("input[name=author]").val();

						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action":"booksearch", "title": "", "author": author},
							dataType    : "json"
						})
						.done(function(e) {
							if (e.body.length >883){
								$("#author-group").append("<div id='authorresult'></div>");
								var table = $("<div>" + e.body + "</div>");
								$("#authorresult").html(table.find("table").parent().html());
								console.log(e.body.length);
							}else{
								console.log(e.body.length);
							}
						});
					});
					$("#updatebook").off("click").click(function(e) {
						var bookupdate_formdata = {
							"action" 		: "bookupdate",
							"bookid"		: "0",
							"title"			: $('input[name=title]').val(),
							"author"		: $('input[name=author]').val(),
							"publisher"		: $('input[name=publisher]').val(),
							"pub_country"	: $('input[name=pub_country]').val(),
							"price"			: $('input[name=price]').val(),
							"note"			: $('textarea[name=note]').val(),
							"pages"			: $('input[name=pages]').val(),
							"series"		: $('input[name=series]').val(),
							"nr_in_series"	: $('input[name=nr_in_series]').val(),
							"pub_date"		: $('input[name=pub_date]').val(),
							"isbn"			: $('input[name=isbn]').val(),
							"currency"	: $('input[name=currency]').val(),
							"media"	: $('input[name=media]').val()
						};
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : bookupdate_formdata,
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							console.log(e);
							console.log("done");

							// hier muss die neue id übernommen und gespeichert werden

							// $("#bookmodal").modal("hide");
							alert("Gespeichert!");
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							console.log("fail");
							$("#output_error").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + bookupdate_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
						});
					});

					// hier muss reviewers aufgerufen und die id übergeben werden
					$("#findreviewermodal").off("click").click(function(e) {
						var bookupdate_formdata = {
							"action" 		: "bookupdate",
							"bookid"		: "0",
							"title"			: $('input[name=title]').val(),
							"author"		: $('input[name=author]').val(),
							"publisher"		: $('input[name=publisher]').val(),
							"pub_country"	: $('input[name=pub_country]').val(),
							"price"			: $('input[name=price]').val(),
							"note"			: $('textarea[name=note]').val(),
							"pages"			: $('input[name=pages]').val(),
							"series"		: $('input[name=series]').val(),
							"nr_in_series"	: $('input[name=nr_in_series]').val(),
							"pub_date"		: $('input[name=pub_date]').val(),
							"isbn"			: $('input[name=isbn]').val(),
							"currency"	: $('input[name=currency]').val(),
							"media"	: $('input[name=media]').val()
						};
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : bookupdate_formdata,
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							console.log(e.bookid);
							console.log("done");

							// hier muss die neue id übernommen und gespeichert werden
							var bookid = e.bookid;
							$("#nav_reviewers").data("bookid", bookid);
							$("#nav_reviewers").trigger("click");
							$("#bookmodal").modal("hide");
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							console.log("fail");
							$("#output_error").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + bookupdate_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
						});
					});
					$("#show_book_data_tab").hide();
					$("#book_modal_label").text("Neues Buch");
					$("#findreviewermodal_label").html("Speichern und Rezensenten suchen");
					$("#deletebook").hide();
				} // if-else endet hier
				$("#isbn").keyup(function() {
					if ($("#isbn").val().length==13) {
						console.log($("#isbn").val().length);
					}
				});
			});
			$("#bookmodal").off("hide.bs.modal").on("hide.bs.modal", function (e) {
				var	titlesearch	= $("input[name=titlesearch]").val();
				var	authorsearch  = $("input[name=authorsearch]").val();
				getBooks(titlesearch, authorsearch);
			});
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			$("#output_header").empty().html("Beim Laden der B&uuml;chersuche ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
		});
	});

	$("#nav_reviewers").off("click").click(function() {
		$(".output").empty();
		$("#main").hide(1000);
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"reviewers"},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output_header").html(e.header);
			$(document).ready( function() {
				$("#fieldssearch").select2({
					placeholder: "...",
					allowClear: true
				});
				$(".rateit").rateit();
				getReviewers("","","");
				$.bootstrapSortable();
				$(document).off("keyup", "#namesearch").on("keyup", "#namesearch", function() {
					getReviewers($("input[name=namesearch]").val(),$("input[name=addresssearch]").val(),$("select[name=fieldssearch]").val());
				});
				$(document).off("keyup", "#addresssearch").on("keyup", "#addresssearch", function() {
					getReviewers($("input[name=namesearch]").val(),$("input[name=addresssearch]").val(),$("select[name=fieldssearch]").val());
				});
				$(document).off("change", "#fieldssearch").on("change", "#fieldssearch", function() {
					getReviewers($("input[name=namesearch]").val(),$("input[name=addresssearch]").val(),$("select[name=fieldssearch]").val());
				});
				var nr_of_address = 0;
				var nr_of_email = 0;
				$("#rermodal").off("show.bs.modal").on("show.bs.modal", function(e) {
					var callSource = $(e.relatedTarget);
					var reviewerid = callSource.data("reviewerid");
					var modal = $("#rermodal");
					$("#new_review_popover_btn").popover({
						html: true,
						content: function() {
				      return $("#new_review_popover_content").html();
				    }
					});

					if ($("#nav_reviewers").data("bookid")&&reviewerid!=0) {
						$("#new_review_popover_btn").show();
						$("#new_review_popover_btn").on("shown.bs.popover", function () {
							$("#new_review_deadline").datepicker().datepicker("option", $.datepicker.regional.de).datepicker("setDate", "+6m").datepicker("option", "changeYear", "true").datepicker("option", "changeMonth", "true");
							$("#assign_rer_to_book").off("click").click(function() {
								if(confirm("Sind Sie sicher?")) {
									//schick an server, verknüpf die beiden
									var bookid = $("#nav_reviewers").data("bookid");
									var deadline = $("#new_review_deadline").val();
									$.ajax({
										type        : "POST",
										url         : "backend.php",
										data        : {"action":"newreview", "bookid" : bookid, "rerid" : reviewerid, "deadline" : deadline},
										dataType    : "json"
									})
									.done(function(e) {
										$(".output").empty();
										$(document).ready( function() {
											$("#nav_reviewers").removeData();
											$("#nav_reviews").trigger("click");
										});
										console.log(e);

									})
									.fail(function(jqXHR, textStatus, errorThrown) {
										$(".output").empty();
										$("#output_header").html("Beim Verkn&uuml;pfen ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR)+ "<br>___<br>" + jqXHR.response);
										console.log(jqXHR);
									});
								}
							});
						});
					} else {
						$("#new_review_popover_btn").hide();
					}

					if(reviewerid!=0){
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action":"rerdata", "rerid":reviewerid},
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							$(document).ready( function() {
								var rerid						= e.id;
								var rerfirstname    = e.firstname;
								var rerlastname     = e.lastname;
								var rertitle        = e.title;
								var rergender       = e.gender;
								var rerpublished    = e.published;
								var rerrequests     = e.requests;
								var rerdeclined       = e.declined;
								var rerwarned       = e.warned;
								var rernote         = e.note;
								var rerpublished_in = e.published_in;
								var rerform         = e.form / 10;
								var rercontent      = e.content / 10;
								var rerformnote     = e.formnote ;
								var rercontentnote  = e.contentnote;
								var reraddress			= e.address;
								var reremail				= e.email;
								var rerfields				= e.fields;
								modal.find("#firstname").val(rerfirstname);
								modal.find("#lastname").val(rerlastname);
								modal.find("#title").val(rertitle);
								modal.find("label").removeClass("active");
								if (rergender) {
									modal.find("label[for=" + rergender + "]").addClass("active");
									modal.find("input:radio[id=" + rergender + "]").prop("checked", true);
								}
								modal.find("#published").val(rerpublished);
								modal.find("#requests").val(rerrequests);
								modal.find("#declined").val(rerdeclined);
								modal.find("#warned").val(rerwarned);
								modal.find("#note").val(rernote);
								modal.find("#published_in").val(rerpublished_in);
								modal.find("#rate_form_div").rateit("value", rerform);
								modal.find("#rateform_note").val(rerformnote);
								modal.find("#rate_content_div").rateit("value", rercontent);
								modal.find("#ratecontent_note").val(rercontentnote);
								var rerfieldsarray = rerfields.split(",");
								modal.find("#fields").val(rerfieldsarray);
								modal.find("#fields").select2();
								modal.find("#rerid").data("rerid", rerid);
								$(".added").remove();
								$.each(reraddress, function(i, val){
									$(".address-group:last").after("\
										<div id='address-group" + i + "' class='added form-group address-group'>\
											<label class='control-label col-sm-3' for='address" + i + "'>" + (val[1]=="other"?"Weitere A":"Standarda") + "dresse</label>\
											<div class='col-sm-5'>\
												<textarea rows='4' data-type='" + val[1] + "' data-addressid='" + val[2] + "' class='form-control address' name='address" + i + "' id='address" + i + "'>" + val[0] + "</textarea>\
											</div>\
											<div class='col-sm-2'>\
												<a href='#' id='remaddress" + i + "' class='remove_address'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
											</div></div>\
									");
								});
								$.each(reremail, function(i, val){
									$(".email-group:last").after("\
										<div id='email-group" + i + "' class='added form-group email-group'>\
											<label class='control-label col-sm-3' for='email" + i + "'>" + (val[1]=="other"?"Weitere ":"Standard-") + "E-Mail-Adresse</label>\
											<div class='col-sm-5'>\
												<input type='text' class='form-control email' name='email" + i + "' id='email" + i + "' value='" + val[0] + "' data-type='" + val[1] + "' data-emailid='" + val[2] + "'>\
											</div>\
											<div class='col-sm-2'>\
												<a href='#' id='rememail" + i + "' class='remove_email'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
											</div>\
										</div>\
									");
								});
								nr_of_email = reremail.length + 1;
								nr_of_address = reraddress.length + 1;
							});
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							$("#output_error").html("Oops: <br>" + JSON.stringify(e) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
						});
					} else {
						modal.find("#firstname").val("");
						modal.find("#lastname").val("");
						modal.find("#title").val("");
						modal.find("label").removeClass("active");
						modal.find("#published").val("");
						modal.find("#requests").val("");
						modal.find("#declined").val("");
						modal.find("#warned").val("");
						modal.find("#note").val("");
						modal.find("#published_in").val("");
						modal.find("#rate_form_div").rateit("value", 0);
						modal.find("#rateform_note").val("");
						modal.find("#rate_content_div").rateit("value", 0);
						modal.find("#ratecontent_note").val("");
						modal.find("#fields").val("");
						modal.find("#fields").select2();
						$(".added").remove();
						$(".address-group:last").after("\
							<div id='address-group1' class='added form-group address-group'><label class='control-label col-sm-3' for='address1'>Standard-Adresse</label><div class='col-sm-5'><textarea rows='4' class='form-control address' name='address1' id='address1' data-type='standard'></textarea></div><div class='col-sm-4'></div>");
						$(".email-group:last").after("\
							<div id='email-group1' class='added form-group email-group'><label class='control-label col-sm-3' for='email1'>Standard-E-Mail-Adresse</label><div class='col-sm-5'><input type='text' class='form-control email' name='email1' id='email1' data-type='standard'></div><div class='col-sm-4'></div>");
						nr_of_email = 1;
						nr_of_address = 1;
					}

				$("#findreviewmodal").off("click").click(function(e) {
					var firstn = modal.find("#firstname").val();
					var lastn = modal.find("#lastname").val();
					$("#nav_reviews").data("firstname", firstn);
					$("#nav_reviews").data("lastname", lastn);
					$("#nav_reviews").trigger("click");
				});
			});

				$("#rermodal").off("shown.bs.modal").on("shown.bs.modal", function () {
					$("#firstname").focus();
				});

				$("#updaterer").off("click").click(function(e) {

					// mal existiert address/email_id, mal nicht. muss kombinieren mit existenz einer rerid. morgen.

					var address = [];
					$(".address").each(function(){
						address.push($(this).val());
						address.push($(this).data("type"));
						($(this).data("addressid")?address.push($(this).data("addressid")):address.push("0"))
					});
					var email = [];
					$(".email").each(function(){
						email.push($(this).val());
						email.push($(this).data("type"));
						($(this).data("emailid")?email.push($(this).data("emailid")):email.push("0"))
					});
					var rerid = $("#rerid").data("rerid");
					var rerupdate_formdata = {
						"action" 				: "rerupdate",
						"id"						: rerid,
						"firstname" 		:	$('input[name=firstname]').val(),
						"lastname" 			:	$('input[name=lastname]').val(),
						"title" 				:	$('input[name=title]').val(),
						"gender"			 	:	$('input[name=gender]:checked').val(),
						"published" 		:	$('input[name=published]').val(),
						"requests"		 	:	$('input[name=requests]').val(),
						"declined" 			:	$('input[name=declined]').val(),
						"warned" 				:	$('input[name=warned]').val(),
						"note" 					:	$('#note').val(),
						"published_in"	:	$('input[name=published_in]').val(),
						"form" 					:	$('#rate_form_div').rateit("value")*10,
						"content" 			:	$('#rate_content_div').rateit("value")*10,
						"formnote" 			:	$('#rateform_note').val(),
						"contentnote" 	:	$('#ratecontent_note').val(),
						"address"				:	address,
						"email"					:	email,
						"fields"				:	$('input[name=fields]').val(),
						"isblocked"			: "false"
					};

					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : rerupdate_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$("#rermodal").modal("hide");
					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						console.log("bar");
						$("#output_error").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + rerupdate_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
					});
				});

				// 	/*
				// 	$("#findreviewermodal").off("click").click(function (e) {
				// 		$("#output").html(" id:" + bookid + "<br>");
				//
				// 	});*/
				$("#deleterer").off("click").click(function (e) {
					var rerdelete_formdata = {
						"action" : "rerdelete",
						"rerid"	 : rerid
					};
					if(confirm("Wirklich entfernen?")){
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : rerdelete_formdata,
							dataType    : "json"
						})
						.done(function(e) {
							console.log(e);
							$("#rermodal").modal("hide");
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							$("#output_error").html("Oops: Beim l&ouml;schen des rezensenten ist was schiefgelaufen<br>" +  errorThrown + "<br><br>" +  JSON.stringify(e));
						});
					}
				});
				$("#rermodal").off("hide.bs.modal").on("hide.bs.modal", function (e) {
					$("#output_body").empty();
					getReviewers($("input[name=namesearch]").val(),$("input[name=addresssearch]").val(),$("select[name=fieldssearch]").val());
				});

			}); // document.ready
			})
			.fail(function(e, jqXHR, textStatus, errorThrown ) {
				console.log(e.response);
				$("#output_error").html("Oops: beim laden der rezensentensuche ist was schiefgelaufen<br><br>" + textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
			});
	});

	$("#nav_reviews").off("click").click(function() {
		$(".output").empty();
		$("#main").hide(1000);

		if ($("#nav_reviews").data("bookn")) {
			var bookn = $("#nav_reviews").data("bookn");
			$("#nav_reviews").removeData();
		} else {
			var bookn = "";
		}

		if ($("#nav_reviews").data("firstname")) {
			var firstn = $("#nav_reviews").data("firstname");
			var lastn = $("#nav_reviews").data("lastname");
			$("#nav_reviews").removeData();
		} else {
			var firstn = "";
			var lastn = "";
		}

		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"reviews", "firstname":firstn, "lastname":lastn, "book":bookn, "heftnr":"","heftnr2":""},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output_header").html(e.html2);

			var reviews_body = e.html.replace(/\[i\]/g,'<i>').replace(/\[\/i\]/g,'</i>').replace(/\[k\]/g,'<span style="font-variant:small-caps;">').replace(/\[\/k\]/g,'</span>');
			$("#output_body").html(reviews_body);

			$(document).ready( function() {
				$("#firstname").val(firstn);
				$("#lastname").val(lastn);
				$.bootstrapSortable();
				$(".table").floatThead({
					scrollContainer: function($table){
						return $table.closest('.wrapper');
					}
				});

				$("#rev_deadline_date").datepicker().datepicker("option", $.datepicker.regional.de).datepicker("setDate", "+6m").datepicker("option", "changeYear", "true").datepicker("option", "changeMonth", "true");
				$("#review_arrived_date").datepicker().datepicker("option", $.datepicker.regional.de).datepicker("setDate", "today").datepicker("option", "changeYear", "true").datepicker("option", "changeMonth", "true");
				$(".rateit").rateit();
				$(".reviewsearch").keyup(function() {
					var findreview_formdata = {
						"action"		 : "reviews",
						"firstname"	 : $("#firstname").val(),
						"lastname"	 : $("#lastname").val(),
						"book"			 : $("#book").val(),
						"heftnr"		 : $("#heftnr").val(),
						"heftnr2"		 : $("#heftnr2").val()
					};
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : findreview_formdata,
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$(".added").remove();
						$("#output_body").html(e.html);

					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						$("#output_body").html("Oops: <br>" + JSON.stringify(findreview_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
					});
				});

				var count1 = $("#reviews_table1 tr").length -1;
				var count2 = $("#reviews_table2 tr").length -1;
				var count3 = $("#reviews_table3 tr").length -1;
				var count4 = $("#reviews_table4 tr").length -1;
				$("#counter1").html(count1);
				$("#counter2").html(count2);
				$("#counter3").html(count3);
				$("#counter4").html(count4);

				$("#zdl_filter_popover").popover({
					html: true,
					content: function() {
						return $("#zdl_filter_content").html();
					}
				});

				$("#zdl_filter_popover").off("shown.bs.popover").on("shown.bs.popover", function () {
					console.log("fired");
					$(".res_filter2").change(function() {
						var which_state = $(this).data("res_state");
						console.log(which_state);
						console.log(typeof which_state);
						if (typeof which_state == "object") {
							for (var i = which_state[0]; i < which_state[1]; i++) {
								if ($(this).is(':checked')) {
									$("tr[data-res_state='" + i + "']").show();
								} else {
									$("tr[data-res_state='" + i + "']").hide();
								}
							}
						} else {
							if ($(this).is(':checked')) {
								$("tr[data-res_state='" + which_state + "']").show();
							} else {
								$("tr[data-res_state='" + which_state + "']").hide();
							}
						}
						var count1 = $("#reviews_table1 tr:visible").length -1;
						var count2 = $("#reviews_table2 tr:visible").length -1;
						var count3 = $("#reviews_table3 tr:visible").length -1;
						var count4 = $("#reviews_table4 tr:visible").length -1;
						$("#counter1").html(count1);
						$("#counter2").html(count2);
						$("#counter3").html(count3);
						$("#counter4").html(count4);
					});

				});

				$(".res_filter").change(function() {
					var which_state = $(this).data("res_state");
					console.log(which_state);
					console.log(typeof which_state);
					if (typeof which_state == "object") {
						for (var i = which_state[0]; i < which_state[1]; i++) {
							if ($(this).is(':checked')) {
								$("tr[data-res_state='" + i + "']").show();
							} else {
								$("tr[data-res_state='" + i + "']").hide();
							}
						}
					} else {
						if ($(this).is(':checked')) {
							$("tr[data-res_state='" + which_state + "']").show();
						} else {
							$("tr[data-res_state='" + which_state + "']").hide();
						}
					}
					var count1 = $("#reviews_table1 tr:visible").length -1;
					var count2 = $("#reviews_table2 tr:visible").length -1;
					var count3 = $("#reviews_table3 tr:visible").length -1;
					var count4 = $("#reviews_table4 tr:visible").length -1;
					$("#counter1").html(count1);
					$("#counter2").html(count2);
					$("#counter3").html(count3);
					$("#counter4").html(count4);
				});

				$(".cb_container").click(function () {
					var checkbox = $(this).find('input');
					var glyph = $(this).find('span');
					console.log(glyph.hasClass('hidden'));
					checkbox.prop('checked', !checkbox[0].checked);
					if (glyph.hasClass('hidden')) {
						glyph.show();
						glyph.removeClass('hidden');
					} else {
						glyph.hide();
						glyph.addClass('hidden');
					}
				});

				$("#batch_zum_setzen").off("click").click(function() {
					var array = [];
					$("input[name^='review_select_']:checked").each(function() {
						array.push($(this).data("reviewid"));
						($(this).data("reviewerid")?array.push($(this).data("reviewerid")):array.push(0));
					});
					console.log(array);
					updateReview("","to_set","","",array);
					$("#nav_reviews").trigger("click");
				});

				$("#batch_vom_setzen").off("click").click(function() {
					var array = [];
					$("input[name^='review_select_']:checked").each(function() {
						array.push($(this).data("reviewid"));
						($(this).data("reviewerid")?array.push($(this).data("reviewerid")):array.push(0));
					});
					console.log(array);
					updateReview("","from_set","","",array);
					$("#nav_reviews").trigger("click");
				});

				$("#batch_heft_zuordnung").off("click").click(function() {
					var array = [];
					$("input[name^='review_select_']:checked").each(function() {
						array.push($(this).data("reviewid"));
						($(this).data("reviewerid")?array.push($(this).data("reviewerid")):array.push(0));
					});
					console.log(array);
					var heft_nr = prompt("Welchem Heft sollen diese Rezensionen zugeordnet werden?");
					if (heft_nr) {
						updateReview(heft_nr,"heft","","",array);
						$("#nav_reviews").trigger("click");
					}
				});

				$("#bookmodal").off("show.bs.modal").on("show.bs.modal", function(e) {
					$("#authorresult").remove();
					$("#titleresult").remove();
					var active_input = "";
					$(".book_inputs").off("click").click(function() {
						active_input = this.id;
						console.log(active_input);
					});
					$("#nbsp_btn").off("click").click(function() {
						addText(active_input, "&nbsp;");
					});
					$("#quotes_btn").off("click").click(function() {
						$("#" + active_input).surroundSelectedText("&#132;","&#147;");
					});
					$("#kursiv_btn").off("click").click(function() {
						$("#" + active_input).surroundSelectedText("[i]","[/i]");
					});
					$("#kapit_btn").off("click").click(function() {
						$("#" + active_input).surroundSelectedText("[k]","[/k]");
					});

					var bookid = $(e.relatedTarget).data("bookid");
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"bookdata", "bookid": bookid},
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$("#title").val(e.title);
						$("#author").val(e.author);
						$("#publisher").val(e.publisher);
						$("#pub_country").val(e.pub_country);
						$("#price").val(e.price);
						$("#note").val(e.note);
						$("#pages").val(e.pages);
						$("#series").val(e.series);
						$("#nr_in_series").val(e.nr_in_series);
						$("#pub_date").val(e.pub_date);
						$("#isbn").val(e.isbn);
						$("#currency").val(e.currency);
						$("#media").val(e.media);
						var pub_year = e.pub_date.slice(-4);
						var price = e.price.replace(".", ",");
						if (e.series) {
							if (e.nr_in_series) {
								var series = "(" + e.series + ". ";
								var nr_in_series = e.nr_in_series + "). ";
							} else {
								var series = "(" + e.series + "). ";
								var nr_in_series = "";
							}
						} else {
							var series = "";
							var nr_in_series = "";
						}
						if (e.media) {
							var media = "(" + e.media + ") ";
						} else {
							var media = "";
						}

						var book_data = e.author + " (" + pub_year + "): " + e.title + ((e.title.slice(-1)=="?"||e.title.slice(-1)=="!")?" ":". ") + e.pub_country + ": " + e.publisher + ". " + e.pages + "&nbsp;S. " + media + series + nr_in_series + e.currency + "&nbsp;" + price;

						book_data = book_data.replace(/\[i\]/g,'<i>').replace(/\[\/i\]/g,'</i>').replace(/\[k\]/g,'<span style="font-variant:small-caps;">').replace(/\[\/k\]/g,'</span>');

						$("#show_book_data1").html(book_data);
						$("#show_book_data2").html(book_data);
						$("#show_book_data3").html(book_data);

					})
					.fail(function(jqXHR, textStatus, errorThrown) {
						$("#output_body").empty().html("Beim Laden der B&uuml;chersuche ist etwas schiefgelaufen:<br><br> Status:" + textStatus + "<br>___<br> Fehler:" + errorThrown + "<br>___<br>" + JSON.stringify(jqXHR));
					});

					$("#updatebook").off("click").click(function(e) {
						var bookupdate_formdata = {
							"action" 		: "bookupdate",
							"bookid"		: bookid,
							"title"			: $('input[name=title]').val(),
							"author"		: $('input[name=author]').val(),
							"publisher"		: $('input[name=publisher]').val(),
							"pub_country"	: $('input[name=pub_country]').val(),
							"price"			: $('input[name=price]').val(),
							"note"			: $('textarea[name=note]').val(),
							"pages"			: $('input[name=pages]').val(),
							"series"		: $('input[name=series]').val(),
							"nr_in_series"	: $('input[name=nr_in_series]').val(),
							"pub_date"		: $('input[name=pub_date]').val(),
							"isbn"			: $('input[name=isbn]').val(),
							"currency"	: $('input[name=currency]').val(),
							"media"	: $('input[name=media]').val()
						};
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : bookupdate_formdata,
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							console.log(e);
							console.log("done");
							// $("#bookmodal").modal("hide");
							alert("Gespeichert!");
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							console.log("fail");
							$("#output_error").html("Oops: Beim speichern des buches ist was schiefgelaufen<br><br><br>" + bookupdate_formdata.title + "<br><br>" + e.ans + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR));
						});
					});
					// "(noch nicht|nicht mehr) vergebene bücher" vs "vergebene bücher"
					if($(e.relatedTarget).data("type")==2){
						$("#findreviewermodal_label").html("Rezension suchen");
						var button_type = "vergeben";
					}else{
						$("#findreviewermodal_label").html("Rezensenten suchen");
						var button_type = "nicht";
					};
					$("#show_book_data_tab").show();
					$("#book_modal_label").html("Daten &auml;ndern");
				});

				$("#bookmodal").off("hide.bs.modal").on("hide.bs.modal", function (e) {
					$("#nav_reviews").trigger("click");
				});

				var nr_of_address = 0;
				var nr_of_email = 0;
				$("#rermodal").off("show.bs.modal").on("show.bs.modal", function(e) {
					var callSource = $(e.relatedTarget);
					var reviewerid = callSource.data("reviewerid");
					var modal = $("#rermodal");
					$("#new_review_popover_btn").popover({
						html: true,
						content: function() {
				      return $("#new_review_popover_content").html();
				    }
					});

					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"rerdata", "rerid":reviewerid},
						dataType    : "json",
						encode      : true
					})
					.done(function(e) {
						$(document).ready( function() {
							var rerid						= e.id;
							var rerfirstname    = e.firstname;
							var rerlastname     = e.lastname;
							var rertitle        = e.title;
							var rergender       = e.gender;
							var rerpublished    = e.published;
							var rerrequests     = e.requests;
							var rerdeclined       = e.declined;
							var rerwarned       = e.warned;
							var rernote         = e.note;
							var rerpublished_in = e.published_in;
							var rerform         = e.form / 10;
							var rercontent      = e.content / 10;
							var rerformnote     = e.formnote ;
							var rercontentnote  = e.contentnote;
							var reraddress			= e.address;
							var reremail				= e.email;
							var rerfields				= e.fields;
							modal.find("#firstname").val(rerfirstname);
							modal.find("#lastname").val(rerlastname);
							modal.find("#title").val(rertitle);
							modal.find("label").removeClass("active");
							if (rergender) {
								modal.find("label[for=" + rergender + "]").addClass("active");
								modal.find("input:radio[id=" + rergender + "]").prop("checked", true);
							}
							modal.find("#published").val(rerpublished);
							modal.find("#requests").val(rerrequests);
							modal.find("#declined").val(rerdeclined);
							modal.find("#warned").val(rerwarned);
							modal.find("#note").val(rernote);
							modal.find("#published_in").val(rerpublished_in);
							modal.find("#rate_form_div").rateit("value", rerform);
							modal.find("#rateform_note").val(rerformnote);
							modal.find("#rate_content_div").rateit("value", rercontent);
							modal.find("#ratecontent_note").val(rercontentnote);
							var rerfieldsarray = rerfields.split(",");
							modal.find("#fields").val(rerfieldsarray);
							modal.find("#fields").select2();
							modal.find("#rerid").data("rerid", rerid);
							$.each(reraddress, function(i, val){
								$(".address-group:last").after("\
									<div id='address-group" + i + "' class='added form-group address-group'>\
										<label class='control-label col-sm-3' for='address" + i + "'>" + (val[1]=="other"?"Weitere A":"Standarda") + "dresse</label>\
										<div class='col-sm-5'>\
											<textarea rows='4' data-type='" + val[1] + "' data-addressid='" + val[2] + "' class='form-control address' name='address" + i + "' id='address" + i + "'>" + val[0] + "</textarea>\
										</div>\
										<div class='col-sm-2'>\
											<a href='#' id='remaddress" + i + "' class='remove_address'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
										</div></div>\
								");
							});
							$.each(reremail, function(i, val){
								$(".email-group:last").after("\
									<div id='email-group" + i + "' class='added form-group email-group'>\
										<label class='control-label col-sm-3' for='email" + i + "'>" + (val[1]=="other"?"Weitere ":"Standard-") + "E-Mail-Adresse</label>\
										<div class='col-sm-5'>\
											<input type='text' class='form-control email' name='email" + i + "' id='email" + i + "' value='" + val[0] + "' data-type='" + val[1] + "' data-emailid='" + val[2] + "'>\
										</div>\
										<div class='col-sm-2'>\
											<a href='#' id='rememail" + i + "' class='remove_email'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>Entfernen</a>\
										</div>\
									</div>\
								");
							});
							nr_of_email = reremail.length + 1;
							nr_of_address = reraddress.length + 1;
						});
					})
					.fail(function(e, errorThrown, jqXHR) {
						console.log(e);
						$("#output_error").html("Oops: <br>" + JSON.stringify(e) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
					});

				$("#findreviewmodal").off("click").click(function(e) {
					var firstn = modal.find("#firstname").val();
					var lastn = modal.find("#lastname").val();
					$("#nav_reviews").data("firstname", firstn);
					$("#nav_reviews").data("lastname", lastn);
					$("#nav_reviews").trigger("click");
				});
			});

				$("#rermodal").off("shown.bs.modal").on("shown.bs.modal", function () {
					$("#firstname").focus();
				});

				$("#rermodal").off("hide.bs.modal").on("hide.bs.modal", function (e) {
					$("#nav_reviews").trigger("click");
				});

				$(".reviews").off("click").click(function() {
					var event_id = "event" + $(this).data("reviewid");
					var event_exists = document.getElementById(event_id);
					console.log($(this).data("reviewid"));
					$(".event").remove();
					if (!event_exists) {
						$(this).parent().after($(e.events_table).filter("#event" + $(this).data("reviewid")));
					}

					$(".event").off("click").click(function() {
						var reviewid = $(this).data("reviewid");
						var reviewerid = $(this).data("reviewerid");
						// updateReview(id, type, date, revid, payload)
						$("#rev_decline_btn").off("click").click(function() {
							updateReview(reviewid,"declined","",reviewerid,$("#rev_decline_reason").val());
							$("#nav_reviews").trigger("click");
						});
						$("#rev_deadline_btn").off("click").click(function() {
							updateReview(reviewid,"new_deadline",$("#rev_deadline_date").val(),reviewerid,$("#rev_deadline_note").val());
							$("#nav_reviews").trigger("click");
						});
						$("#rev_warning_btn").off("click").click(function() {
							updateReview(reviewid,"warning","",reviewerid,$("#rev_warning_note").val());
							$("#nav_reviews").trigger("click");
						});
						$("#rev_change_newrev").off("keyup").keyup(function() {
							var name = $("#rev_change_newrev").val();
							$.ajax({
								type        : "POST",
								url         : "backend.php",
								data        : {"action":"rersearch", "name": name, "address": "", "fields": ""},
								dataType    : "json"
							})
							.done(function(e) {
								$("#change_rersults").remove();
								if (e.body.length >566){
									$("#rev_change_newrev").after("<div id=change_rersults>" + e.body + "</div>");
									$("#change_rersults td,th").hide();
									$("#change_rersults td:first-child").show();
									$("#change_rersults tr").off("click").click(function(e) {
										var rer_name = $(this).find("td");
										console.log(rer_name.eq("0").html());
										$("#rev_change_newrev").val($.trim(rer_name.eq("0").html()));
										var rer_id = $(this).data("reviewerid");
										$("#rev_change_newrev").data("reviewerid", rer_id);
										$("#change_rersults").remove();
										console.log($("#rev_change_newrev").data());
									});
								}
							})
							.fail(function(e, errorThrown, jqXHR) {
								console.log(e);
								$("#output_body").html("Oops: <br>" + JSON.stringify(findreview_formdata) + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
							});
						});
						$("#rev_change_btn").off("click").click(function() {
							var new_reviewerid = $("#rev_change_newrev").data("reviewerid");
							updateReview(reviewid,"change_of_rer","",new_reviewerid,$("#rev_change_note").val());
							$("#rev_change_newrev").removeData();
							$("#nav_reviews").trigger("click");
						});
						$(".toast").hide();
						$("#zdl_stage_btn").off("click").click(function(e) {
							var zdl_objekt = {};
							zdl_objekt.Rezensionseingang = $("#review_arrived").prop("checked");
							zdl_objekt.Eingangsdatum = $("#review_arrived_date").val();
							zdl_objekt.Dateispeicherort = $("#review_filelocation").val();
							zdl_objekt.Formatiert = $("#formatiert").prop("checked");
							zdl_objekt.Lektoriert = $("#lektoriert").prop("checked");
							zdl_objekt.zum_Setzen = $("#zum_setzen").prop("checked");
							zdl_objekt.vom_Setzen = $("#vom_setzen").prop("checked");
							zdl_objekt.Seitenzahl = $("#vom_setzen_seitenzahl").val();
							zdl_objekt.Imprimatur = $("#imprimatur").prop("checked");
							zdl_objekt.Umbruch = $("#umbruch").prop("checked");
							zdl_objekt.Kommentar = $("#zdl_comment").val();
							updateReview(reviewid, "zdl_stage", "", "", zdl_objekt);
							$("#zdl-toast").fadeIn(400).delay(3000).fadeOut(400);

							var zdl_vorher = $("#zdl_stage_btn").data("zdl_objekt");

							if (zdl_vorher.Rezensionseingang=="false" && zdl_objekt.Rezensionseingang) {
								updateReview(reviewid, "review_arrived", zdl_objekt.Eingangsdatum, reviewerid, "");
							}
							if (zdl_vorher.Formatiert=="false" && zdl_objekt.Formatiert) {
								updateReview(reviewid, "formatiert", "", reviewerid, "");
							}
							if (zdl_vorher.Lektoriert=="false" && zdl_objekt.Lektoriert) {
								updateReview(reviewid, "lektoriert", "", reviewerid, "");
							}
							if (zdl_vorher.zum_Setzen=="false" && zdl_objekt.zum_Setzen) {
								updateReview(reviewid, "zum_setzen", "", reviewerid, "");
							}
							if (zdl_vorher.vom_Setzen=="false" && zdl_objekt.vom_Setzen) {
								updateReview(reviewid, "vom_setzen", "", reviewerid, "");
							}
							if (zdl_vorher.Imprimatur=="false" && zdl_objekt.Imprimatur) {
								updateReview(reviewid, "imprimatur", "", reviewerid, "");
							}
							if (zdl_vorher.Umbruch=="false" && zdl_objekt.Umbruch) {
								updateReview(reviewid, "umbruch", "", reviewerid, "");
							}
							$("#nav_reviews").trigger("click");
						});

						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action" : "resdata", "resid" : reviewid},
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							$("#rev_events").html(e.events);
							var ev_data = jQuery.parseJSON(e.ev_data);
	// { "Rezensionseingang" : "Angekommen! Speicherort: h" , "Rezensionseingang" : "" , "Umbruch" : "" , "Imprimatur" : "" , "zur. vom Setzen" : "" , "zum Setzen" : "abegd" , "Lektoriert" : "" , "Formatiert" : "" , "Rezensionseingang" : "vsfdcv s" , "Umbruch" : "Erledigt!  | qy2awe" , "Imprimatur" : "Erledigt!  | khjbk7h" , "zur. vom Setzen" : "Erledigt! Seitenzahl: 455 | hftgh" , "zum Setzen" : "Erledigt!  | hfgjhftz" , "Lektoriert" : "Erledigt!  | rtzutz" , "Formatiert" : "Erledigt!  | rtzrt" , "Rezensionseingang" : "Angekommen! Speicherort: h | retzrtz" , "Wechsel des Rezensenten" : "Wechsel von Prof. Alivia Brakus zu Prof. Dallas Mosciski | " , "Mahnung" : "rtzrtz" , "Neue Deadline" : "" , "Neu vergeben an Prof. Alivia Brakus " : "" , "Absage von Prof. Molly Cronin " : "Persönliche Beteiligung am Buch" , "Neue Rezension, vergeben an Prof. Molly Cronin " : "" }
							// if (e.ev_data.search("Rezensionseingang | Angekommen!") >0) {
							// 	console.log("yay");
							// } else {
							// 	console.log("nay: " + e.ev_data.search("Rezensionseingang | Angekommen!"));
							// }
							// console.log(ev_data);

							$("#rev_events span").off("click").click(function() {
								if(confirm(unescape("Diesen Vorgang wirklich l%F6schen?"))){
									var event_id = $(this).data("event_id");
									$.ajax({
										type        : "POST",
										url         : "backend.php",
										data        : {"action" : "delete_event", "event_id" : event_id},
										dataType    : "json",
										encode      : true
									})
									.done(function(e) {
										alert(unescape("gel%F6scht!"));
									});
								}
							});
							var zdl_objekt = {};
							zdl_objekt = jQuery.parseJSON(e.zdl_stage);
							$("#review_arrived").prop("checked", (zdl_objekt.Rezensionseingang === "true" ? true:false));
							$("#review_arrived_date").val(zdl_objekt.Eingangsdatum);
							$("#review_filelocation").val(zdl_objekt.Dateispeicherort);
							$("#formatiert").prop("checked", (zdl_objekt.Formatiert === "true" ? true:false));
							$("#lektoriert").prop("checked", (zdl_objekt.Lektoriert === "true" ? true:false));
							$("#zum_setzen").prop("checked", (zdl_objekt.zum_Setzen === "true" ? true:false));
							$("#vom_setzen").prop("checked", (zdl_objekt.vom_Setzen === "true" ? true:false));
							$("#vom_setzen_seitenzahl").val(zdl_objekt.Seitenzahl);
							$("#imprimatur").prop("checked", (zdl_objekt.Imprimatur === "true" ? true:false));
							$("#umbruch").prop("checked", (zdl_objekt.Umbruch === "true" ? true:false));
							$("#zdl_comment").val(zdl_objekt.Kommentar);
							$("#zdl_stage_btn").data("zdl_objekt", zdl_objekt);
						})
						.fail(function(e, errorThrown, jqXHR) {
							console.log(e);
							$("#output_body").html("Oops: (resdata) <br>" +/* JSON.stringify(zdl_stage_state)*/ + "<br><br>" +  errorThrown + "<br><br>" +  JSON.stringify(jqXHR) + "<br><br>" +  JSON.stringify(e));
						});
					});
				});
			});
			$("#reviewmodal").off("hide.bs.modal").on("hide.bs.modal", function (e) {
				$("#nav_reviews").trigger("click");
			});
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			$("#output_body").html(textStatus + "<br>___<br>" + errorThrown + "<br>___<br>" + jqXHR.responseText);
		});
	});

		// $("#output_header").empty().html("<div class='narf' id='a'>a</div><div class='narf' id='b'>b</div>");
		// $(".narf").click(function(e){
		// 	console.log(e.target.id);
		// });
		//



	$("#nav_bugreports").off("click").click(function() {
		$(".output").empty();
		$("#main").hide(1000);
			$.ajax({
				type        : "POST",
				url         : "backend.php",
				data        : {"action":"bugreports"},
				dataType    : "json"
			})
			.done(function(e) {
				$("#output_body").html(e.html);
				$("#author").val($("#nav_login").data("user"));
				$("#save_bugreport").off("click").click(function() {
					if(!$("#author").val()){
						alert("Bitte Namen eingeben");
					}else {
						var content = $("#new_bugreport").val();
						var author = $("#author").val();
						var id = null;
						($(this).data("id") ?id = $(this).data("id") : id = null)
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"bugreports", "content":content, "author":author, "id":id},
						dataType    : "json"
					})
					.done(function(e) {
						$("#nav_bugreports").trigger("click");
						console.log(e);
					})
					.fail(function( jqXHR, textStatus, errorThrown ) {
						console.log(jqXHR);
					});
				}
				});
				$("#show_all").off("click").click(function() {
					$(".hide_me").toggle();
					($("#show_all").text() === "Alle anzeigen") ? $("#show_all").text("Wieder ausblenden") : $("#show_all").text("Alle anzeigen");
				});
				$(".glyphicon-trash").off("click").click(function() {
					if(confirm(unescape("Diesen Kommentar wirklich l%F6schen?"))){
						var id = $(this).data("id");
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action" : "delete_bugreport", "id" : id},
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							alert(unescape("gel%F6scht!"));
							$("#nav_bugreports").trigger("click");
						});
					}
				});
				$(".glyphicon-ok").off("click").click(function() {
					if(confirm("erledigt?")){
						var id = $(this).data("id");
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action" : "mark_bugreport_done", "id" : id},
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							alert("erledigt!");
							$("#nav_bugreports").trigger("click");
						});
					}
				});
				$(".hide_this").off("click").click(function() {
					if(confirm("ausblenden?")){
						var id = $(this).data("id");
						$.ajax({
							type        : "POST",
							url         : "backend.php",
							data        : {"action" : "hide_bugreport", "id" : id},
							dataType    : "json",
							encode      : true
						})
						.done(function(e) {
							alert("erledigt!");
							$("#nav_bugreports").trigger("click");
						});
					}
				});
				$(".answer").off("click").click(function() {
					// <div class='container-fluid'><div class='col-sm-3'>Name: <input id='author'></div><div class='col-sm-3'><textarea id='new_bugreport' rows='5'></textarea></div></div><button type='button' id='save_bugreport' name='save_bugreport' class='btn btn-success'>Speichern<span class='fa fa-arrow-right'></span></button>
				});

				console.log();
			})
			.fail(function( jqXHR, textStatus, errorThrown ) {
				console.log(jqXHR);
			});
	});

	$("#nav_admin").off("click").click(function() {
		$(".output").empty();
		$("#main").hide(1000);
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"admin"},
			dataType    : "json"
		})
		.done(function(e) {
			$("#output_body").html(e.html);
			$(".fields").off("keyup").keyup(function() {
				var id = $(this).prop("id");
				var name = $(this).val();
				$.ajax({
					type        : "POST",
					url         : "backend.php",
					data        : {"action":"fieldupdate", "id":id, "name":name},
					dataType    : "json"
				})
				.done(function(e) {
					console.log(id + " (" + name + ") gespeichert");
				})
				.fail(function( jqXHR, textStatus, errorThrown ) {
					console.log(jqXHR);
				});
			});

			$(".glyphicon-trash").off("click").click(function(){
				var id = $(this).data("id");
				if (confirm("Sind Sie sicher?")) {
					$.ajax({
						type        : "POST",
						url         : "backend.php",
						data        : {"action":"fielddelete", "id":id},
						dataType    : "json"
					})
					.done(function(e) {
						console.log(id + " gelöscht");
						$("#nav_admin").trigger("click");
					})
					.fail(function( jqXHR, textStatus, errorThrown ) {
						console.log(jqXHR);
					});
				}
			});

			$("#new_field").off("click").click(function(){
				var name = prompt("Bitte eingeben:");
				$.ajax({
					type        : "POST",
					url         : "backend.php",
					data        : {"action":"fieldupdate", "id":"new", "name":name},
					dataType    : "json"
				})
				.done(function(e) {
					console.log("Neues FG (" + name + ") gespeichert");
					$("#nav_admin").trigger("click");
				})
				.fail(function( jqXHR, textStatus, errorThrown ) {
					console.log(jqXHR);
				});

			});
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			console.log(jqXHR);
		});
	});

	$("#main").off("click").click(function() {
		$(".output").empty();
		var bookid = prompt("id?");
		$.ajax({
			type        : "POST",
			url         : "backend.php",
			data        : {"action":"foo", "bookid": bookid},
			dataType    : "json"
		})
		.done(function(e) {
			console.log(e.html);
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			console.log(jqXHR);
		});
	});
} else {
	$("#nav_login").trigger("click");
}

console.log(e);
})
.fail(function(e) {
console.log("loginstate abfrage failed");
});

});
